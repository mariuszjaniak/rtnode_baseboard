/*****************************************************************************
 *                                                                           *
 *   halNetworkInterface.c                                                   *
 *                                                                           *
 *   The lm3s8962 network interface hardware abstraction layer               *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

/* Standard includes. */
#include <stdint.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* RTnet includes */
#include "rtnet_inet.h"
#include "rtnet_private.h"
#include "halNetworkInterface.h"
#include "halTimerInterface.h"

/* Driver includes. */
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_nvic.h"
#include "inc/hw_types.h"
#include "inc/hw_ethernet.h"
#include "driverlib/ethernet.h"
#include "driverlib/interrupt.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"

/* Other includes. */
#include "types.h"
#include "globals.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

struct xNETWORK_DEV{
  xSemaphoreHandle  xEthRxEventSemaphore;
  uint64_t          ullTimeStamp;
};
typedef struct xNETWORK_DEV xNetworkDev_t;

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

/* A deferred interrupt handler task that processes received frames. */
static void prvETHDeferredInterruptHandlerTask(void *pvParameters);
/* Drop etheret frame -- there is no such function in Stellaris driverlib */
static void EthernetPacketDrop(unsigned long ulBase);

/*****************************************************************************
 *  Global variables
 *****************************************************************************/

/* Network devices */
static xNetworkDev_t xNetwork;

/*****************************************************************************
 * Functions implementation
 *****************************************************************************/

BaseType_t  xHalNetworkInterfaceInitialise(uint8_t pucMac[])
{
  BaseType_t  xReturn = pdFAIL;

  /* Enable and Reset the Ethernet Controller. */
  SysCtlPeripheralEnable(SYSCTL_PERIPH_ETH);
  SysCtlPeripheralReset(SYSCTL_PERIPH_ETH);
  /* Set GPIO PF2 and PF3 as ETH LED pins */
  GPIOPinTypeEthernetLED(GPIO_PORTF_BASE, GPIO_PIN_2 | GPIO_PIN_3);
  /* Disable all Ethernet Controller interrupt sources. */
  EthernetIntDisable(ETH_BASE, (ETH_INT_PHY  |
      ETH_INT_MDIO |
      ETH_INT_RXER |
      ETH_INT_RXOF |
      ETH_INT_TX   |
      ETH_INT_TXER |
      ETH_INT_RX));
  EthernetIntClear(ETH_BASE, EthernetIntStatus(ETH_BASE, false));
  /* Initialize the Ethernet Controller */
  EthernetInitExpClk(ETH_BASE, SysCtlClockGet());
  EthernetConfigSet(ETH_BASE, (ETH_CFG_TX_DPLXEN |
      ETH_CFG_TX_CRCEN  |
      ETH_CFG_TX_PADEN));
  //EthernetEnable(ETH_BASE);
  /* Set MAC address */
  EthernetMACAddrSet(ETH_BASE, pucMac);
  /* Initialize network structure */
  xNetwork.xEthRxEventSemaphore = xSemaphoreCreateCounting(1, 0);
  if(xNetwork.xEthRxEventSemaphore == NULL){
    /* Clean */
    EthernetDisable(ETH_BASE);
    return pdFAIL;
  }
  /* The Rx deferred interrupt handler task is created at the highest
   * possible priority (but lower then timer task) to ensure the interrupt
   * handler can return directly to it no matter which task was running when
   * the interrupt occurred. */
  xReturn = xTaskCreate(prvETHDeferredInterruptHandlerTask,
                        NULL,
                        configSTACK_SIZE_RTNETETH,
                        &xNetwork,
                        configTASK_PRIO_RTNETETH,
                        NULL);
  if(xReturn == pdFAIL){
    /* Clean */
    EthernetDisable(ETH_BASE);
    vSemaphoreDelete(xNetwork.xEthRxEventSemaphore);
    return pdFAIL;
  }
  return xReturn;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xHalNetworkInterfaceOutput(uint8_t   pucBuffer[],
                                         size_t    xDataLength,
                                         uint64_t *pullTimeStamp)
{
  BaseType_t  xReturn = pdFAIL;

  /* Set frame time stamp */
  if(pullTimeStamp != NULL){
    uint8_t  *ptr = (uint8_t *)pullTimeStamp;
    uint64_t  pullTime;
    xHalTimerGet(&pullTime);
    pullTime = rtnet_htonll(pullTime);
    memcpy(ptr, &pullTime, sizeof(uint64_t));
  }
  if(EthernetPacketPutNonBlocking(ETH_BASE, pucBuffer, xDataLength) > 0)
    xReturn = pdPASS;
  return xReturn;
}
/*---------------------------------------------------------------------------*/

void ETH_isr(void)
{
  signed BaseType_t  hptw = FALSE;
  unsigned long        status;

  /* Get frame time stamp */
  xHalTimerGet(&xNetwork.ullTimeStamp);
  /* Read and clear the interrupt */
  status = EthernetIntStatus(ETH_BASE, false);
  EthernetIntClear(ETH_BASE, status);
	/* Unblock the deferred interrupt handler task if the event was an Rx. */
	if(status & ETH_INT_RX){
    EthernetIntDisable(ETH_BASE, ETH_INT_RX);
		xSemaphoreGiveFromISR(xNetwork.xEthRxEventSemaphore, &hptw);
		portEND_SWITCHING_ISR(hptw);
	}
}
/*---------------------------------------------------------------------------*/

void prvETHDeferredInterruptHandlerTask(void *pvParameters)
{
  xNetworkDev_t *pxDev = pvParameters;
  uint64_t       ullTimeStamp;

  /* Enable the Ethernet controller */
  EthernetEnable(ETH_BASE);
  /* Enable the Ethernet interrupt */
  IntEnable(INT_ETH);
  /* Priorities that allow for calling FreeRTOS API are in range:
   * 3 (the highest) to 7 (the lowest) -- see FreeRTOSConfig.h */
  IntPrioritySet(INT_ETH, configINTERRUPT_PRIORITY_ETH);
  /* Enable Ethernet TX and RX packet interrupts */
  //EthernetIntEnable(ETH_BASE, ETH_INT_RX | ETH_INT_TX);
  EthernetIntEnable(ETH_BASE, ETH_INT_RX);
  /* Main loop */
	for( ;; ){
	  xRTbuf_t *pxRTbuf;
		/* Wait for the ETH interrupt to indicate that another packet has been
		received. */
		while(xSemaphoreTake(pxDev->xEthRxEventSemaphore, portMAX_DELAY) ==
		    pdFALSE);
		/* Store time stamp */
    //xHalTimerGet(halTIMER_DEV_ID0, &ullTimeStamp);
		ullTimeStamp = pxDev->ullTimeStamp;
		/* Allocate a network buffer descriptor that references an Ethernet
		   buffer large enough to hold the received data. */
		pxRTbuf = pxRTbufGet(rtnetETHERNET_FRAME_SIZE, (portTickType) 0);
		if(pxRTbuf != NULL){
		  uint32_t ulLen;
      /* Copy the received data into pcNetworkBuffer->pucEthernetBuffer. */
		  ulLen = EthernetPacketGetNonBlocking(ETH_BASE,
		                                       pxRTbuf->pucData,
		                                       pxRTbuf->ulSize);
		  pxRTbuf->ulSize = ulLen;
		  /* Assert the receiver interrupt */
		  EthernetIntEnable(ETH_BASE, ETH_INT_RX);
		  /* See if the data contained in the received Ethernet frame needs
		     to be processed. */
		  switch(eRTnetCheckFrame(pxRTbuf->pucData)){
		  case eRTnetFrameDiscipline:
		  case eRTnetFrameIPv4:
		    xRTnetProcessFrame(pxRTbuf, ullTimeStamp);
		    break;
		  case eRTnetFrameOther:
		  case eRTnetFrameNotRecipient:
        /* Release the Ethernet buffer */
		    vRTbufRelease(pxRTbuf);
		    break;
		  }
		}
    else{
      /* Clear Ethernet buffer */
      EthernetPacketDrop(ETH_BASE);
      /* Assert the receiver interrupt */
      EthernetIntEnable(ETH_BASE, ETH_INT_RX);
    }
	}
}
/*---------------------------------------------------------------------------*/

void EthernetPacketDrop(unsigned long ulBase)
{
    volatile unsigned long ulTemp;
    long lFrameLen, lTempLen;
    long i = 0;

    /* Read WORD 0 (see format above) from the FIFO, set the receive
     * Frame Length and store the first two bytes of the destination
     * address in the receive buffer. */
    ulTemp = HWREG(ulBase + MAC_O_DATA);
    lFrameLen = (long)(ulTemp & 0xFFFF);
    /* Read all but the last WORD into the receive buffer. */
    lTempLen = lFrameLen - 6;
    while(i <= (lTempLen - 4)){
      ulTemp = HWREG(ulBase + MAC_O_DATA);
      i += 4;
    }
    /* Read the last 1, 2, or 3 BYTES into the buffer */
    if(i < lTempLen){
      ulTemp = HWREG(ulBase + MAC_O_DATA);
      if(i == lTempLen - 3) i += 1;
      else if(i == lTempLen - 2) i += 2;
      else if(i == lTempLen - 1) i += 3;
    }
    /* Read any remaining WORDS (that did not fit into the buffer). */
    while(i < (lFrameLen - 2)){
        ulTemp = HWREG(ulBase + MAC_O_DATA);
        i += 4;
    }
}
/*---------------------------------------------------------------------------*/
