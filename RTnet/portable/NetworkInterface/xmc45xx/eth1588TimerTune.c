/*****************************************************************************
 *                                                                           *
 *   eth1588TimerTune.c                                                      *
 *                                                                           *
 *   The XMC4500 ETH 1588 timer frequency tuning procedure                   *
 *                                                                           *
 *   Copyright (C) 2015 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                               *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#include "eth1588TimerTune.h"
#include "XMC4500.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

/*****************************************************************************
 *  Global variables
 *****************************************************************************/

/*****************************************************************************
 * Functions implementation
 *****************************************************************************/

void vEth1588TimerTune(uint64_t ullMasterClockCount,
                       uint64_t ullSlaveClockCount)
{
  int64_t  llClockDiffCount;
  int64_t ullFreqCompensationValue;

  llClockDiffCount          = ullMasterClockCount - ullSlaveClockCount;
  ullFreqCompensationValue  = ETH0->TIMESTAMP_ADDEND;
  ullFreqCompensationValue *= (ullMasterClockCount + llClockDiffCount);
  ullFreqCompensationValue /= ullSlaveClockCount;
  ETH0->TIMESTAMP_ADDEND    = (uint32_t) ullFreqCompensationValue;
  /* Update addend register */
  ETH0->TIMESTAMP_CONTROL  |= ETH_TIMESTAMP_CONTROL_TSADDREG_Msk;
}
/*---------------------------------------------------------------------------*/
