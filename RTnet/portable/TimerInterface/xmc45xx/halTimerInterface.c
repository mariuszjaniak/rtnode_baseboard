/*****************************************************************************
 *                                                                           *
 *   halTimerInterface.c                                                     *
 *                                                                           *
 *   The XMC4500 timer hardware abstraction layer                            *
 *                                                                           *
 *   Copyright (C) 2015 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                               *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"

/* RTnet includes */
#include "halTimerInterface.h"

/* Driver includes. */
#include "Driver_ETH_MAC.h"
#include "eth1588TimerTune.h"

/* Other includes. */
#include "globals.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define timerNANOSEC 1000000000UL

#define TIME_TO_UINT64(sec, nsec) (((uint64_t)(sec)*timerNANOSEC) + \
                                   ((uint64_t)(nsec)))

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

/* CMISI driver structures */
extern ARM_DRIVER_ETH_MAC Driver_ETH_MAC0;

struct xTIMER_DEV{
  TaskHandle_t        xTaskTimerEvent;
  xTdmaDev_t         *pxTdma;
  uint64_t            ullMasterClockTimePrv;
  uint64_t            ullSlaveClockTimePrv;
  ARM_DRIVER_ETH_MAC *xMac;
};

typedef struct xTIMER_DEV xTimerDev_t;

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

/* A deferred interrupt handler task that processes received frames. */
static void prvTimerDeferredInterruptHandlerTask(void *pvParameters);

/*****************************************************************************
 *  Global variables
 *****************************************************************************/

/* Network devices */
static xTimerDev_t xTimer;

/*****************************************************************************
 * Functions implementation
 *****************************************************************************/

BaseType_t  xHalTimerInterfaceInitialise(xTdmaDev_t *pxTdma)
{
  BaseType_t  xReturn = pdFAIL;

  /* Initialize timer structure */
  xTimer.pxTdma                = pxTdma;
  xTimer.ullMasterClockTimePrv = 0;
  xTimer.ullSlaveClockTimePrv  = 0;
  xTimer.xMac                  = &Driver_ETH_MAC0;
  /* The timer deferred interrupt handler task is created at the highest
   * possible priority to ensure the interrupt handler can return directly to
   * it no matter which task was running when the interrupt occurred. */
  xReturn = xTaskCreate(prvTimerDeferredInterruptHandlerTask,
                        NULL,
                        configSTACK_SIZE_RTNETTIMER,
                        &xTimer,
                        configTASK_PRIO_RTNETTIMER,
                        &xTimer.xTaskTimerEvent);
  if(xReturn == pdFAIL) return pdFAIL;

  /* Timer initialization is handled by ETH module */

  return xReturn;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xHalTimerGet(uint64_t *pullTime)
{
  ARM_ETH_MAC_TIME xTime;

  xTimer.xMac->ControlTimer(ARM_ETH_MAC_TIMER_GET_TIME, &xTime);
  *pullTime = TIME_TO_UINT64(xTime.sec, xTime.ns);
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xHalTimerSet(uint64_t ullTime)
{
  ARM_ETH_MAC_TIME xTime;

  xTime.sec = (uint32_t)(ullTime / timerNANOSEC);
  xTime.ns  = (uint32_t)(ullTime % timerNANOSEC);
  xTimer.xMac->ControlTimer(ARM_ETH_MAC_TIMER_SET_TIME, &xTime);
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xHalTimerSyncInit(uint64_t ullMasterClockTime)
{
  xTimer.ullMasterClockTimePrv = ullMasterClockTime;
  xTimer.ullSlaveClockTimePrv  = ullMasterClockTime;
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xHalTimerSync(uint64_t ullMasterClockTime,
                          uint64_t ullSlaveClockTime)
{
  /* Tune slave clock frequency */
  vEth1588TimerTune(ullMasterClockTime - xTimer.ullMasterClockTimePrv,
                    ullSlaveClockTime  - xTimer.ullSlaveClockTimePrv);
  xTimer.ullMasterClockTimePrv = ullMasterClockTime;
  xTimer.ullSlaveClockTimePrv  = ullSlaveClockTime;
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xHalTimerNewEvent(uint64_t ullTime,
                              uint64_t ullLim)
{
  ARM_ETH_MAC_TIME xTime;
  uint64_t         ullActual;

  xHalTimerGet(&ullActual);
  if(ullTime - ullActual > ullLim) return pdFAIL;
  /* Set alarm */
  xTime.sec = (uint32_t)(ullTime / timerNANOSEC);
  xTime.ns  = (uint32_t)(ullTime % timerNANOSEC);
  xTimer.xMac->ControlTimer(ARM_ETH_MAC_TIMER_SET_ALARM, &xTime);
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

void vEthernetTimerNotify(BaseType_t *hptw)
{
  /* Unblock the deferred interrupt handler task */
  if(xTimer.pxTdma->eState != eTdmaStatePreInit){
    //BaseType_t  hptw = false;
    vTaskNotifyGiveFromISR(xTimer.xTaskTimerEvent, hptw);
    //portEND_SWITCHING_ISR(hptw);
  }
}
/*---------------------------------------------------------------------------*/

void prvTimerDeferredInterruptHandlerTask(void *pvParameters)
{
  xTimerDev_t *pxDev = pvParameters;

  for( ;; ){
    uint8_t *pucPtr;
    /* Wait for the timer interrupt */
    ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
    /* Process slot event (in order to reduce jitter move to timer isr) */
    taskENTER_CRITICAL();
    {
      pucPtr = pucTdmaProcessSlotEvent(pxDev->pxTdma);
    }
    taskEXIT_CRITICAL();
    /* Post process slot event */
    xTdmaPostProcessSlotEvent(pxDev->pxTdma, pucPtr);
  }
}
/*---------------------------------------------------------------------------*/
