/*****************************************************************************
 *                                                                           *
 *   rtnet_common.h                                                          *
 *                                                                           *
 *   RTnet common data types and utilities                                   *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#ifndef _RTNETCOMMON_H_
#define _RTNETCOMMON_H_

#include <stdint.h>

/* FreeRTOS includes */

/* RTnet includes */

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define RTNET_NETMASK_BROADCAST 0x00FFFFFFUL

/* Socket flags */
#define RTNET_ZERO_COPY         0x00000001

/* Address families. */
#define RTNET_AF_INET           2

/* Socket types. */
#define RTNET_SOCK_DGRAM        2

/* IP protocols */
#define RTNET_IPPROTO_UDP       17

#define RTNET_SO_RXTIMEOUT      0x00   /* Set the receive time out. */
#define RTNET_SO_TXTIMEOUT      0x01   /* Set the send time out. */
#define RTNET_SO_UDPCKSUM       0x02   /* Enable the UDP checksum for outgoing
                                          packets*/
/* Error codes */
#define RTNET_SOCKET_ERROR     -1
#define RTNET_EWOULDBLOCK      -2
#define RTNET_EINVAL           -4
#define RTNET_EADDRNOTAVAIL    -5
#define RTNET_EADDRINUSE       -6
#define RTNET_TIMEOUT          -7
#define RTNET_ENOPROTOOPT      -8
#define RTNET_EMSGSIZE         -9
#define RTNET_ENOMEM           -10

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

typedef void* xRTnetSocket_t;

struct xRTNET_SOCKADDR
{
  uint32_t sin_addr;
  uint16_t sin_port;
};
typedef struct xRTNET_SOCKADDR xRTnetSockAddr_t;

/*****************************************************************************
 * Globals variables
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

/*****************************************************************************
 *
 * Calculate 16-bit one's complement (RFC 768) checksum.
 *
 * \param[in] pucData  Points to buffer containing checksumed data.
 * \param[in] usLen    Data size.
 *
 * \return Returns calculated checksum.
 *
 *****************************************************************************/

uint16_t rtnet_checksum(uint8_t *pucData, uint16_t  usLen);

#endif /* _RTNETCOMMON_H_ */
