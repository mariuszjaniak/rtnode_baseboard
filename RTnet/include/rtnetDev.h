/*****************************************************************************
 *                                                                           *
 *   rtnetDev.h                                                              *
 *                                                                           *
 *   RTnet device                                                            *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#ifndef _RTNETDEV_H_
#define _RTNETDEV_H_

#include <stdint.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"

/* RTnet includes */
#include "rtmac.h"
#include "rtcfg.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define rtnetdevMAC_SIZE 6

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

struct xRTNET_DEV{
  xRTmacDev_t xRTmac;
  xRTcfgDev_t xRTcfg;
  uint32_t    ulMagic;
  uint32_t    ulTxFrame;
  uint32_t    ulRxFrame;
  uint8_t     pucMac[rtnetdevMAC_SIZE];
};
typedef struct xRTNET_DEV xRTnetDev_t;

/*****************************************************************************
 * Globals variables
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

/*****************************************************************************
 *
 * Initialize RTnet device.
 *
 * \param[in] pxDev   Points to the RTnet device structure.
 * \param[in] pucMac  The array containing device Ethernet MAC address.
 *
 * \return Returns the pdPASS value if succeed initialization, pdFAIL in
 *         other case.
 *
 *****************************************************************************/

BaseType_t  xRTnetDevInit(xRTnetDev_t *xDev, uint8_t pucMac[]);

/*****************************************************************************
 *
 * Determine if RTnet derive has been initialized already.
 *
 * \return Returns pdTRUE if RTnet device has been initialized, pdFALSE
 *         otherwise.
 *
 *****************************************************************************/

BaseType_t  xRTnetDevIsInit(xRTnetDev_t *pxDev);

#endif /* _RTNETDEV_H_ */
