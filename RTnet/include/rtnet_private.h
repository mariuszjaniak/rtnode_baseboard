/*****************************************************************************
 *                                                                           *
 *   rtnet_private.h                                                         *
 *                                                                           *
 *   Private RTnet stack definitions                                         *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#ifndef _RTNET_PRIVATE_H_
#define _RTNET_PRIVATE_H_

#include <stdint.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"

/* RTnet includes */
#include "RTnetConfig.h"
#include "rtbuf.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define RTNET_MS_TO_TIC(ms) ((TickType_t)((((uint64_t)(ms))*configTICK_RATE_HZ)/1000))

#define RTNET_MIN(a, b) (((a) < (b)) ? (a) : (b))
#define RTNET_MAX(a, b) (((a) > (b)) ? (a) : (b))

/* Dimensions the buffers that are filled by received Ethernet frames. */
#define rtnetETHERNET_MAC_SIZE       (6UL)
#define rtnetETHERNET_ETHERTYPE_SIZE (2UL)
#define rtnetETHERNET_HEADER_SIZE    (2*rtnetETHERNET_MAC_SIZE + rtnetETHERNET_ETHERTYPE_SIZE)
#define rtnetETHERNET_MTU_SIZE       (rtnetconfigETHERNET_MTU_SIZE)
#define rtnetETHERNET_CRC_SIZE       (4UL)
#define rtnetETHERNET_FRAME_SIZE     (rtnetETHERNET_HEADER_SIZE + rtnetETHERNET_MTU_SIZE + rtnetETHERNET_CRC_SIZE)

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

enum eRTNET_FRAMECLASS{
  eRTnetFrameDiscipline,
  eRTnetFrameIPv4,
  eRTnetFrameOther,
  eRTnetFrameNotRecipient,
};
typedef enum eRTNET_FRAMECLASS eRTnetFrameClass_t;

enum eRTNET_FRAME{
  eRTnetFrameEthernet,
  eRTnetFrameRTmacTdma,
  eRTnetFrameRTcfg
};
typedef enum eRTNET_FRAME eRTnetFrame_t;

enum eRTNET_EVENT{
  eRTnetEventTxFrame,
  eRTnetEventTxReady,
  eRTnetEventSync,
  eRTnetEventReady,
  eRTnetEventRxRTcfg,
  eRTnetEventRxIPv4,
  eRTnetEventIPRx,
  eRTnetEventSendAcknowledge,
  eRTnetEventSendReady,
  eRTnetEventSendHeartbeat,
  eRTnetEventStartHeartbeat,
};
typedef enum eRTNET_EVENT eRTnetEvent_t;

struct xRTNET_EVENT{
  void          *pvData;
  uint32_t       ulLen;
  eRTnetEvent_t  eType;
};
typedef struct xRTNET_EVENT xRTnetEvent_t;

/*****************************************************************************
 * Globals variables
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

/*****************************************************************************
 *
 * Examines a received Ethernet frame to determine if, the Ethernet frame
 * should be processed or dropped.
 *
 * \param[in] pucFrame The array containing Ethernet frame.
 *
 * \return Returns:
 *           - eRTnetFrameDiscipline -- the RTnet frame, should be processed
 *           - eRTnetFrameOther -- a standard Ethernet frame, should be
 *             processed,
 *           - eRTnetFrameNotRecipient -- frame addressed to different device,
 *             should NOT be processed.
 *
 *****************************************************************************/

eRTnetFrameClass_t eRTnetCheckFrame(uint8_t pucFrame[]);

/*****************************************************************************
 *
 * Process RTnet frame.
 *
 * \param[in] pxRTbuf      Points to real-time data buffer containing frame.
 * \param[in] ullTimeStamp The frame time stamp (the time when frame has been
 *                         received)
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTnetProcessFrame(xRTbuf_t *pxRTbuf, uint64_t  ullTimeStamp);

/*****************************************************************************
 *
 * Prepare RTnet frame, allocate buffer and add appropriate headers.
 *
 * \param[out] pulSize   Points to the variable where frame header size will be
 *                       stored.
 * \param[in]  xLen      The requested space for the data.
 * \param[in]  pucMac    Points to the array containing destination MAC
 *                       address.
 * \param[in]  eFrameTyp A frame type.
 *
 * \return Returns the pointer to allocated frame if success, NULL in
 *         other case.
 *
 *****************************************************************************/

uint8_t *pucRTnetPrepareFrame(uint32_t      *pulSize,
                              size_t         xLen,
                              uint8_t       *pucMac,
                              eRTnetFrame_t  eFrameType);

/*****************************************************************************
 *
 * Add Ethernet header to the frame.
 *
 * \param[in]  pucFrame  Points to the array containing frame
 * \param[in]  pucMac    Points to the array containing destination MAC
 *                       address.
 * \param[in]  eFrameTyp A frame type.
 *
 * \return Returns the size of the added header
 *
 *****************************************************************************/

uint32_t ulRTnetHeader(uint8_t       *pucFrame,
                       uint8_t       *pucMac,
                       eRTnetFrame_t  eFrameType);

/*****************************************************************************
 *
 * Send event to the RTnet stack.
 *
 * \param[in] eEvent       The event type.
 * \param[in] pvData       Points to the data container associated with event.
 * \param[in] ulLen        The data length.
 * \param[in] xTicksToWait The maximum amount of time the task should block
 *                         waiting for space to become available on the queue,
 *                         should it already be full. The call will return
 *                         immediately if this is set to 0. The time is defined
 *                         in tick periods so the constant portTICK_RATE_MS
 *                         should be used to convert to real time if this is
 *                         required.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTnetSendEvent(eRTnetEvent_t  eEvent,
                            void          *pvData,
                            uint32_t       ulLen,
                            portTickType   xTicksToWait);

/*****************************************************************************
 *
 * Send frame.
 *
 * \param[in] pucFrame  The array containing Ethernet frame.
 * \param[in] ulLen     The length of the frame.
 * \param[in] xTicksToWait The maximum amount of time the task should block
 *                         waiting for space to become available on the queue,
 *                         should it already be full. The call will return
 *                         immediately if this is set to 0. The time is defined
 *                         in tick periods so the constant portTICK_RATE_MS
 *                         should be used to convert to real time if this is
 *                         required.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTnetSendFrame(uint8_t      pucFrame[],
                            uint32_t     ulLen,
                            portTickType xTicksToWait);

/*****************************************************************************
 *
 * Convert IP address to MAC address
 *
 * \param[out] pucMac    Points to the array where the MAC address will be
 *                       stored.
 * \param[in]  ulIpAddr  The IP address.
 *
 * \return Returns the pointer to array containing MAC address if succeed,
 *         NULL in other case.
 *
 *****************************************************************************/

uint8_t *pucRTnetGetMacPtr(uint32_t ulIpAddr);

/*****************************************************************************
 *
 * Determine master device MAC address
 *
 * \return Returns the pointer to the array containing master device MAC
 *         addres.
 *
 *****************************************************************************/

uint8_t *pucRTnetMacMaster(void);

/*****************************************************************************
 *
 * Determine broadcast MAC address
 *
 * \return Returns the pointer to the array containing master device MAC
 *         addres.
 *
 *****************************************************************************/

uint8_t *pucRTnetMacBroadcast(void);

/*****************************************************************************
 *
 * Determine device IP address
 *
 * \return Returns the pointer to the array containing master device MAC
 *         addres.
 *
 *****************************************************************************/

uint32_t ulRTnetIpAddr(void);

/*****************************************************************************
 *
 * Get size of Ethernet frame header.
 *
 * \return Returns the header size.
 *
 *****************************************************************************/

size_t xRTnetHeaderSize(void);

#endif /* _RTNET_PRIVATE_H_ */
