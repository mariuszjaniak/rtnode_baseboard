/*****************************************************************************
 *                                                                           *
 *   rtcfg.h                                                                 *
 *                                                                           *
 *   RTcfg -- real-time configuration service for RTnet                      *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#ifndef _RTCFG_H_
#define _RTCFG_H_

#include <stdint.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"

/* RTnet includes */
#include "rtarp.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define rtcfgETHERTYPE 0x9022
#define rtcfgMAC_SIZE  6

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

enum eRTCFG_FRAME{
  eRTcfgFrameConfiguration,
  eRTcfgFrameNewAnnouncement,
  eRTcfgFrameReplyAnnouncement,
  eRTcfgFrameInitial,
  eRTcfgFrameSubsequent,
  eRTcfgFrameAcknowledge,
  eRTcfgFrameReady,
  eRTcfgFrameHeartbeat,
  eRTcfgFrameDeadStation,
};
typedef enum eRTCFG_FRAME eRTcfgFrame_t;

enum eRTCFG_ADDR{
  eRTcfgAddrMAC,
  eRTcfgAddrIP,
};
typedef enum eRTCFG_ADDR eRTcfgAddr_t;

enum eRTCFG_STATE{
  eRTcfgStateInit,
  eRTcfgStateReady
};
typedef enum eRTCFG_STATE eRTcfgState_t;

struct xRTCFG_DEV{
  xRTarpDev_t   xArp;
  uint32_t      ulIpAddr;
  uint32_t      ulStage2Data;
  uint32_t      ulStage2Len;
  uint16_t      usHeartbeat;
  uint8_t       ucBurstRate;
  uint8_t       ucStage2Frame;
  uint8_t       pucMaster[rtcfgMAC_SIZE];
  eRTcfgAddr_t  eAddr;
  eRTcfgState_t eState;
};
typedef struct xRTCFG_DEV xRTcfgDev_t;

/*****************************************************************************
 * Globals variables
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

/*****************************************************************************
 *
 * Initialize RTcfg service.
 *
 * \param[in] pxDev  Points to the RTcfg device structure.
 *
 * \return Returns the pdPASS value if succeed initialization, pdFAIL in
 *         other case.
 *
 *****************************************************************************/

BaseType_t  xRTcfgInit(xRTcfgDev_t  *pxDev);

/*****************************************************************************
 *
 * Process received RTcfg frame.
 *
 * \param[in] pxDev        Points to the RTcfg device structure.
 * \param[in] pucFrame     The array containing received frame.
 * \param[in] ulLen        The length of the RTcfg frame.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTcfgProcessFrame(xRTcfgDev_t *pxDev,
                                 uint8_t      pucFrame[],
                                 uint32_t     ulLen,
                                 uint8_t     *pucSource,
                                 uint8_t     *pucDestination);

/*****************************************************************************
 *
 * Send RTcfg Acknowledge frame.
 *
 * \param[in] pxDev  Points to the RTcfg device structure.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTcfgSendAcknowledge(xRTcfgDev_t *pxDev);

/*****************************************************************************
 *
 * Send RTcfg Ready frame.
 *
 * \param[in] pxDev  Points to the RTcfg device structure.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTcfgSendReady(xRTcfgDev_t *pxDev);

/*****************************************************************************
 *
 * Send RTcfg Heartbeat frame.
 *
 * \param[in] pxDev  Points to the RTcfg device structure.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTcfgSendHearbeat(xRTcfgDev_t *pxDev);

/*****************************************************************************
 *
 * Add new entry to routing table.
 *
 * \param[in] pxDev     Points to the RTcfg device structure.
 * \param[in] pucMac    The array containing device Ethernet MAC address.
 * \param[in] ulIpAddr  The IP address.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTcfgRouteAdd(xRTcfgDev_t *pxDev,
                             uint8_t      pucMac[],
                             uint32_t     ulIpAddr);

/*****************************************************************************
 *
 * Remove MAC address from routing table.
 *
 * \param[in] pxDev   Points to the RTcfg device structure.
 * \param[in] pucMac  The array containing device Ethernet MAC address.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTcfgRouteDelMac(xRTcfgDev_t *pxDev, uint8_t pucMac[]);

/*****************************************************************************
 *
 * Remove IP address from routing table.
 *
 * \param[in] pxDev     Points to the RTcfg device structure.
 * \param[in] ulIpAddr  The IP address.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTcfgRouteDelIp(xRTcfgDev_t *pxDev, uint32_t ulIpAddr);

/*****************************************************************************
 *
 * Convert IP address to MAC address
 *
 * \param[in]  pxDev     Points to the RTcfg device structure.
 * \param[in]  ulIpAddr  The IP address.
 *
 * \return Returns the pointer to array containing MAC address if succeed,
 *         NULL in other case.
 *
 *****************************************************************************/

uint8_t *pucRTcfgGetMacPtr(xRTcfgDev_t *pxDev, uint32_t ulIpAddr);

/*****************************************************************************
 *
 * Get size of selected RTcfg frame.
 *
 * \param[in] eFrameType  A frame type.
 *
 * \return Returns the selected frame size.
 *
 *****************************************************************************/

size_t xRTcfgFrameSize(eRTcfgFrame_t eFrameType);

#endif /* _RTCFG_H_ */
