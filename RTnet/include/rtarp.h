/*****************************************************************************
 *                                                                           *
 *   rtarp.h                                                                 *
 *                                                                           *
 *   RT ARP -- real-time Address Resolution Protocol                         *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#ifndef _RTARP_H_
#define _RTARP_H_

#include <stdint.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"

/* RTnet includes */
#include "RTnetConfig.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define rtarpETHERNET_MAC_SIZE 6

#define rtarpWITHOUT_IP        0

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

enum eRTARP_STATE{
  eRTarpStateInit,
  eRTarpStateReady
};
typedef enum eRTARP_STATE eRTarpState_t;

struct xRTARP_ENT{
  uint32_t       ulIpAddr;
  uint8_t        pucMac[rtarpETHERNET_MAC_SIZE];
  eRTarpState_t  eState;
};
typedef struct xRTARP_ENT xRTarpEnt_t;

struct xRTARP_DEV{
  xRTarpEnt_t pucTab[rtnetconfigARP_TAB_SIZE];
  uint32_t    ulLen;
};
typedef struct xRTARP_DEV xRTarpDev_t;

/*****************************************************************************
 * Globals variables
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

/*****************************************************************************
 *
 * Initialize ARP structure.
 *
 * \param[in] pxDev  Points to the ARP structure.
 *
 * \return Returns the pdPASS value if succeed initialization, pdFAIL in
 *         other case.
 *
 *****************************************************************************/

BaseType_t  xRTarpInit(xRTarpDev_t *pxDev);

/*****************************************************************************
 *
 * Add new entry to ARP table.
 *
 * \param[in] pxDev     Points to the ARP structure.
 * \param[in] pucMac    Points to the array containing the MAC address.
 * \param[in] ulIpAddr  The IP address.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTarpAdd(xRTarpDev_t *pxDev,
                        uint8_t     *pucMac,
                        uint32_t     ulIpAddr);

/*****************************************************************************
 *
 * Delete entry from ARP table by IP address.
 *
 * \param[in] pxDev     Points to the ARP structure.
 * \param[in] ulIpAddr  The IP address.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTarpDelIp(xRTarpDev_t *pxDev,
                          uint32_t     ulIpAddr);

/*****************************************************************************
 *
 * Delete entry from ARP table by MAC address.
 *
 * \param[in] pxDev     Points to the ARP structure.
 * \param[in] pucMac    Points to the array containing the MAC address.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTarpDelMac(xRTarpDev_t *pxDev,
                           uint8_t     *pucMac);


/*****************************************************************************
 *
 * Determine if ARP table has entry with given MAC address.
 *
 * \param[in]  pxDev     Points to the ARP structure.
 * \param[in]  pucMac    Points to the array containing the MAC address.
 *
 * \return Returns the pdPASS value if there is a entry with given MAC address,
 *         pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTarpHasMac(xRTarpDev_t *pxDev,
                           uint8_t     *pucMac);

/*****************************************************************************
 *
 * Returns IP address associated with given MAC address.
 *
 * \param[in]  pxDev     Points to the ARP structure.
 * \param[in]  pucMac    Points to the array containing the MAC address.
 * \param[out] pulIpAddr Points to the variable where IP address will be stored.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTarpGetIp(xRTarpDev_t *pxDev,
                          uint8_t     *pucMac,
                          uint32_t    *pulIpAddr);

/*****************************************************************************
 *
 * Returns MAC address associated with given IP address.
 *
 * \param[in]  pxDev     Points to the ARP structure.
 * \param[out] pucMac    Points to the array where the MAC address will be
 *                       stored.
 * \param[in]  ulIpAddr  The IP address.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTarpGetMac(xRTarpDev_t *pxDev,
                           uint8_t     *pucMac,
                           uint32_t     ulIpAddr);

/*****************************************************************************
 *
 * Returns pointer to array containing MAC address associated with given IP
 * address.
 *
 * \param[in]  pxDev     Points to the ARP structure.
 * \param[in]  ulIpAddr  The IP address.
 *
 * \return Returns the pointer to array containing MAC address if succeed,
 *         NULL in other case.
 *
 *****************************************************************************/

uint8_t *pucRTarpGetMacPtr(xRTarpDev_t *pxDev,
                           uint32_t     ulIpAddr);

/*****************************************************************************
 *
 * Change state of device with given MAC addres
 *
 * \param[in] pxDev   Points to the ARP structure.
 * \param[in] pucMac  Points to the array where the MAC address will be stored.
 * \param[in] eState  The device state.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTarpSetState(xRTarpDev_t   *pxDev,
                             uint8_t       *pucMac,
                             eRTarpState_t  eState);

/*****************************************************************************
 *
 * Get state of device with given MAC addres
 *
 * \param[in]  pxDev   Points to the ARP structure.
 * \param[in]  pucMac  Points to the array where the MAC address will be stored.
 * \param[out] eState  Point to the waraible where the device state will be
 *                     stored
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/


BaseType_t  xRTarpGetState(xRTarpDev_t   *pxDev,
                             uint8_t       *pucMac,
                             eRTarpState_t *eState);


#endif /* _RTARP_H_ */
