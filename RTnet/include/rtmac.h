/*****************************************************************************
 *                                                                           *
 *   rtmac.h                                                                 *
 *                                                                           *
 *   RTmac -- real-time media access control framework for RTnet             *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#ifndef _RTMAC_H_
#define _RTMAC_H_

#include <stddef.h>
#include <stdint.h>

#include "FreeRTOS.h"

/* RTnet includes */
#include "tdma.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define rtmacETHERTYPE 0x9021

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

enum eRTMAC_FRAME{
  eRTmacFrameTdma,
  eRTmacFrameTunel,
};
typedef enum eRTMAC_FRAME eRTmacFrame_t;

struct xRTMAC_DEV{
  xTdmaDev_t    xTdma;
};
typedef struct xRTMAC_DEV xRTmacDev_t;

/*****************************************************************************
 * Globals variables
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

/*****************************************************************************
 *
 * Initialize RTmac device.
 *
 * \param[in] pxDev       Points to the RTmac device structure.
 *
 * \return Returns the pdPASS value if succeed initialization, pdFAIL in
 *         other case.
 *
 *****************************************************************************/

BaseType_t  xRTmacInit(xRTmacDev_t  *pxDev);

/*****************************************************************************
 *
 * Process received RTmac frame.
 *
 * \param[in] pxDev        Points to the RTmac device structure.
 * \param[in] pucFrame     The array containing received frame.
 * \param[in] ullTimeStamp The frame time stamp (the time when frame has been
 *                         received).
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTmacProcessFrame(xRTmacDev_t *pxDev,
                                 uint8_t      pucFrame[],
                                 uint64_t     ullTimeStamp);

/*****************************************************************************
 *
 * Send requested frame type.
 *
 * \param[in] pxDev      Points to the RTmac device structure.
 * \param[in] pucFrame   The array containing Ethernet frame.
 * \param[in] ulLen      The length of the Ethernet frame.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTmacSend(xRTmacDev_t *pxDev,
                         uint8_t      pucBuffer[],
                         uint32_t     ulLen);

/*****************************************************************************
 *
 * Get size of RTmac frame header.
 *
 * \return Returns the header size.
 *
 *****************************************************************************/

size_t xRTmacHeaderSize(void);

/*****************************************************************************
 *
 * Add RTmac header to the frame.
 *
 * \param[in] pucFrame    A frame.
 * \param[in] eFrameType  A frame type:
 *                          - eRTmacFrameTdma -- TDMA discipline frame
 *                            (no additional parameters),
 *                          - eRTmacFrameTunel -- tunneling, one additional
 *                            parameter 'ucType' which define tunneled frame
 *                            type.
 *
 * \return Returns the header size.
 *
 *****************************************************************************/

uint32_t ulRTmacHeader(uint8_t pucFrame[], eRTmacFrame_t eFrameType, ...);

/*****************************************************************************
 *
 * Update network devices link status, enable Rx/Tx if link is UP, disable if
 * link is down.
 *
 * \return Returns the pdPASS value if link is UP, pdFAIL if link is down.
 *
 *****************************************************************************/

BaseType_t  xRTmacUpdateLinkStatus(void);

/*****************************************************************************
 *
 * Check if RTmac is ready
 *
 * \param[in] pxDev        Points to the RTmac device structure.
 *
 * \return Returns the pdTRUE value if RTmac is ready, pdFALSE otherwise.
 *
 *****************************************************************************/

BaseType_t  xRTmacIsReady(xRTmacDev_t *pxDev);

#endif /* _RTMAC_H_ */
