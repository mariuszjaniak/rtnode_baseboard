/*****************************************************************************
 *                                                                           *
 *   halTimerInterface.h                                                     *
 *                                                                           *
 *   Timer hardware abstraction layer                                        *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#ifndef _HAL_TIMER_INTERFACE_H
#define _HAL_TIMER_INTERFACE_H

#include <stdint.h>

#include "FreeRTOS.h"

#include "tdma.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

//uint64_t xGetTic(void);

/*****************************************************************************
 *
 * Initialize hardware timers.
 *
 * \param[in] pxTdma Points to the TDMA discipline structure.
 *
 * \return Returns the pdPASS value if succeed initialization, pdFAIL in
 *         other case.
 *
 *****************************************************************************/

BaseType_t  xHalTimerInterfaceInitialise(xTdmaDev_t *pxTdma);

/*****************************************************************************
 *
 * Get global time.
 *
 * \param[out] pullTime Points to the variable where global time will be
 *                      stored.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xHalTimerGet(uint64_t *pullTime);

/*****************************************************************************
 *
 * Set global time, and sync timer with master.
 *
 * \param[in] ullTime            A new global time value.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xHalTimerSet(uint64_t ullTime);

/*****************************************************************************
 *
 * Initialize timer synchronization procedure.
 *
 * \param[in] ullMasterClockTime A master clock timestamp
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xHalTimerSyncInit(uint64_t ullMasterClockTime);

/*****************************************************************************
 *
 * Synchronize timer with master.
 *
 * \param[in] ullMasterClockTime A master clock timestamp
 * \param[in] ullSlaveClockTime  A slave clock timestamp
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xHalTimerSync(uint64_t ullMasterClockTime,
                          uint64_t ullSlaveClockTime);

/*****************************************************************************
 *
 * Set new timer event (TDMA slot event).
 *
 * \param[in] ullTime The moment of time when event will be generated.
 * \param[in] ullLim  The upper bound of the event period.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xHalTimerNewEvent(uint64_t ullTime,
                                uint64_t ullLim);

#endif /* _HAL_TIMER_INTERFACE_H */

