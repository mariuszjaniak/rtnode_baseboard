/************************************************************************
 *                                                                           *
 *   rtnet.c                                                                 *
 *                                                                           *
 *   RTnet -- Real Time networking                                           *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#include <string.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "timers.h"
#include "event_groups.h"

/* RTnet includes */
#include "RTnetConfig.h"
#include "rtnet_inet.h"
#include "rtnet_mem.h"
#include "rtnet_struct.h"
#include "rtnet_private.h"
#include "rtbuf.h"
#include "rtipv4.h"
#include "rtsocket.h"
#include "rtnetDev.h"
#include "rtnet.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define rtnetDEF_TIMER_PERIOD   (configTICK_RATE_HZ)

#define rtnetETHERTYPE_IPV4     rtipv4ETHERTYPE
#define rtnetETHERTYPE_RTMAC    rtmacETHERTYPE
#define rtnetETHERTYPE_RTCFG    rtcfgETHERTYPE

#define rtnetCRC_BYTES          (4UL)

#define rtnetEVENTGROUP_BIT_READY (1 << 0)
#define rtnetEVENTGROUP_BIT_SYNC  (1 << 1)

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

struct xRTNET{
  xRTnetDev_t        xDev;
  xSemaphoreHandle   xSemTx;
  xQueueHandle       xEventQueue;
  EventGroupHandle_t xEventGroup;
  xTaskHandle        xTask;
  xTimerHandle       xTimer;
};
typedef struct xRTNET xRTnet_t;

RTNET_STRUCT_BEGIN();
struct xRTNET_HEADER{
  uint8_t  pucDestination[rtnetETHERNET_MAC_SIZE];
  uint8_t  pucSource[rtnetETHERNET_MAC_SIZE];
  uint16_t usFrameType;
} RTNET_STRUCT_STRUCT;
RTNET_STRUCT_END();
typedef struct xRTNET_HEADER xRTnetHeader_t;

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

static void     prvRTnetTask(void *pvParameters);
static uint32_t prvRTnetHeader(uint8_t  pucFrame[],
                               uint8_t  pucMacS[],
                               uint8_t  pucMacD[],
                               uint16_t usType);
static void     prvTimerCallback(xTimerHandle pxTimer);

/*****************************************************************************
 *  Global variables
 *****************************************************************************/

xRTnetDev_t          *gpxRTnetDev = NULL;
static xRTnet_t       gxRTnet;
static const uint8_t  gpucBroadcast[] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

uint32_t              RTnetCycle;
uint32_t              RTnetState;

/*****************************************************************************
 * Functions implementation
 *****************************************************************************/

void prvRTnetTask(void *pvParameters)
{
  xRTnetEvent_t  xEvent;

  /* Loop, processing RTnet events. */
  for( ;; ){
    /* Wait until there is something to do. */
    RTnetState = 0;
    if(xQueueReceive(gxRTnet.xEventQueue,
                     &xEvent,
                     portMAX_DELAY) == pdPASS){
      RTnetCycle++;
      switch(xEvent.eType){
      case eRTnetEventTxFrame:
        /* This event is handled by prvRTnetSendEvent(...) function */
        break;
      case eRTnetEventTxReady:
        /* This event is handled by prvRTnetSendEvent(...) function */
        break;
      case eRTnetEventReady:
        /* This event is handled by prvRTnetSendEvent(...) function */
        break;
      case eRTnetEventRxIPv4:
        RTnetState |= 1 << 0;
        {
          xRTbuf_t *pxBuf = xEvent.pvData;
          pxBuf->ulIdx += sizeof(xRTnetHeader_t);
          if(xRTipv4ProcessFrame(pxBuf, ulRTnetIpAddr()) == pdFAIL)
            vRTbufRelease(xEvent.pvData);
        }
        gxRTnet.xDev.ulRxFrame++;
        break;
      case eRTnetEventRxRTcfg:
        {
          xRTbuf_t       *pxBuf = xEvent.pvData;
          xRTnetHeader_t *pxHeader;

          RTnetState |= 1 << 1;

          pxHeader = (xRTnetHeader_t*) pxBuf->pucData;
          xRTcfgProcessFrame(&gxRTnet.xDev.xRTcfg,
                             pxBuf->pucData + sizeof(xRTnetHeader_t),
                             pxBuf->ulSize - sizeof(xRTnetHeader_t),
                             pxHeader->pucSource,
                             pxHeader->pucDestination);
        }
        vRTbufRelease(xEvent.pvData);
        gxRTnet.xDev.ulRxFrame++;
        break;
      case eRTnetEventSendAcknowledge:

        RTnetState |= 1 << 2;

        if(xRTcfgSendAcknowledge(&gxRTnet.xDev.xRTcfg) == pdFAIL){
          xRTnetSendEvent(eRTnetEventSendAcknowledge, NULL, 0, portMAX_DELAY);
        }
        break;
      case eRTnetEventSendReady:

        RTnetState |= 1 << 3;

        if(xRTcfgSendReady(&gxRTnet.xDev.xRTcfg) == pdFAIL){
          xRTnetSendEvent(eRTnetEventSendReady, NULL, 0, portMAX_DELAY);
        }
        break;
      case eRTnetEventSendHeartbeat:

        RTnetState |= 1 << 4;

        xRTcfgSendHearbeat(&gxRTnet.xDev.xRTcfg);
        break;
      case eRTnetEventStartHeartbeat:

        RTnetState |= 1 << 5;

        xTimerChangePeriod(gxRTnet.xTimer,
                           RTNET_MS_TO_TIC(gxRTnet.xDev.xRTcfg.usHeartbeat),
                           portMAX_DELAY);
        xTimerStart(gxRTnet.xTimer, portMAX_DELAY);
        break;
      default:
        break;
      }
    }
  }
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTnetInit(uint8_t pucMac[])
{

  RTnetCycle = 0;

  if(pucMac == NULL) return pdFAIL;
  /* Initialize RT buffers */
  if(xRTbufInit() == pdFAIL) return pdFAIL;
  /* Create TX semaphore */
  gxRTnet.xSemTx = xSemaphoreCreateCounting(rtnetconfigTX_QUEUE_LENGTH, 0);
  if(gxRTnet.xSemTx == NULL) return pdFAIL;
  /* Create the event queue */
  gxRTnet.xEventQueue = xQueueCreate(rtnetconfigEVENT_QUEUE_LENGTH,
                                     sizeof(xRTnetEvent_t));
  if(gxRTnet.xEventQueue == NULL){
    /* Clean */
    vSemaphoreDelete(gxRTnet.xSemTx);
    return pdFAIL;
  }
  /* Create event group */
  gxRTnet.xEventGroup = xEventGroupCreate();
  if(gxRTnet.xEventGroup == NULL){
    /* Clean */
    vQueueDelete(gxRTnet.xEventQueue);
    vSemaphoreDelete(gxRTnet.xSemTx);
    return pdFAIL;
  }
  /* Create timer */
  gxRTnet.xTimer = xTimerCreate(NULL,
                                rtnetDEF_TIMER_PERIOD,
                                pdTRUE,
                                0,
                                prvTimerCallback);
  if(gxRTnet.xTimer == NULL){
    /* Clean */
    vEventGroupDelete(gxRTnet.xEventGroup);
    vQueueDelete(gxRTnet.xEventQueue);
    vSemaphoreDelete(gxRTnet.xSemTx);
    return pdFAIL;
  }
  /* Create the task that processes Ethernet and stack events. */
  if(xTaskCreate(prvRTnetTask,
                 NULL,
                 rtnetconfigRTNET_TASK_STACK_SIZE,
                 &gxRTnet.xDev,
                 rtnetconfigRTNET_TASK_PRIORITY,
                 gxRTnet.xTask) == pdFAIL){
    /* Clean */
    xTimerDelete(gxRTnet.xTimer, portMAX_DELAY);
    vEventGroupDelete(gxRTnet.xEventGroup);
    vQueueDelete(gxRTnet.xEventQueue);
    vSemaphoreDelete(gxRTnet.xSemTx);
    return pdFAIL;
  }
  /* Initialize sockets service */
  vRTsocketInit();
  /* Initialize RTnet device */
  if(xRTnetDevInit(&gxRTnet.xDev, pucMac) ==  pdFAIL){
    /* Clean */
    vTaskDelete(gxRTnet.xTask);
    xTimerDelete(gxRTnet.xTimer, portMAX_DELAY);
    vEventGroupDelete(gxRTnet.xEventGroup);
    vQueueDelete(gxRTnet.xEventQueue);
    vSemaphoreDelete(gxRTnet.xSemTx);
    return pdFAIL;
  }
  gpxRTnetDev = &gxRTnet.xDev;
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTnetSend(uint8_t      pucFrame[],
                         uint32_t     ulLen,
                         portTickType xTicksToWait)
{
  if(xRTnetDevIsInit(&gxRTnet.xDev) == pdFALSE) return pdFAIL;
  return xRTnetSendFrame(pucFrame, ulLen, xTicksToWait);
}
/*---------------------------------------------------------------------------*/

xRTnetSocket_t xRTnetSocket(uint32_t ulDomain,
                            uint32_t ulType,
                            uint32_t ulProtocol)
{
  return pxRTsocketOpen(ulDomain, ulType, ulProtocol);
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTnetClosesocket(xRTnetSocket_t xSocket)
{
  return xRTsecketClose(xSocket);
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTnetBind(xRTnetSocket_t          xSocket,
                         const xRTnetSockAddr_t *pxAddr,
                         uint32_t                ulAddrlen)
{
  return xRTsocketBind(xSocket, pxAddr, ulAddrlen);
}
/*---------------------------------------------------------------------------*/

int32_t lRTnetRecvfrom(xRTnetSocket_t    xSocket,
                       void             *pvBuf,
                       size_t            xLen,
                       uint32_t          ulFlags,
                       xRTnetSockAddr_t *pxSrcAddr,
                       uint32_t         *pulAddrlen)
{

  return lRTsocketRecvfrom(xSocket,
                           pvBuf,
                           xLen,
                           ulFlags,
                           pxSrcAddr,
                           pulAddrlen);
}
/*---------------------------------------------------------------------------*/

int32_t lRTnetSendto(xRTnetSocket_t    xSocket,
                     void             *pvBuf,
                     size_t            xLen,
                     uint32_t          ulFlags,
                     xRTnetSockAddr_t *pxDestAddr,
                     uint32_t          ulAddrlen)
{

  return lRTsocketSendto(xSocket,
                         pvBuf,
                         xLen,
                         ulFlags,
                         pxDestAddr,
                         ulAddrlen);
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTnetSetsockopt(xRTnetSocket_t  xSocket,
                               int32_t         lLevel,
                               int32_t         lOptname,
                               const void     *pvOptval,
                               size_t          xOptlen)
{
  return xRTsocketSetsockopt(xSocket,
                             lLevel,
                             lOptname,
                             pvOptval,
                             xOptlen);
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTnetRouteAdd(uint8_t pucMac[], uint32_t ulIpAddr)
{
  return xRTcfgRouteAdd(&gxRTnet.xDev.xRTcfg, pucMac, ulIpAddr);
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTnetRouteDelMac(uint8_t pucMac[])
{
  return xRTcfgRouteDelMac(&gxRTnet.xDev.xRTcfg, pucMac);
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTnetRouteDelIp(uint32_t ulIpAddr)
{
  return xRTcfgRouteDelIp(&gxRTnet.xDev.xRTcfg, ulIpAddr);
}
/*---------------------------------------------------------------------------*/

void *pvRTnetGetUdpDataBuffer(size_t xSize)
{
  return pvRTsocketGetUdpDataBuffer(xSize);
}
/*---------------------------------------------------------------------------*/

void vRTnetReleaseUdpDataBuffer(void *pvData)
{
  vRTsocketReleaseUdpDataBuffer(pvData);
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTnetProcessFrame(xRTbuf_t *pxRTbuf, uint64_t ullTimeStamp)
{
  xRTnetHeader_t *pxHeader = (xRTnetHeader_t*) pxRTbuf->pucData;
  BaseType_t    xReturn  = pdPASS;

  if(xRTnetDevIsInit(&gxRTnet.xDev) == pdFALSE) return pdFAIL;
  switch(rtnet_ntohs(pxHeader->usFrameType)){
  case rtnetETHERTYPE_IPV4:
    if(xRTmacIsReady(&gxRTnet.xDev.xRTmac)){
      /* The frame will be processed by RTnet task. */
      xReturn = xRTnetSendEvent(eRTnetEventRxIPv4,
                                pxRTbuf,
                                sizeof(xRTbuf_t),
                                0);
      if(xReturn == pdFAIL) vRTbufRelease(pxRTbuf);
    }
    else
      vRTbufRelease(pxRTbuf);
    break;
  case rtnetETHERTYPE_RTMAC:
    xReturn = xRTmacProcessFrame(&gxRTnet.xDev.xRTmac,
                                 pxRTbuf->pucData + sizeof(xRTnetHeader_t),
                                 ullTimeStamp);
    vRTbufRelease(pxRTbuf);
    break;
  case rtnetETHERTYPE_RTCFG:
    /* The frame will be processed by RTnet task. */
    xReturn = xRTnetSendEvent(eRTnetEventRxRTcfg,
                              pxRTbuf,
                              sizeof(xRTbuf_t),
                              0);
    if(xReturn == pdFAIL) vRTbufRelease(pxRTbuf);
    break;
  default:
    /* Not RTnet frame */
    vRTbufRelease(pxRTbuf);
    xReturn = pdFAIL;
    break;
  }
  return xReturn;
}
/*---------------------------------------------------------------------------*/

eRTnetFrameClass_t eRTnetCheckFrame(uint8_t pucFrame[])
{
  xRTnetHeader_t *pxHeader = (xRTnetHeader_t*) pucFrame;

  if((memcmp(gpucBroadcast,
             pxHeader->pucDestination,
             rtnetETHERNET_MAC_SIZE) == 0) ||
     (memcmp(gxRTnet.xDev.pucMac,
             pxHeader->pucDestination,
             rtnetETHERNET_MAC_SIZE) == 0)){
    switch(rtnet_ntohs(pxHeader->usFrameType)){
    case rtnetETHERTYPE_RTMAC:
    case rtnetETHERTYPE_RTCFG:
      return eRTnetFrameDiscipline;
    case rtnetETHERTYPE_IPV4:
      return eRTnetFrameIPv4;
    default:
      return eRTnetFrameOther;
    }
  }
  return eRTnetFrameNotRecipient;
}
/*---------------------------------------------------------------------------*/

uint8_t *pucRTnetPrepareFrame(uint32_t      *pulSize,
                              size_t         xLen,
                              uint8_t       *pucMac,
                              eRTnetFrame_t  eFrameType)
{
  uint8_t  *pucFrame = NULL;
  uint32_t  ulSize;

  if(pucMac == NULL) return NULL;
  switch(eFrameType){
  case eRTnetFrameEthernet:
    if(xLen > rtnetETHERNET_MTU_SIZE) return NULL;
    /* Allocate buffer */
    pucFrame = rtnet_malloc(sizeof(xRTnetHeader_t) + xLen);
    if(pucFrame == NULL) return NULL;
    /* Add Ethernet header */
    ulSize = prvRTnetHeader(pucFrame,
                            gxRTnet.xDev.pucMac,
                            pucMac,
                            rtnetETHERTYPE_IPV4);
    if(pulSize != NULL) *pulSize = ulSize;
    break;
  case eRTnetFrameRTmacTdma:
    if(xLen > rtnetETHERNET_MTU_SIZE - xRTmacHeaderSize()) return NULL;
    /* Allocate buffer */
    pucFrame = rtnet_malloc(sizeof(xRTnetHeader_t) +
                            xRTmacHeaderSize() +
                            xLen);
    if(pucFrame == NULL) return NULL;
    /* Add Ethernet header */
    ulSize = prvRTnetHeader(pucFrame,
                            gxRTnet.xDev.pucMac,
                            pucMac,
                            rtnetETHERTYPE_RTMAC);
    if(pulSize != NULL) *pulSize = ulSize;
    /* Add RTmac header */
    ulSize += ulRTmacHeader(pucFrame + ulSize, eRTmacFrameTdma);
    if(pulSize != NULL) *pulSize = ulSize;
    break;
  case eRTnetFrameRTcfg:
    if(xLen > rtnetETHERNET_MTU_SIZE) return NULL;
    /* Allocate buffer */
    pucFrame = rtnet_malloc(sizeof(xRTnetHeader_t) + xLen);
    if(pucFrame == NULL) return NULL;
    /* Add Ethernet header */
    ulSize = prvRTnetHeader(pucFrame,
                            gxRTnet.xDev.pucMac,
                            pucMac,
                            rtnetETHERTYPE_RTCFG);
    if(pulSize != NULL) *pulSize = ulSize;
    break;
  }
  return pucFrame;
}
/*---------------------------------------------------------------------------*/

uint32_t ulRTnetHeader(uint8_t       *pucFrame,
                       uint8_t       *pucMac,
                       eRTnetFrame_t  eFrameType)
{
  if((pucFrame == NULL) || (pucMac == NULL)) return 0;
  switch(eFrameType){
  case eRTnetFrameEthernet:
    return prvRTnetHeader(pucFrame,
                          gxRTnet.xDev.pucMac,
                          pucMac,
                          rtnetETHERTYPE_IPV4);
  case eRTnetFrameRTmacTdma:
    return prvRTnetHeader(pucFrame,
                          gxRTnet.xDev.pucMac,
                          pucMac,
                          rtnetETHERTYPE_RTMAC);
  case eRTnetFrameRTcfg:
    return prvRTnetHeader(pucFrame,
                          gxRTnet.xDev.pucMac,
                          pucMac,
                          rtnetETHERTYPE_RTCFG);
  }
  return 0;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTnetSendEvent(eRTnetEvent_t  eEvent,
                            void          *pvData,
                            uint32_t       ulLen,
                            portTickType   xTicksToWait)
{
  xRTnetEvent_t xEventMessage;

  switch(eEvent){
  case eRTnetEventTxFrame:
    xSemaphoreGive(gxRTnet.xSemTx);
    return pdPASS;
  case eRTnetEventTxReady:
    {
      uint32_t ulIdx;
      for(ulIdx = 0; ulIdx < rtnetconfigTX_QUEUE_LENGTH; ++ulIdx){
        xSemaphoreGive(gxRTnet.xSemTx);
      }
    }
    return pdPASS;
  case eRTnetEventSync:
    xEventGroupSetBits(gxRTnet.xEventGroup, rtnetEVENTGROUP_BIT_SYNC);
    return pdPASS;
  case eRTnetEventReady:
    xEventGroupSetBits(gxRTnet.xEventGroup, rtnetEVENTGROUP_BIT_READY);
    return pdPASS;
  default:
    xEventMessage.eType  = eEvent;
    xEventMessage.pvData = pvData;
    xEventMessage.ulLen  = ulLen;
    return xQueueSendToBack(gxRTnet.xEventQueue, &xEventMessage, xTicksToWait);
  }
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTnetSendFrame(uint8_t      pucFrame[],
                            uint32_t     ulLen,
                            portTickType xTicksToWait)
{
  if(xSemaphoreTake(gxRTnet.xSemTx, xTicksToWait) == pdFALSE) return pdFAIL;
  if(xRTmacSend(&gxRTnet.xDev.xRTmac, pucFrame, ulLen) == pdPASS){
    gxRTnet.xDev.ulTxFrame++;
    return pdPASS;
  }
  return pdFAIL;
  //return xRTmacSend(&gxRTnet.xDev.xRTmac, pucFrame, ulLen);
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTnetWaitRedy(portTickType xTicksToWait)
{
  EventBits_t uxBits;

  uxBits = xEventGroupWaitBits(gxRTnet.xEventGroup,
                               rtnetEVENTGROUP_BIT_READY,
                               pdFALSE,
                               pdFALSE,
                               xTicksToWait);
  if(uxBits & rtnetEVENTGROUP_BIT_READY) return pdPASS;
  return pdFAIL;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTnetWaitSync(portTickType xTicksToWait)
{
  EventBits_t uxBits;

  uxBits = xEventGroupWaitBits(gxRTnet.xEventGroup,
                               rtnetEVENTGROUP_BIT_SYNC,
                               pdTRUE,
                               pdFALSE,
                               xTicksToWait);
  if(uxBits & rtnetEVENTGROUP_BIT_SYNC) return pdPASS;
  return pdFAIL;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTnetUpdateLinkStatus(void)
{
  return xRTmacUpdateLinkStatus();
}
/*---------------------------------------------------------------------------*/


uint8_t *pucRTnetGetMacPtr(uint32_t ulIpAddr)
{
  return pucRTcfgGetMacPtr(&gxRTnet.xDev.xRTcfg, ulIpAddr);
}
/*---------------------------------------------------------------------------*/

uint8_t *pucRTnetMacMaster(void)
{
  return gxRTnet.xDev.xRTcfg.pucMaster;
}
/*---------------------------------------------------------------------------*/

uint8_t *pucRTnetMacBroadcast(void)
{
  return (uint8_t*) gpucBroadcast;
}
/*---------------------------------------------------------------------------*/

uint32_t ulRTnetIpAddr(void)
{
  return gxRTnet.xDev.xRTcfg.ulIpAddr;
}
/*---------------------------------------------------------------------------*/

uint32_t ulRTnetGetIpAddr(void)
{
  return ulRTnetIpAddr();
}
/*---------------------------------------------------------------------------*/

size_t xRTnetHeaderSize(void)
{
  return sizeof(xRTnetHeader_t);
}
/*---------------------------------------------------------------------------*/

uint32_t prvRTnetHeader(uint8_t  pucFrame[],
                        uint8_t  pucMacS[],
                        uint8_t  pucMacD[],
                        uint16_t usType)
{
  xRTnetHeader_t *pxHeader = (xRTnetHeader_t*) pucFrame;

  memcpy(pxHeader->pucSource,
         pucMacS,
         rtnetETHERNET_MAC_SIZE);
  memcpy(pxHeader->pucDestination,
         pucMacD,
         rtnetETHERNET_MAC_SIZE);
  pxHeader->usFrameType = rtnet_htons(usType);
  return sizeof(xRTnetHeader_t);
}
/*---------------------------------------------------------------------------*/

void prvTimerCallback(xTimerHandle pxTimer)
{
  (void) pxTimer;
  xRTnetSendEvent(eRTnetEventSendHeartbeat, NULL, 0, 0);
}
/*---------------------------------------------------------------------------*/
