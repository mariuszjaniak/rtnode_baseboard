/*****************************************************************************
 *                                                                           *
 *   rtipv4.h                                                                *
 *                                                                           *
 *   IPv4 -- real-time Internet Protocol version 4                           *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#include <stdarg.h>
#include <stdint.h>

/* RTnet includes */
#include "rtnet_inet.h"
#include "rtnet_struct.h"
#include "rtudp.h"
#include "rtipv4.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define rtipv4BROADCAST_NETMASK  RTNET_NETMASK_BROADCAST

#define rtipv4IP_VERSION         4
#define rtipv4IP_IHL             5

#define rtipv4IP_DF              0x4000          /* Flag: "Don't Fragment" */
#define rtipv4IP_MF              0x2000          /* Flag: "More Fragments" */
#define rtipv4IP_OFFSET          0x1FFF          /* "Fragment Offset" part */

#define rtipv4IP_PROTOCOL_UDP    rtudpIPPROTOCOL

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

RTNET_STRUCT_BEGIN();
struct xRTIPV4_HEADER{
#ifdef rtnetconfigBYTE_ORDER_LITTLE_ENDIAN
  uint8_t  ucIhl:4,
           ucVersion:4;
#else
  uint8_t  ucVersion:4,
           ucIhl:4;
#endif
  uint8_t  ucTos;
  uint16_t usTotLen;
  uint16_t usId;
  uint16_t usFragOff;
  uint8_t  ucTtl;
  uint8_t  ucProtocol;
  uint16_t usCheck;
  uint32_t ulSaddr;
  uint32_t ulDaddr;
} RTNET_STRUCT_STRUCT;
RTNET_STRUCT_END();
typedef struct xRTIPV4_HEADER xRTipv4Header_t;

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

/*****************************************************************************
 *  Global variables
 *****************************************************************************/

/*****************************************************************************
 * Functions implementation
 *****************************************************************************/

BaseType_t  xRTipv4ProcessFrame(xRTbuf_t *pxBuf, uint32_t ulIpAddr)
{
  uint8_t         *pucFrame = pxBuf->pucData + pxBuf->ulIdx;
  xRTipv4Header_t *pxHeader = (xRTipv4Header_t *) pucFrame;

  {
    uint32_t ulMask;
    uint32_t ulDaddr;
    ulMask  = rtnet_htonl(rtipv4BROADCAST_NETMASK);
    ulDaddr = pxHeader->ulDaddr;
    if((ulDaddr != ulIpAddr) && ((ulDaddr & ulMask) != ulMask)) return pdFAIL;
  }
  if(pxHeader->ucVersion != rtipv4IP_VERSION) return pdFAIL;
  if(pxHeader->ucIhl != rtipv4IP_IHL) return pdFAIL;
  if((rtnet_ntohs(pxHeader->usFragOff) & (rtipv4IP_MF | rtipv4IP_OFFSET)) != 0)
    return pdFAIL;
  if(rtnet_checksum(pucFrame, sizeof(xRTipv4Header_t)) != 0) return pdFAIL;
  pxBuf->ulIdx += sizeof(xRTipv4Header_t);
  switch(pxHeader->ucProtocol){
  case rtipv4IP_PROTOCOL_UDP:
    return xRTudpProcessIPv4Frame(pxBuf, pxHeader->ulSaddr, pxHeader->ulDaddr);
  default:
    return pdFAIL;
  }
  //return pdPASS;
}
/*---------------------------------------------------------------------------*/

uint32_t ulRTipv4DefUdpFrame(uint8_t          *pucFrame,
                             xRTnetSockAddr_t *pxSrcAddr,
                             xRTnetSockAddr_t *pxDstAddr,
                             uint8_t          *pucData,
                             uint16_t          usLen,
                             uint8_t           ucOpt)
{
  xRTipv4Header_t *pxHeader = (xRTipv4Header_t *) pucFrame;
  uint16_t usSize;

  usSize = sizeof(xRTipv4Header_t);
  /* Start with UDP frame because this process overwrite IP header when
   * UDP pseudo header is generated */
  usSize += ulRTudpFrame(pucFrame + sizeof(xRTipv4Header_t),
                         pxSrcAddr,
                         pxDstAddr,
                         pucData,
                         usLen,
                         ucOpt);
  /* Fill IP header */
  pxHeader->ucVersion  = rtipv4IP_VERSION;
  pxHeader->ucIhl      = rtipv4IP_IHL;
  pxHeader->ucTos      = 0;
  pxHeader->usTotLen   = rtnet_htons(usSize);
  pxHeader->usId       = 0;
  pxHeader->usFragOff  = 0;
  pxHeader->ucTtl      = 128;
  pxHeader->ucProtocol = rtipv4IP_PROTOCOL_UDP;
  pxHeader->usCheck    = 0;
  pxHeader->ulSaddr    = pxSrcAddr->sin_addr;
  pxHeader->ulDaddr    = pxDstAddr->sin_addr;
  pxHeader->usCheck    = rtnet_checksum(pucFrame, sizeof(xRTipv4Header_t));
  return usSize;
}
/*---------------------------------------------------------------------------*/

size_t xRTipv4UdpSize(size_t xLen)
{
  return sizeof(xRTipv4Header_t) + xRTudpSize(xLen);
}
/*---------------------------------------------------------------------------*/

size_t xRTipv4HeaderSize(void)
{
  return sizeof(xRTipv4Header_t);
}
/*---------------------------------------------------------------------------*/


