/*****************************************************************************
 *                                                                           *
 *   tdmaCfg.c                                                               *
 *                                                                           *
 *   TDMA discipline configuration service                                   *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#include <stdlib.h>
#include <string.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"

/* RTnet includes */
#include "tdmaCfg.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

static BaseType_t  prvStr2uint32(char     *str,
                                   uint32_t *ulVal,
                                   uint32_t  ulMin,
                                   uint32_t  ulMax);
static xTdmaSlot_t  *prvTdmaCfgFindId(uint32_t ulId);
static int           prvTdmaCfgCompare(const void *pvArg1, const void *pvArg2);

/*****************************************************************************
 *  Global variables
 *****************************************************************************/

xTdmaCfg_t *gpxTdmaCfg = NULL;

/*****************************************************************************
 * Functions implementation
 *****************************************************************************/

BaseType_t  xTdmaCfgInit(xTdmaCfg_t *pxCfg)
{
  if(pxCfg == NULL) return pdFAIL;
  memset(pxCfg, 0, sizeof(xTdmaCfg_t));
  /* Set default max cycle length */
  pxCfg->ullCycle = rtnetconfigTDMA_CYCLE_MAX;
  /* Register TDMA configuration structure */
  gpxTdmaCfg = pxCfg;
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xTdmaCfgAdd(xTdmaSlot_t *pxSlot)
{
  uint32_t ulLen;

  ulLen = gpxTdmaCfg->ulLen;
  if(ulLen >= rtnetconfigTDMA_SLOTS_NUMBER) return pdFAIL;
  if((pxSlot->ulPhasing < 1) || (pxSlot->ulPeriod < 1) ||
      pxSlot->ulPhasing > pxSlot->ulPeriod) return pdFAIL;
  if((pxSlot->ulSize < tdmacfgMIN_SLOT_SIZE) ||
     (pxSlot->ulSize > tdmacfgMAX_SLOT_SIZE)) return pdFAIL;
  if(prvTdmaCfgFindId(pxSlot->ulId) != NULL) return pdFAIL;
  taskENTER_CRITICAL();
  {
    memcpy(gpxTdmaCfg->pxSlots + ulLen, pxSlot, sizeof(xTdmaSlot_t));
    ulLen++;
    if(ulLen > 1){
      qsort(gpxTdmaCfg->pxSlots, ulLen, sizeof(xTdmaSlot_t), prvTdmaCfgCompare);
    }
    gpxTdmaCfg->ulLen = ulLen;
    if(pxSlot->ulSize > gpxTdmaCfg->ulMaxSize)
      gpxTdmaCfg->ulMaxSize = pxSlot->ulSize;
  }
  taskEXIT_CRITICAL();
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xTdmaCfgCmd(int argc, char *argv[])
{
  xTdmaSlot_t xSlot;
  uint32_t    ulPom;
  int         i;

  if(gpxTdmaCfg == NULL) return pdFAIL;
  if(argc < 5) return pdFAIL;
  if(strcmp(argv[2], "slot") != 0) return pdFAIL;
  memset(&xSlot, 0, sizeof(xTdmaSlot_t));
  xSlot.ulPhasing = 1;
  xSlot.ulPeriod  = 1;
  xSlot.ulSize    = tdmacfgMAX_SLOT_SIZE;
  if(prvStr2uint32(argv[3],
                   &xSlot.ulId,
                   0,
                   rtnetconfigTDMA_SLOTS_NUMBER) == pdFAIL) return pdFAIL;
  if(prvStr2uint32(argv[4], &ulPom, 0, 0xFFFFFFFF) == pdFAIL)
    return pdFAIL;
  xSlot.ullOffset = ((uint64_t) ulPom) * 1000ULL;
  for (i = 5; i < argc; i++){
    if(strcmp(argv[i], "-p") == 0){
      char *ptr;
      if(++i > argc) return pdFAIL;
      ptr = strchr(argv[i], '/');
      if(ptr == NULL) return pdFAIL;
      *ptr++ = '\0';
      if(prvStr2uint32(ptr,
                       &xSlot.ulPeriod,
                       1,
                       rtnetconfigTDMA_SLOTS_NUMBER) == pdFAIL) return pdFAIL;
      if(prvStr2uint32(argv[i],
                       &xSlot.ulPhasing,
                       1,
                       xSlot.ulPeriod) == pdFAIL) return pdFAIL;
    }
    else if(strcmp(argv[i], "-s") == 0){
      if(++i > argc) return pdFAIL;
      if(prvStr2uint32(argv[i],
                       &xSlot.ulSize,
                       tdmacfgMIN_SLOT_SIZE,
                       tdmacfgMAX_SLOT_SIZE) == pdFAIL)
        return pdFAIL;
    }
    else return pdFAIL;
  }
  return xTdmaCfgAdd(&xSlot);
}
/*---------------------------------------------------------------------------*/

BaseType_t  xTdmaCfgHasSlot(void)
{
  if(gpxTdmaCfg == NULL) return pdFAIL;
  if(gpxTdmaCfg->ulLen > 0) return pdPASS;
  return pdFAIL;
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvStr2uint32(char     *str,
                          uint32_t *ulVal,
                          uint32_t  ulMin,
                          uint32_t  ulMax)
{
  uint32_t  ulData;
  char     *pom;

  ulData = strtoul(str, &pom, 10);
  if((*pom != '\0') || (pom == str)) return pdFAIL;
  if((ulData < ulMin) || (ulData > ulMax)) return pdFAIL;
  *ulVal = ulData;
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

xTdmaSlot_t *prvTdmaCfgFindId(uint32_t ulId)
{
  uint32_t ulIdx;

  for(ulIdx = 0; ulIdx < gpxTdmaCfg->ulLen; ulIdx++){
    xTdmaSlot_t *pxSlot;
    pxSlot = gpxTdmaCfg->pxSlots + ulIdx;
    if(pxSlot->ulId == ulId) return pxSlot;
  }
  return NULL;
}
/*---------------------------------------------------------------------------*/

int prvTdmaCfgCompare(const void *pvArg1, const void *pvArg2)
{
    const xTdmaSlot_t *pxArg1 = pvArg1;
    const xTdmaSlot_t *pxArg2 = pvArg2;

    if(pxArg1->ullOffset < pxArg2->ullOffset) return -1;
    else if(pxArg1->ullOffset == pxArg2->ullOffset) return 0;
    else return 1;
}
/*---------------------------------------------------------------------------*/
