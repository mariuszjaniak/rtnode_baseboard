/*****************************************************************************
 *                                                                           *
 *   serializer.c                                                            *
 *                                                                           *
 *   Data serialization infrastructure                                       *
 *                                                                           *
 *   Copyright (C) 2014 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.wroc.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#include <string.h>

#include "serializer.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define SERIALIZER_PREAMBLE     0xA5A5
#define SERIALIZER_PROTOCOL_LEN 4

#define PACKED_STRUCT __attribute__( (packed) )

#if defined RTNET
#  include "rtnet_inet.h"
#  define htonll(x) rtnet_htonll(x)
#  define ntohll(x) rtnet_ntohll(x)
#  define htonl(x)  rtnet_htonl(x)
#  define ntohl(x)  rtnet_ntohl(x)
#  define htons(x)  rtnet_htons(x)
#  define ntohs(x)  rtnet_ntohs(x)
#elif defined __linux__
#  if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#    define htonll(x) (x)
#    define ntohll(x) (x)
#    define htonl(x)  (x)
#    define ntohl(x)  (x)
#    define htons(x)  (x)
#    define ntohs(x)  (x)
#  elif __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#    include <byteswap.h>
#    define htonll(x) bswap_64(x)
#    define ntohll(x) bswap_64(x)
#    define htonl(x)  bswap_32(x)
#    define ntohl(x)  bswap_32(x)
#    define htons(x)  bswap_16(x)
#    define ntohs(x)  bswap_16(x)
#  else
#  error "Unknown byte order"
#  endif
#endif

#define pack754_32(f) (pack754((f), 32, 8))
#define pack754_64(f) (pack754((f), 64, 11))
#define unpack754_32(i) (unpack754((i), 32, 8))
#define unpack754_64(i) (unpack754((i), 64, 11))

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

struct sSERIALIZER_HEADER{
  uint16_t preamble;
  uint8_t  protocol[SERIALIZER_PROTOCOL_LEN];
  uint8_t  id[SERIALIZER_ID_LEN];
  uint16_t len;
} PACKED_STRUCT;
typedef struct sSERIALIZER_HEADER sSerializerHeader_t;

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

static uint64_t    pack754(long double f, unsigned bits, unsigned expbits);
static long double unpack754(uint64_t i, unsigned bits, unsigned expbits);

/*****************************************************************************
 * Globals variables
 *****************************************************************************/

static const uint8_t gProtocol[SERIALIZER_PROTOCOL_LEN] = {
    'S',
    'E',
    'R',
    ':'
};

/*****************************************************************************
 * Function implementation
 *****************************************************************************/

uint64_t pack754(long double f, unsigned bits, unsigned expbits)
{
  long double fnorm;
  int         shift;
  long long   sign, exp, significand;
  unsigned    significandbits = bits - expbits - 1; // -1 for sign bit

  if (f == 0.0) return 0; // get this special case out of the way

  // check sign and begin normalization
  if (f < 0) { sign = 1; fnorm = -f; }
  else { sign = 0; fnorm = f; }

  // get the normalized form of f and track the exponent
  shift = 0;
  while(fnorm >= 2.0) { fnorm /= 2.0; shift++; }
  while(fnorm < 1.0) { fnorm *= 2.0; shift--; }
  fnorm = fnorm - 1.0;

  // calculate the binary form (non-float) of the significand data
  significand = fnorm * ((1LL<<significandbits) + 0.5f);

  // get the biased exponent
  exp = shift + ((1<<(expbits-1)) - 1); // shift + bias

  // return the final answer
  return (sign<<(bits-1)) | (exp<<(bits-expbits-1)) | significand;
}

long double unpack754(uint64_t i, unsigned bits, unsigned expbits)
{
  long double result;
  long long shift;
  unsigned bias;
  unsigned significandbits = bits - expbits - 1; // -1 for sign bit

  if (i == 0) return 0.0;

  // pull the significand
  result = (i&((1LL<<significandbits)-1)); // mask
  result /= (1LL<<significandbits); // convert back to float
  result += 1.0f; // add the one back on

  // deal with the exponent
  bias = (1<<(expbits-1)) - 1;
  shift = ((i>>significandbits)&((1LL<<expbits)-1)) - bias;
  while(shift > 0) { result *= 2.0; shift--; }
  while(shift < 0) { result /= 2.0; shift++; }

  // sign it
  result *= (i>>(bits-1))&1? -1.0: 1.0;

  return result;
}

int serializerInit(sSerializer_t *dev, uint8_t *data, uint32_t size)
{
#ifdef DEBUG
  if((dev == NULL) || ((data == NULL) && (size > 0))) return SERIALIZER_ERR;
#endif
  dev->data = data;
  dev->size = size;
  dev->len  = 0;
  return SERIALIZER_OK;
}

int serializerHeaderAdd(sSerializer_t *dev, const uint8_t *id)
{
  int len;

#ifdef DEBUG
  if(dev == NULL) return SERIALIZER_ERR;
#endif
  len = sizeof(sSerializerHeader_t);
  if(dev->data != NULL){
    if(dev->size < dev->len + len) return SERIALIZER_ERR;
    sSerializerHeader_t *header = (sSerializerHeader_t*) dev->data;
    header->preamble = htons(SERIALIZER_PREAMBLE);
    memcpy(header->protocol, gProtocol, SERIALIZER_PROTOCOL_LEN);
    if(id == NULL) memset(header->id, 0, SERIALIZER_ID_LEN);
    else memcpy(header->id, id, SERIALIZER_ID_LEN);
    header->len = 0;
  }
  dev->len += len;
  return len;
}

int serializerUInt8Add(sSerializer_t *dev, uint8_t data)
{
  int len;

#ifdef DEBUG
  if(dev == NULL) return SERIALIZER_ERR;
#endif
  len = sizeof(data);
  if(dev->data != NULL){
    if(dev->size < dev->len + len) return SERIALIZER_ERR;
    dev->data[dev->len] = data;
  }
  dev->len += len;
  return len;
}

int serializerSInt8Add(sSerializer_t *dev, int8_t data)
{
  return serializerUInt8Add(dev, (uint8_t) data);
}

int serializerUInt16Add(sSerializer_t *dev, uint16_t data)
{
  int len;

#ifdef DEBUG
  if(dev == NULL) return SERIALIZER_ERR;
#endif
  len = sizeof(data);
  if(dev->data != NULL){
    if(dev->size < dev->len + len) return SERIALIZER_ERR;
    data = htons(data);
    memcpy(dev->data + dev->len, &data, len);
  }
  dev->len += len;
  return len;
}

int serializerSInt16Add(sSerializer_t *dev, int16_t data)
{
  return serializerUInt16Add(dev, (uint16_t) data);
}

int serializerUInt32Add(sSerializer_t *dev, uint32_t data)
{
  int len;

#ifdef DEBUG
  if(dev == NULL) return SERIALIZER_ERR;
#endif
  len = sizeof(data);
  if(dev->data != NULL){
    if(dev->size < dev->len + len) return SERIALIZER_ERR;
    data = htonl(data);
    memcpy(dev->data + dev->len, &data, len);
  }
  dev->len += len;
  return len;
}

int serializerSInt32Add(sSerializer_t *dev, int32_t data)
{
  return serializerUInt32Add(dev, (uint32_t) data);
}

int serializerUInt64Add(sSerializer_t *dev, uint64_t data)
{
  int len;

#ifdef DEBUG
  if(dev == NULL) return SERIALIZER_ERR;
#endif
  len = sizeof(data);
  if(dev->data != NULL){
    if(dev->size < dev->len + len) return SERIALIZER_ERR;
    data = htonll(data);
    memcpy(dev->data + dev->len, &data, len);
  }
  dev->len += len;
  return len;
}

int serializerSInt64Add(sSerializer_t *dev, int64_t data)
{
  return serializerUInt64Add(dev, (uint64_t) data);
}

int serializerFloatAdd(sSerializer_t *dev, float data)
{
  int len;

#ifdef DEBUG
  if(dev == NULL) return SERIALIZER_ERR;
#endif
  len = sizeof(uint32_t);
  if(dev->data != NULL){
    if(dev->size < dev->len + len) return SERIALIZER_ERR;
    uint32_t pom;
    pom = pack754_32(data);
    memcpy(dev->data + dev->len, &pom, len);
  }
  dev->len += len;
  return len;
}

int serializerDoubleAdd(sSerializer_t *dev, double data)
{
  int len;

#ifdef DEBUG
  if(dev == NULL) return SERIALIZER_ERR;
#endif
  len = sizeof(uint64_t);
  if(dev->data != NULL){
    if(dev->size < dev->len + len) return SERIALIZER_ERR;
    uint64_t pom;
    pom = pack754_64(data);
    memcpy(dev->data + dev->len, &pom, len);
  }
  dev->len += len;
  return len;
}

int serializerReadyAdd(sSerializer_t *dev)
{
#ifdef DEBUG
  if(dev == NULL) return SERIALIZER_ERR;
#endif
  if(dev->data != NULL){
    sSerializerHeader_t *header = (sSerializerHeader_t*) dev->data;
    header->len = htons(dev->len - sizeof(sSerializerHeader_t));
  }
  return dev->len;
}

int serializerHeaderGet(sSerializer_t *dev, uint8_t *id, uint16_t *size)
{
  int len;

#ifdef DEBUG
  if(dev == NULL) return SERIALIZER_ERR;
#endif
  len = sizeof(sSerializerHeader_t);
  if(dev->data != NULL){
    if(dev->size < dev->len + len) return SERIALIZER_ERR;
    sSerializerHeader_t *header = (sSerializerHeader_t*) dev->data;
    if(ntohs(header->preamble) != SERIALIZER_PREAMBLE) return SERIALIZER_ERR;
    if(memcmp(header->protocol, gProtocol, SERIALIZER_PROTOCOL_LEN) != 0)
      return SERIALIZER_ERR;
    if(id != NULL) memcpy(id, header->id, SERIALIZER_ID_LEN);
    if(size != NULL) *size = ntohs(header->len);
  }
  dev->len += len;
  return len;
}

int serializerUInt8Get(sSerializer_t *dev, uint8_t *data)
{
  int len;

#ifdef DEBUG
  if(dev == NULL) return SERIALIZER_ERR;
#endif
  len = sizeof(*data);
  if((dev->data != NULL) && (data != NULL)){
    if(dev->size < dev->len + len) return SERIALIZER_ERR;
    *data = dev->data[dev->len];
  }
  dev->len += len;
  return len;
}

int serializerSInt8Get(sSerializer_t *dev, int8_t *data)
{
  return serializerUInt8Get(dev, (uint8_t*)data);
}

int serializerUInt16Get(sSerializer_t *dev, uint16_t *data)
{
  int len;

#ifdef DEBUG
  if(dev == NULL) return SERIALIZER_ERR;
#endif
  len = sizeof(*data);
  if((dev->data != NULL) && (data != NULL)){
    if(dev->size < dev->len + len) return SERIALIZER_ERR;
    uint16_t tmp;
    memcpy(&tmp, dev->data + dev->len, len);
    *data = ntohs(tmp);
  }
  dev->len += len;
  return len;
}

int serializerSInt16Get(sSerializer_t *dev, int16_t *data)
{
  return serializerUInt16Get(dev, (uint16_t*)data);
}

int serializerUInt32Get(sSerializer_t *dev, uint32_t *data)
{
  int len;

#ifdef DEBUG
  if(dev == NULL) return SERIALIZER_ERR;
#endif
  len = sizeof(*data);
  if((dev->data != NULL) && (data != NULL)){
    if(dev->size < dev->len + len) return SERIALIZER_ERR;
    uint32_t tmp;
    memcpy(&tmp, dev->data + dev->len, len);
    *data = ntohl(tmp);
  }
  dev->len += len;
  return len;
}

int serializerSInt32Get(sSerializer_t *dev, int32_t *data)
{
  return serializerUInt32Get(dev, (uint32_t*)data);
}

int serializerUInt64Get(sSerializer_t *dev, uint64_t *data)
{
  int len;

#ifdef DEBUG
  if(dev == NULL) return SERIALIZER_ERR;
#endif
  len = sizeof(*data);
  if((dev->data != NULL) && (data != NULL)){
    if(dev->size < dev->len + len) return SERIALIZER_ERR;
    uint64_t tmp;
    memcpy(&tmp, dev->data + dev->len, len);
    *data = ntohll(tmp);
  }
  dev->len += len;
  return len;
}

int serializerSInt64Get(sSerializer_t *dev, int64_t *data)
{
  return serializerUInt64Get(dev, (uint64_t*)data);
}

int serializerFloatGet(sSerializer_t *dev, float *data)
{
  int len;

#ifdef DEBUG
  if(dev == NULL) return SERIALIZER_ERR;
#endif
  len = sizeof(uint32_t);
  if((dev->data != NULL) && (data != NULL)){
    if(dev->size < dev->len + len) return SERIALIZER_ERR;
    uint32_t tmp;
    memcpy(&tmp, dev->data + dev->len, len);
    *data = unpack754_32(tmp);
  }
  dev->len += len;
  return len;
}

int serializerDoubleGet(sSerializer_t *dev, double *data)
{
  int len;

#ifdef DEBUG
  if(dev == NULL) return SERIALIZER_ERR;
#endif
  len = sizeof(uint64_t);
  if((dev->data != NULL) && (data != NULL)){
    if(dev->size < dev->len + len) return SERIALIZER_ERR;
    uint64_t tmp;
    memcpy(&tmp, dev->data + dev->len, len);
    *data = unpack754_64(tmp);
  }
  dev->len += len;
  return len;
}

int serializerReadyGet(sSerializer_t *dev)
{
#ifdef DEBUG
  if(dev == NULL) return SERIALIZER_ERR;
#endif
  if(dev->data != NULL){
    uint16_t len;
    sSerializerHeader_t *header = (sSerializerHeader_t*) dev->data;
    len = ntohs(header->len);
    if(len != dev->len - sizeof(sSerializerHeader_t)) return SERIALIZER_ERR;
  }
  return dev->len;
}
