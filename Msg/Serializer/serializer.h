/*****************************************************************************
 *                                                                           *
 *   serializer.h                                                            *
 *                                                                           *
 *   Data serialization infrastructure                                       *
 *                                                                           *
 *   Copyright (C) 2014 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.wroc.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#ifndef _SERIALIZER_H_
#define _SERIALIZER_H_

#include <stddef.h>
#include <stdint.h>

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define SERIALIZER_ID_LEN  16

#define SERIALIZER_OK      0
#define SERIALIZER_ERR    -1

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

struct sSERIALIZER{
  uint8_t  *data;
  uint32_t  size;
  uint32_t  len;
};
typedef struct sSERIALIZER sSerializer_t;

/*****************************************************************************
 * Globals variables
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

int serializerInit(sSerializer_t *dev, uint8_t *data, uint32_t size);

int serializerHeaderAdd(sSerializer_t *dev, const uint8_t *id);
int serializerSInt8Add(sSerializer_t *dev, int8_t data);
int serializerUInt8Add(sSerializer_t *dev, uint8_t data);
int serializerSInt16Add(sSerializer_t *dev, int16_t data);
int serializerUInt16Add(sSerializer_t *dev, uint16_t data);
int serializerSInt32Add(sSerializer_t *dev, int32_t data);
int serializerUInt32Add(sSerializer_t *dev, uint32_t data);
int serializerSInt64Add(sSerializer_t *dev, int64_t data);
int serializerUInt64Add(sSerializer_t *dev, uint64_t data);
int serializerFloatAdd(sSerializer_t *dev, float data);
int serializerDoubleAdd(sSerializer_t *dev, double data);
int serializerReadyAdd(sSerializer_t *dev);

int serializerHeaderGet(sSerializer_t *dev, uint8_t *id, uint16_t *size);
int serializerUInt8Get(sSerializer_t *dev, uint8_t *data);
int serializerSInt8Get(sSerializer_t *dev, int8_t *data);
int serializerUInt16Get(sSerializer_t *dev, uint16_t *data);
int serializerSInt16Get(sSerializer_t *dev, int16_t *data);
int serializerUInt32Get(sSerializer_t *dev, uint32_t *data);
int serializerSInt32Get(sSerializer_t *dev, int32_t *data);
int serializerUInt64Get(sSerializer_t *dev, uint64_t *data);
int serializerSInt64Get(sSerializer_t *dev, int64_t *data);
int serializerFloatGet(sSerializer_t *dev, float *data);
int serializerDoubleGet(sSerializer_t *dev, double *data);
int serializerReadyGet(sSerializer_t *dev);

#endif /* _SERIALIZER_H_ */
