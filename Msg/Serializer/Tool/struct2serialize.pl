#!/usr/bin/perl
use strict;

##
# A script to build header and source file for a serializer
# 
# 
# Copyright(C) 2014 by Janusz Jakubiak
# 

#TODO: Header
# license
# 
# parse serializer to get map automaticaly?
# 

my %opis;
#definition of mapping
$opis{SerializerMap}=q|
int8_t SInt8
uint8_t UInt8 
int16_t SInt16
uint16_t UInt16
int32_t SInt32 
uint32_t UInt32
int64_t SInt64 
uint64_t UInt64
float Float
double Double 
|;

use Getopt::Long;
my %options=('template'=>'template');
Getopt::Long::Configure ('bundling');
GetOptions (\%options,'config|c=s' , 'template|t:s' ,
                'help|h|?');

# no arguments and no config defined
#if ($#ARGV<0 and ! defined $options{config}) {
if ( ! defined $options{config} and ! defined $options{help}) {
  print qq%Run "$0 [-h|--help|-?]" to get information on usage
%;
  exit 0;
} 

$options{help}=1 if (! -r $options{config});

die "No template files!\n" if (! -r $options{template}.'.c' or ! -r $options{template}.'.h' );

if (defined $options{help} ) {
print qq%
Usage: 
$0 -c|--config configfile [-t|--template templatebasename]

 templatebasename (default template): there must exist 
  templatebasename.c and templatebasename.h files

 configfile:  
  StructName=NameOfStr
   will create a structure, .c and .h files with that name

  StructDef=<<END
  structure content
  END
   definition of structure, ending "END" must be at the beginning 
   of line; no complex data types are allowed (only single word 
   type names); no comments are allowed;

  StructDescription=<<END
   description
  END
   text of the description to be put in .c and .h comments
%;
exit 0;
}


sub define_struct() {
  my $s='';
  $s.="struct s$opis{STRUCTNAME} {\n";
  $s.="$opis{StructDef}";
  $s.="};\ntypedef struct s$opis{STRUCTNAME} s$opis{StructName}_t;\n";
}

sub process_SerializerMap{
  my ($t,$T);
  my @items=split('^',$opis{SerializerMap});
  for my $i (@items) {
    if ($i=~/\s*(\w+)\s+(\w+)\b/) {
      $t=$1;$T=$2;
      $opis{MAP}{"$t"}="$T";
    }

}
}

sub process_StructDef() {
  my ($type,$var);
  my @items=split('\s*;\s*',$opis{StructDef});
  for my $i (@items) {
    if ($i=~/\s*(\w+)\s+(\w+)\b/) {
      $type=$1; $var=$2;
      if (!$opis{MAP}{$1}) {
	print STDERR "No mapping defined for $1 -> skipping\n";
	next;
      }
      my $btype=$opis{MAP}{$type};
      $opis{STRUCTSERIALIZEBODY}.="if(serializer${btype}Add(&ser,  msg->$var)  < 0) return -1;\n";
      $opis{STRUCTDESERIALIZEBODY}.="if(serializer${btype}Get(&ser,  &msg->$var)  < 0) return -1;\n";
    }
  }
}

sub process_file() {
  while (<IN>) {
    my $line=$_;
    #simple substitutions
    s/\$\{StructName\}/$opis{StructName}/g;
    s/\$\{StructName15\}/$opis{StructName15}/g;
    s/\$\{STRUCTNAME\}/$opis{STRUCTNAME}/g;
    s/\$\{structName\}/$opis{structName}/g;
    s/\$\{CONFIG\}/$options{config}/g;

    s/STRUCTDESCRIPTION//; #a moze usunac cala linie?

    print OUT;
    #additions
    if ($line=~/STRUCTSERIALIZEBODY/) {
      print OUT $opis{STRUCTSERIALIZEBODY}."\n";
    }
    if ($line=~/STRUCTDESERIALIZEBODY/) {
      print OUT $opis{STRUCTDESERIALIZEBODY}."\n";
    }
    if ($line=~/STRUCTDESCRIPTION/) {
      print OUT $opis{StructDescription}."\n";
    }
    if ($line=~/STRUCTFULLDEF/) {
      print OUT $opis{STRUCTFULLDEF}."\n";
    }
    #print OUT $line;
  }
}


#test if files present
open (CFG,"<$options{config}") or die "Cannot open $options{config}\n";

while (<CFG>) {
  if (/StructName\s*=\s*(\w+)\b/) { $opis{StructName}="$1";  }
  if (/^\s*(\w+)\s*=\s*<<(\w+)\b/) {
    my ($var,$end,$s)=($1,$2,''); 
    while (<CFG>) {
      if (/^$end/) {
	last;
      }else{
	$s.=$_ ;
      }
    }
    $opis{"$var"}="$s";
    $s='';
  }
}

#checking if required fields were defined in config
if (!$opis{StructDef} or !$opis{StructName}) { 
  print "Required fields: StructName and StructDef\nNothing to be done\n";
  exit 0;
}

#defining optional fields
$opis{StructDescription}='' if (!$opis{StructDescription}); #if no description defined

$opis{STRUCTNAME}=uc($opis{StructName});
$opis{StructName15}=substr($opis{StructName},0,15);
$opis{structName}=lc(substr($opis{StructName},0,1)).substr($opis{StructName},1);

$opis{STRUCTSERIALIZEBODY}='';
$opis{STRUCTDESERIALIZEBODY}='';
$opis{STRUCTFULLDEF}='';
$opis{MAP}=();
#/* STRUCTSERIALIZEBODY */
#/* STRUCTDESERIALIZEBODY */
#/* STRUCTFULLDEF */


#convert definition of SerializerMap to hash table $opis{MAP}
if ($opis{SerializerMap}) {
  process_SerializerMap();
}
$opis{STRUCTFULLDEF}=define_struct();


process_StructDef();

##debug: print structures
#for my $k (keys %opis) {
#  print "$k => $opis{$k}\n";
#}
#print define_struct()."\n";

print "Processing template header file:template.h";
open(IN,"<template.h") or die "Cannot open input file!\n";
open(OUT,">$opis{structName}.h") or die "Cannot open output $opis{structName}.h!\n";
process_file();
close IN;close OUT;
print "\nDone - $opis{structName}.h created\n";
print "Processing template source file:template.c";
open(IN,"<template.c") or die "Cannot open!\n";
open(OUT,">$opis{structName}.c") or die "Cannot open output $opis{structName}.c!\n";
process_file();
close IN;close OUT;
print "\nDone - $opis{structName}.c created\n";
