/*****************************************************************************
 *                                                                           *
 *   ${structName}.c                                                         *
 *                                                                           *
 *   This file was autogenerated from ${CONFIG}                              *
 *   STRUCTDESCRIPTION                                                       *
 *                                                                           *
 *   Copyright (C) 2014 by <name>                                            *
 *   <email>                                                                 *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#include <string.h>

#include "${structName}.h"
#include "serializer.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

static int _${structName}Serialize(s${StructName}_t *msg, uint8_t *buf, uint32_t size);
static int _${structName}IsSerialized(sSerializer_t *ser);

/*****************************************************************************
 * Globals variables
 *****************************************************************************/

static const uint8_t gId[SERIALIZER_ID_LEN] = "${StructName15}";

/*****************************************************************************
 * Function implementaion
 *****************************************************************************/

int _${structName}Serialize(s${StructName}_t *msg, uint8_t *buf, uint32_t size)
{
  sSerializer_t ser;

  serializerInit(&ser, buf, size);
  if(serializerHeaderAdd(&ser, gId)      < 0) return -1;
  /* STRUCTSERIALIZEBODY */
  return serializerReadyAdd(&ser);
}

int _${structName}IsSerialized(sSerializer_t *ser)
{
  uint8_t id[SERIALIZER_ID_LEN];

  if(serializerHeaderGet(ser, id, NULL) < 0) return -1;
  if(memcmp(gId, id, SERIALIZER_ID_LEN) != 0) return -1;
  return 1;
}

int ${structName}Serialize(s${StructName}_t *msg, uint8_t *buf, uint32_t size)
{
  return _${structName}Serialize(msg, buf, size);
}

int ${structName}DeSerialize(s${StructName}_t *msg, uint8_t *buf, uint32_t size)
{
  sSerializer_t ser;

  serializerInit(&ser, buf, size);
  if(_${structName}IsSerialized(&ser)           < 0) return -1;
  /* STRUCTDESERIALIZEBODY */
  return serializerReadyGet(&ser);
}

int ${structName}IsSerialized(uint8_t *buf, uint32_t size)
{
  sSerializer_t ser;

  serializerInit(&ser, buf, size);
  return _${structName}IsSerialized(&ser);
}

int ${structName}SerializeSize(void)
{
  s${StructName}_t msg;
  return _${structName}Serialize(&msg, NULL, 0);
}

