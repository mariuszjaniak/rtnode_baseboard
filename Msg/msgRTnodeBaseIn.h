/*****************************************************************************
 *                                                                           *
 *   msgRTnodeBaseIn.h                                                         *
 *                                                                           *
 *   This file was autogenerated from ../../msgRTnodeBaseIn.cfg                              *
 *                                                          *
Message containing input signals of the RTnode base board.  

 *                                                                           *
 *   Copyright (C) 2014 by <name>                                            *
 *   <email>                                                                 *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#ifndef _MSGRTNODEBASEIN_H_
#define _MSGRTNODEBASEIN_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

/*****************************************************************************
 * Constants
 *****************************************************************************/

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

/* STRUCTFULLDEF */
struct sMSGRTNODEBASEIN {
  uint8_t  id;
  uint8_t  gid;
  uint16_t ch00;
  uint16_t ch01;
  uint16_t ch02;
  uint16_t ch03;
  uint8_t  i0;
  uint8_t  i1;
  uint16_t pos0;
  uint16_t tic0;
  uint32_t t0;
  int8_t   dir0;
  uint32_t err0;
  uint16_t pos1;
  uint16_t tic1;
  uint32_t t1;
  int8_t   dir1;
  uint32_t err1;
};
typedef struct sMSGRTNODEBASEIN sMsgRTnodeBaseIn_t;


/*****************************************************************************
 * Globals variables
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

int msgRTnodeBaseInSerialize(sMsgRTnodeBaseIn_t *msg, uint8_t *buf, uint32_t size);
int msgRTnodeBaseInDeSerialize(sMsgRTnodeBaseIn_t *msg, uint8_t *buf, uint32_t size);
int msgRTnodeBaseInIsSerialized(uint8_t *buf, uint32_t size);
int msgRTnodeBaseInSerializeSize(void);

#ifdef __cplusplus
}
#endif

#endif /* _MSGRTNODEBASEIN_H_ */
