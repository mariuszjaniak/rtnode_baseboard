/*****************************************************************************
 *                                                                           *
 *   msgRTnodeBaseCtrDef.h                                                   *
 *                                                                           *
 *   Control commands definitions for the RTnode baseboard                   *
 *                                                                           *
 *   Copyright (C) 2015 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.edu.pl                                             *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#ifndef _MSGRTNODEBASECTRDEF_H_
#define _MSGRTNODEBASECTRDEF_H_

#ifdef __cplusplus
extern "C"
{
#endif

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define MSGRTNODEBASECTRDEF_TYPE_ERR    0
#define MSGRTNODEBASECTRDEF_TYPE_PING   1
#define MSGRTNODEBASECTRDEF_TYPE_REBOOT 2
#define MSGRTNODEBASECTRDEF_TYPE_CFGNEW 3
#define MSGRTNODEBASECTRDEF_TYPE_CFGREQ 4    //request
#define MSGRTNODEBASECTRDEF_TYPE_CFGREP 5    //reply

#define MSGRTNODEBASECTRDEF_ERR_OK      0
#define MSGRTNODEBASECTRDEF_ERR_UNKNOWN 1
#define MSGRTNODEBASECTRDEF_ERR_MEM     2
#define MSGRTNODEBASECTRDEF_ERR_FRAME   3
#define MSGRTNODEBASECTRDEF_ERR_FLASH   4
#define MSGRTNODEBASECTRDEF_ERR_INT     5

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

/*****************************************************************************
 * Globals variables
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/


#ifdef __cplusplus
}
#endif

#endif /* _MSGRTNODEBASECTRDEF_H_ */
