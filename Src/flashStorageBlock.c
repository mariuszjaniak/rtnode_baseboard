/*****************************************************************************
 *                                                                           *
 *   flashStorageBlock.c                                                     *
 *                                                                           *
 *   Flash storage for data blocks                                           *
 *                                                                           *
 *   Copyright (C) 2015 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                               *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *****************************************************************************/

#include <stddef.h>
#include "xmc_flash.h"
#include "flashStorageBlock.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define HEAD_MAGIC       0xA5        /* Magic number */

#define BYTE_ERASE_STATE 0x00        /* Set 0x00 if all 0 or 0xFF if all 1*/

#define ALIGN32          __attribute__ ((aligned (4)))

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

struct xHeader {
  uint32_t len;                   // data block size
  uint16_t id;                    // data block sequence number
  uint8_t  magic;                 // magic pattern
  uint8_t  sum;                   // checksum
} __attribute__( (packed) );
typedef struct xHeader sHeader;

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

static uint32_t getLen(void *block);
static uint16_t getId(void *block);
static bool     isValid(void *block, uint32_t size);

/*****************************************************************************
 *  Global variables
 *****************************************************************************/

/*****************************************************************************
 * Functions implementation
 *****************************************************************************/

uint32_t getLen(void *block)
{
  sHeader  *head = block;

  return head->len;
}

uint16_t getId(void *block)
{
  sHeader  *head = block;

  return head->id;
}

bool isValid(void *block, uint32_t size)
{
  sHeader  *head = block;
  uint32_t  len;

  if(block == NULL) return -1;
  /* Parse length of the data block */
  len = head->len;
  if(len + sizeof(sHeader) > size) return false;
  /* Parse data block id */
  //id  = head->id;
  /* Parse data special patterns */
  if(head->magic != HEAD_MAGIC) return false;
  /* Compute the checksum */
  {
    uint32_t  i;
    uint32_t  sum = head->sum;
    uint8_t  *ptr = (uint8_t*) block + sizeof(sHeader);

    for(i = 0; i < len; i++) sum += *ptr++;
    /* The checksum should be zero */
    if((sum & 0xFF) != 0) return false;
  }
  return true;
}

void flashStorageBlockInit(sflashStorageBlock *dev,
                           void               *start,
                           void               *end,
                           uint32_t            esize,
                           uint32_t            psize)
{
  uint8_t  *offset  = start;
  uint8_t  *current = NULL;

  /* Find valid data block  */
  while(offset < (uint8_t*) end){
    if(isValid(offset, (uint8_t*) end - offset)){
      uint32_t bsize = getLen(offset);;
      /* Check if a valid parameter block has been previously found */
      if(current != NULL){
        uint16_t cId = getId(current);
        uint16_t bId = getId(offset);
        /* Check if the sequence number for the data block is
           greater than the current block */
        if(((bId > cId) && ((bId - cId) < 32768)) ||
           ((cId > bId) && ((cId - bId) > 32768))){
          /* The new data block is more recent than the current one, so
             make it the new current data block */
          current = offset;
        }
      }
      else{
        /* The new data block is more recent than the current one, so
           make it the new current data block */
        current = offset;
      }
      /* Move to next programable page */
      offset += (bsize/psize + (bsize % psize != 0))*psize;
    }
    else
      /* Move to next programable page */
      offset += psize;
  }
  /* Store flash block parameters */
  dev->start   = start;
  dev->end     = end;
  dev->current = current;
  dev->esize   = esize;
  dev->psize   = psize;
}
/*---------------------------------------------------------------------------*/

bool flashStorageBlockStore(sflashStorageBlock *dev,
                            void               *block,
                            uint32_t            bsize)
{
  uint16_t  id;
  uint8_t   sum;
  void     *new;
  bool      wrap = false;

  /* Check if there is a valid parameter block in flash */
  if(dev->current == NULL){
    new = dev->start;
    id = 0;
  }
  else{
    uint32_t offset;
    uint32_t size = getLen(dev->current);
    uint32_t psize = dev->psize;
    /* Find offset of the naex programable page */
    offset = (size/psize + (size % psize != 0))*psize;
    /* Move to next programable page */
    new = (uint8_t*) dev->current + offset;
    if(new >= dev->end) new = dev->start;
    id = getId(dev->current) + 1;
  }
  /* Compute the checksum of the data block to be written */
  {
    uint32_t  i;
    uint8_t  *ptr = block;

    sum = 0;
    for(i = 0; i < bsize; i++) sum -= ptr[i];
  }
  /* Find a location to store this data block. This infinite loop will be
   * explicitly broken out of when a valid location is found */
  while(true){
    /* Check if this location is at the start of an erase block */
    if(((uint32_t) new & (dev->esize - 1)) == 0){
      /* Erase sector */
      XMC_FLASH_EraseSector(new);
    }
    /* Loop through this portion of flash to see if is all erased */
    {
      uint32_t  i;
      uint32_t  offset;
      uint32_t  psize = dev->psize;
      uint8_t  *ptr   = new;

      for(i = 0; i < bsize; i++)
        if(ptr[i] != BYTE_ERASE_STATE) break;
      /* If all bytes in this portion of flash are zeros, then break out of
       * the loop since this is a good location for storing the parameter
       * block */
      if(i == bsize) break;
      /* There is a broken flash region, so find offset of the next programable
       * page */
      offset = (i/psize + (i % psize != 0))*psize;
      /* Move to next programable page */
      new = (uint8_t*) new + offset;
      if(new >= dev->end){
        new  = dev->start;
        wrap = true;
      }
    }
    /* If every possible location has been checked and none are valid, then
     * it will not be possible to write this parameter block */
    if(((dev->current != NULL) && (new >= dev->current) && wrap) ||
       ((dev->current == NULL) && (new >= dev->start)   && wrap))
      return false;
  }
  /* Write this parameter block to flash */
  {
    sHeader   head;
    uint32_t  left = bsize + sizeof(sHeader);
    uint8_t   data[dev->psize] ALIGN32;
    uint8_t  *ptrBlock = block;
    uint8_t  *ptrNew   = new;
    bool      first = true;

    /* Set block header */
    head.len   = bsize;
    head.id    = id;
    head.magic = HEAD_MAGIC;
    head.sum   = sum;
    /* Write */
    while(left > 0){
      uint32_t len;
      if(left >= dev->psize) len = dev->psize;
      else{
        memset(data, BYTE_ERASE_STATE, sizeof(data));
        len = left;
      }
      if(first){
        memcpy(data, &head, sizeof(sHeader));
        memcpy(data + sizeof(sHeader), ptrBlock, len - sizeof(sHeader));
        ptrBlock  += len - sizeof(sHeader);
        first = false;
      }
      else{
        memcpy(data, ptrBlock, len);
        ptrBlock  += len;
      }
      XMC_FLASH_ProgramPage((uint32_t*)ptrNew, (uint32_t*) data);
      ptrNew  += len;
      left -= len;
    }
    /* Compare the data block the with that saved to flash. Return if any of
     * the data does not compare, leaving the previous data block in flash as
     * the most recent */
    /* Check header */
    if(memcmp(new, &head, sizeof(sHeader)) != 0)
      return false;
    /* Check data block */
    if(memcmp((uint8_t*) new + sizeof(sHeader), block, bsize) != 0)
      return false;
  }
  /* The new parameter block becomes the most recent parameter block */
  dev->current = new;

  return true;
}
/*---------------------------------------------------------------------------*/

void* flashStorageBlockRestore(sflashStorageBlock *dev, uint32_t *bsize)
{
  if(dev->current == NULL) return NULL;
  if(bsize != NULL) *bsize = getLen(dev->current);
  return (uint8_t*) dev->current + sizeof(sHeader);
}
/*---------------------------------------------------------------------------*/
