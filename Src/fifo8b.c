/*****************************************************************************
 *                                                                           *
 *   fifo8b.h                                                                *
 *                                                                           *
 *   Byte size software fifo                                                 *
 *                                                                           *
 *   Copyright (C) 2009 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *****************************************************************************/

#include "fifo8b.h"
#include <string.h>

void fifo8bInit(sFifo8b *fifo, uint8_t *buf, uint32_t size)
{
  fifo->in   = 0;
  fifo->out  = 0;
  fifo->full = false;
  fifo->size = size;
  fifo->q    = buf;
}

uint32_t fifo8bStat (sFifo8b *fifo)
{
  uint32_t in, out;
  bool    full;
  
  /* Read q buffer variables into locals */
  in   = fifo->in;
  out  = fifo->out;
  full = fifo->full;
  /* Data in buffer, no pointer wrap */
  if(in > out) return (in - out);
  /* Data in buffer with pointer wrap */
  if(in < out) return (fifo->size - out + in);
  /* qin must be = qout, check if is full */
  if(full) return (fifo->size);
  /* Buffer empty */
  return 0;
}

bool fifo8bGet(sFifo8b *fifo, uint8_t *data)
{
  uint32_t in, out;
  bool    full;
  
  /* Read buffer variables into locals */
  in   = fifo->in;
  out  = fifo->out;
  full = fifo->full;
  /* Check if data are available */
  if((in != out) || full)
  { 
    *data = fifo->q[out];                 /* read data byte */
    out++;				                        /* update pointer */
    if(out > (fifo->size - 1)) out = 0;   /* check for wrap */
    fifo->out  = out;
    fifo->full = false;
    return true;
  }
  else
    return false;
}

bool fifo8bPut(sFifo8b *fifo, uint8_t data)
{
  uint32_t in, out;
  bool    full;

  /* Read buffer variables into locals */
  in   = fifo->in;
  out  = fifo->out;
  full = fifo->full;
  /* Check if buffer is not full */
  if((in!=out) || !full)
  { /* Add data to buffer as space is available */
    fifo->q[in] = data;
    in++;
    if(in > (fifo->size - 1)) in = 0;    /* check for wrap */
    if(in == out) fifo->full = true;     /* buffer is now full */
    fifo->in = in;
    return true;
  }
  else
    return false;
}

uint32_t fifo8bRead(sFifo8b *fifo, uint8_t *data, uint32_t len)
{
  uint32_t in, out;
  bool    full;

  /* Read buffer variables into locals */
  in   = fifo->in;
  out  = fifo->out;
  full = fifo->full;
  /* Check if requested data length is zero or buffer is empty */
  if((len == 0) || ((in == out) && !full)) return 0;
  /* Copy data without wrapping */
  if(in > out){
    uint32_t count;
    /* Get number of available data */
    count = in - out;
    /* Limit number of a data to the requested value */
    if(count > len) count = len;
    /* Copy data */
    memcpy(data, fifo->q + out, count);
    /* Update indexes */
    fifo->out  = out + count;
    return count;
  }
  /* Copy data with wrapping */
  if((in < out) || full){
    uint32_t count;
    /* Get number of available data in the first block */
    count = fifo->size - out;
    /* Check if requested data length fits the first block */
    if(count > len){
      /* Limit number of a data to the requested value */
      count = len;
      /* Copy data */
      memcpy(data, fifo->q + out, count);
      /* Update indexes */
      fifo->out  = out + count;
      /* Clear full flag */
      fifo->full = false;
      return count;
    }
    /* Copy data */
    memcpy(data, fifo->q + out, count);
    /* Update length value */
    len -= count;
    /* Check remaining and available data length */
    if((len == 0) || (in == 0)){
      /* Wrap index */
      fifo->out  = 0;
      /* Clear full flag */
      fifo->full = false;
      return count;
    }
    {
      uint32_t count1;
      /* Get number of available data in the second block */
      count1 = fifo->in;
      /* Limit number of a data to the requested value */
      if(count1 > len) count1 = len;
      /* Copy data */
      memcpy(data + count, fifo->q, count1);
      /* Update indexes */
      fifo->out  = count1;
      /* Clear full flag */
      fifo->full = false;
      return count + count1;
    }
  }
  return 0;
}

uint32_t fifo8bWrite(sFifo8b *fifo, uint8_t *data, uint32_t len)
{
  uint32_t in, out;
  bool    full;

  /* Read buffer variables into locals */
  in   = fifo->in;
  out  = fifo->out;
  full = fifo->full;
  /* Check if data length is zero or buffer is full */
  if((len == 0) || ((in == out) && full)) return 0;
  /* Copy data without wrapping */
  if(out > in){
    uint32_t count;
    /* Get number of available space in the buffer */
    count = out - in;
    /* Limit number of a data to the requested value */
    if(count > len) count = len;
    /* Copy data */
    memcpy(fifo->q + in, data, count);
    /* Update input index */
    in += count;
    /* Update indexes */
    fifo->in = in;
    /* Check if buffer is full */
    if(in == out) fifo->full = true;
    return count;
  }
  /* Copy data with wrapping */
  if((out < in) || !full){
    uint32_t count;
    /* Get number of available space in the first block */
    count = fifo->size - in;
    /* Check if requested data length fits the first block */
    if(count > len){
      /* Limit number of a data to the requested value */
      count = len;
      /* Copy data */
      memcpy(fifo->q + in, data, count);
      /* Update indexes */
      fifo->in  = in + count;
      return count;
    }
    /* Copy data */
    memcpy(fifo->q + in, data, count);
    /* Update length value */
    len -= count;
    /* Check remaining and available data length */
    if((len == 0) || (out == 0)){
      /* Wrap index */
      fifo->in = 0;
      /* Check if buffer is full */
      if(out == 0) fifo->full = true;
      return count;
    }
    {
      uint32_t count1;
      /* Get number of available data in the second block */
      count1 = fifo->out;
      /* Limit number of a data to the requested value */
      if(count1 > len) count1 = len;
      /* Copy data */
      memcpy(fifo->q, data + count, count1);
      /* Update indexes */
      fifo->in = count1;
      /* Check if buffer is full */
      if(out == count1) fifo->full = true;
      return count + count1;
    }
  }
  return 0;
}

bool fifo8bDel(sFifo8b *fifo)
{
  uint32_t in, out;
  bool    full;
  
  /* Read buffer variables into locals */
  in   = fifo->in;
  out  = fifo->out;
  full = fifo->full;
  /* If (data available) */
  if((in != out) || full)
  { 
    /* update pointer */
    if(in == 0) in = (fifo->size - 1);
    else --in;
    fifo->in = in;
    fifo->full = false;
    return true;				  
  }
  else
    return false;				  
}
