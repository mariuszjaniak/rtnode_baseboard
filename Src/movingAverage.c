/***************************************************************************
 *                                                                         *
 *   movingAverage.c                                                       *
 *                                                                         *
 *   Moving average filter                                                 *
 *                                                                         *
 *   Copyright (C) 2010 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.wroc.pl                                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <string.h>
#include "movingAverage.h"

/***************************************************************************** 
 * Constants
 *****************************************************************************/

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

/***************************************************************************** 
 * Function prototypes
 *****************************************************************************/

/***************************************************************************** 
 *  Global variables
 *****************************************************************************/

/***************************************************************************** 
 * Functions implementation
 *****************************************************************************/

void movingAverage32bInit(sMA32b *par, int32_t *z, uint32_t v)
{  
#ifdef DEBUG
  if((par == NULL) || (z == NULL)) return;
#endif
  /* Set initial state */
  par->z   = z;
  par->y   = 0;
  par->v   = v;
  par->s   = 0;
  memset(z, 0, sizeof(int32_t)*(1 << v));
}

int32_t movingAverage32b(sMA32b *par, int32_t in)
{
  uint32_t  s, v;
  int32_t  *z;
  int64_t   y;
  
#ifdef DEBUG
  if(par == NULL) return 0;
#endif
  /* Init local variables */
  z   = par->z;
  y   = par->y;
  v   = par->v;
  s   = par->s;
  /* Calculate the filter */
  y = y + in - z[s];
  /* Store input at the beginning of the delay line */
  z[s] = in;
  /* Increment state, wrap if needed */
  if(++s >= (1UL << v)) s = 0;
  /* Store state */
  par->s = s;       
  /* Store output value (not scaled) */
  par->y = y;
  /* */
  y >>= v;
  /* Saturate output */
  if(y > INT32_MAX) y = INT32_MAX;
  else if(y < INT32_MIN) y = INT32_MIN;
  /* Return scaled output value */
  return (int32_t) y;
}
