/***************************************************************************
 *                                                                         *
 *   cmdlineio.c                                                           *
 *                                                                         *
 *   Command line input/output instructions                                *
 *                                                                         *
 *   Copyright (C) 2009 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.edu.pl                                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/

#include <stdlib.h>
#include <string.h>
#include "cmdlineio.h"
#include "cmdline.h"
#include "halTimerInterface.h"
#include "rtnet_inet.h"
#include "rtnetDev.h"
#include "rtnet.h"
#include "msgRTnodeBaseCfg.h"
#include "globals.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

/***************************************************************************** 
 * Function prototypes
 *****************************************************************************/

int str2Bool(char *str, bool *val);
int str2UW8(char *str, uint8_t *val, uint8_t min, uint8_t max);
int str2UW16(char *str, uint16_t *val, uint16_t min, uint16_t max);
int str2UW32(char *str, uint32_t *val, uint32_t min, uint32_t max);

/***************************************************************************** 
 *  Global variables
 *****************************************************************************/

extern xRTnetDev_t *gpxRTnetDev;
uint32_t            mDataCount;

/*****************************************************************************
 * Function implementation 
 *****************************************************************************/

int str2Bool(char *str, bool *val)
{
  if((!strcmp(str, "0")) || (!strcmp(str, "false"))){
    *val = false;
    return CMD_NOERR;
  }
  if((!strcmp(str, "1")) || (!strcmp(str, "true"))){
    *val = true;
    return CMD_NOERR;
  }
  return CMD_ERR;
}
/*---------------------------------------------------------------------------*/

int str2UW8(char *str, uint8_t *val, uint8_t min, uint8_t max)
{
  uint32_t  data;
  char    *pom;

  data = strtoul(str, &pom, 10);
  if((*pom != '\0') || (pom == str)) return CMD_ERR;
  if((data < min) || (data > max)) return CMD_ERR;
  *val = (uint8_t)data;
  return CMD_NOERR;
}
/*---------------------------------------------------------------------------*/

int str2UW16(char *str, uint16_t *val, uint16_t min, uint16_t max)
{
  uint32_t  data;
  char    *pom;

  data = strtoul(str, &pom, 10);
  if((*pom != '\0') || (pom == str)) return CMD_ERR;
  if((data < min) || (data > max)) return CMD_ERR;
  *val = (uint16_t)data;
  return CMD_NOERR;
}
/*---------------------------------------------------------------------------*/

int str2UW32(char *str, uint32_t *val, uint32_t min, uint32_t max)
{
  uint32_t  data;
  char    *pom;

  data = strtoul(str, &pom, 10);
  if((*pom != '\0') || (pom == str)) return CMD_ERR;
  if((data < min) || (data > max)) return CMD_ERR;
  *val = data;
  return CMD_NOERR;
}
/*---------------------------------------------------------------------------*/

int MAIN_ver(int UNUSED(argc), char *UNUSED(argv[]))
{
  rtprintf("%u %u\r\n", VER_MAJOR, VER_MINOR);
  return CMD_NOERR;
}
/*---------------------------------------------------------------------------*/

int MAIN_logo(int UNUSED(argc), char *UNUSED(argv[]))
{
  rtprintf("# RTNet - FreeRTOS - XMC4500 v%u.%u\r\n", VER_MAJOR,
            VER_MINOR);
  rtprintf("# author: Mariusz Janiak\r\n");
  rtprintf("# Wroclaw, %s, %s\r\n", __TIME__, __DATE__);
  rtprintf("# Copyright (C) 2015 by Mariusz Janiak\r\n\r\n");
  return CMD_NOERR;
}
/*---------------------------------------------------------------------------*/

int MAIN_s(int UNUSED(argc), char *UNUSED(argv[]))
{
  xRTnetDev_t *rtnet = NULL;
  xTdmaDev_t  *tdma  = NULL;
  xRTcfgDev_t *rtcfg = NULL;
  uint32_t     i;

  rtnet = gpxRTnetDev;
  tdma  = &rtnet->xRTmac.xTdma;
  rtcfg = &rtnet->xRTcfg;
  /* RTnet*/
  /* Test ETH link status */
  gStat.ethSrv.link = xRTnetUpdateLinkStatus();
  if(gStat.ethSrv.link)
    rtprintf("link                   = connected\r\n");
  else
    rtprintf("link                   = disconnected\r\n");
  rtprintf("mac                    = 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x\r\n",
           rtnet->pucMac[0],
           rtnet->pucMac[1],
           rtnet->pucMac[2],
           rtnet->pucMac[3],
           rtnet->pucMac[4],
           rtnet->pucMac[5]);
  rtprintf("txframes               = %d\r\n", rtnet->ulTxFrame);
  rtprintf("rxframes               = %d\r\n", rtnet->ulRxFrame);
  /* RTcfg */
  switch(rtcfg->eState){
  case eRTcfgStateInit:
    rtprintf("rtcfg.state            = Init\r\n");
    break;
  case eRTcfgStateReady:
    rtprintf("rtcfg.state            = Ready\r\n");
    break;
  }
    rtprintf("rtcfg.master           = 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x\r\n",
           rtcfg->pucMaster[0],
           rtcfg->pucMaster[1],
           rtcfg->pucMaster[2],
           rtcfg->pucMaster[3],
           rtcfg->pucMaster[4],
           rtcfg->pucMaster[5]);
  if(rtcfg->eAddr == eRTcfgAddrIP){
    uint32_t ulIpAddr;
    ulIpAddr = rtnet_ntohl(rtcfg->ulIpAddr);
    rtprintf("rtcfg.ip               = %d.%d.%d.%d\r\n",
             (ulIpAddr >> 24) & 0xFFUL,
             (ulIpAddr >> 16) & 0xFFUL,
             (ulIpAddr >> 8)  & 0xFFUL,
              ulIpAddr        & 0xFFUL);
    rtprintf("rtcfg.addr             = IP\r\n");
  }
  else rtprintf("rtcfg.addr             = MAC\r\n");
  rtprintf("rtcfg.heartbeat        = %d\r\n", rtcfg->usHeartbeat);
  rtprintf("rtcfg.state2data       = %d\r\n", rtcfg->ulStage2Data);
  rtprintf("rtcfg.state2len        = %d\r\n", rtcfg->ulStage2Len);
  rtprintf("rtcfg.state2frame      = %d\r\n", rtcfg->ucStage2Frame);
  rtprintf("rtcfg.burstrate        = %d\r\n", rtcfg->ucBurstRate);
  rtprintf("rtcfg.arp.len          = %d\r\n", rtcfg->xArp.ulLen);
  for(i = 0; i < rtcfg->xArp.ulLen; ++i){
    xRTarpEnt_t *ent;
    uint32_t     ulIpAddr;
    ent      = rtcfg->xArp.pucTab + i;
    ulIpAddr = rtnet_ntohl(ent->ulIpAddr);
    rtprintf("rtcfg.arp.tab[%d].mac   = 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x\r\n",
        i,
        ent->pucMac[0],
        ent->pucMac[1],
        ent->pucMac[2],
        ent->pucMac[3],
        ent->pucMac[4],
        ent->pucMac[5]);
    rtprintf("rtcfg.arp.tab[%d].ip    = %d.%d.%d.%d\r\n", i,
             (ulIpAddr >> 24) & 0xFFUL,
             (ulIpAddr >> 16) & 0xFFUL,
             (ulIpAddr >> 8)  & 0xFFUL,
              ulIpAddr        & 0xFFUL);
    switch(ent->eState){
    case eRTarpStateInit:
      rtprintf("rtcfg.arp.tab[%d].state = Init\r\n", i);
      break;
    case eRTarpStateReady:
      rtprintf("rtcfg.arp.tab[%d].state = Ready\r\n", i);
      break;
    }
  }
  /* TDMA */
  rtprintf("rtmac.tdma.cfg.cycle   = 0x%08x%08x\r\n",
      (uint32_t)(tdma->xCfg.ullCycle >> 32ULL), (uint32_t)tdma->xCfg.ullCycle);
  rtprintf("rtmac.tdma.cfg.maxsize = %d\r\n", tdma->xCfg.ulMaxSize);
  rtprintf("rtmac.tdma.cfg.len     = %d\r\n", tdma->xCfg.ulLen);
  for(i = 0; i < tdma->xCfg.ulLen; ++i){
    rtprintf("rtmac.tdma.cfg.slots[%d].offset  = 0x%08x%08x\r\n", i,
        (uint32_t)(tdma->xCfg.pxSlots[i].ullOffset >> 32ULL),
        (uint32_t)tdma->xCfg.pxSlots[i].ullOffset);
    rtprintf("rtmac.tdma.cfg.slots[%d].period  = %d\r\n", i,
        tdma->xCfg.pxSlots[i].ulPeriod);
    rtprintf("rtmac.tdma.cfg.slots[%d].phasing = %d\r\n", i,
        tdma->xCfg.pxSlots[i].ulPhasing);
    rtprintf("rtmac.tdma.cfg.slots[%d].size    = %d\r\n", i,
        tdma->xCfg.pxSlots[i].ulSize);
    rtprintf("rtmac.tdma.cfg.slots[%d].id      = %d\r\n", i,
           tdma->xCfg.pxSlots[i].ulId);
  }
  switch(rtnet->xRTmac.xTdma.eState){
  case eTdmaStatePreInit:
    rtprintf("rtmac.tdma.state       = PreInit\r\n");
    break;
  case eTdmaStateInit:
    rtprintf("rtmac.tdma.state       = Init\r\n");
    break;
  case eTdmaStateCalibrationRequest:
    rtprintf("rtmac.tdma.state       = CalibrationRequest\r\n");
    break;
  case eTdmaStateCalibrationReplay:
    rtprintf("rtmac.tdma.state       = CalibrationReply\r\n");
    break;
  case eTdmaStateNormal:
    rtprintf("rtmac.tdma.state       = Normal\r\n");
    break;
  }
  rtprintf("rtmac.tdma.count.init  = %d\r\n", tdma->xCount.ulInit);
  rtprintf("rtmac.tdma.count.calib = %d\r\n", tdma->xCount.ulCalib);
  rtprintf("rtmac.tdma.count.wait  = %d\r\n", tdma->xCount.ulWait);
  rtprintf("rtmac.tdma.count.slot  = %d\r\n", tdma->xCount.ulSlot);
  rtprintf("rtmac.tdma.count.cycle = %d\r\n", tdma->xCount.ulCycle);
  rtprintf("rtmac.tdma.t.offs      = 0x%08x%08x\r\n",
      (uint32_t)(tdma->xT.llOffs >> 32ULL), (uint32_t)tdma->xT.llOffs);
  rtprintf("rtmac.tdma.t.trans     = 0x%08x%08x\r\n",
        (uint32_t)(tdma->xT.ullTrans >> 32ULL), (uint32_t)tdma->xT.ullTrans);
  rtprintf("rtmac.tdma.t.sched     = 0x%08x%08x\r\n",
          (uint32_t)(tdma->xT.ullSched >> 32ULL), (uint32_t)tdma->xT.ullSched);
  rtprintf("rtmac.tdma.t.proc      = 0x%08x%08x\r\n",
            (uint32_t)(tdma->xT.ullProc >> 32ULL), (uint32_t)tdma->xT.ullProc);
  rtprintf("rtmac.tdma.txfifo.in   = %d\r\n", tdma->xTxFifo.ulIn);
  rtprintf("rtmac.tdma.txfifo.out  = %d\r\n", tdma->xTxFifo.ulOut);
  switch(tdma->xTxFifo.eFlag){
  case eTdmaEmpty:
    rtprintf("rtmac.tdma.txfifo.out  = Empty\r\n");
    break;
  case eTdmaFull:
    rtprintf("rtmac.tdma.txfifo.out  = Full\r\n");

    for(i = 0; i < rtnetconfigTDMA_FIFO_LENGTH; i++){
      rtprintf("rtmac.tdma.txfifo.buffer[%d].len = %d\r\n", i, tdma->xTxFifo.xBuffer[i].xLen);
    }

    break;
  }
 {
    uint64_t time;
    xHalTimerGet(&time);
    rtprintf("time  = 0x%08x%08x\r\n",
             (uint32_t)(time >> 32ULL), (uint32_t)time);
  }
  return CMD_NOERR;
}
/*---------------------------------------------------------------------------*/

int MAIN_p(int UNUSED(argc), char *UNUSED(argv[]))
{
  rtprintf("ctr.stat = %d\r\n", gStat.ethSrv.ctr.stat);
  rtprintf("ctr.port = %d\r\n", gStat.ethSrv.ctr.port);
  if(gStat.ethSrv.ctr.err) rtprintf("ctr.err  = TRUE\r\n");
  else rtprintf("ctr.err  = FALSE\r\n");
  rtprintf("rec.stat = %d\r\n", gStat.ethSrv.rec.stat);
  rtprintf("rec.port = %d\r\n", gStat.ethSrv.rec.port);
  if(gStat.ethSrv.rec.err) rtprintf("rec.err  = TRUE\r\n");
  else rtprintf("rec.err  = FALSE\r\n");
  rtprintf("snd.stat = %d\r\n", gStat.ethSrv.snd.stat);
  rtprintf("snd.port = %d\r\n", gStat.ethSrv.snd.port);
  if(gStat.ethSrv.snd.err) rtprintf("rec.err  = TRUE\r\n");
  else rtprintf("snd.err  = FALSE\r\n");
  return CMD_NOERR;
}
/*---------------------------------------------------------------------------*/

int MAIN_io(int UNUSED(argc), char *UNUSED(argv[]))
{
  int i;

  for(i = 0; i < BSPRTNODEBASE_AI_SIZE; i++){
    rtprintf("ai.ch0%d  = %d\r\n", i, gStat.row.ai.ch[i]);
  }
  for(i = 0; i < BSPRTNODEBASE_AO_SIZE; i++){
    rtprintf("ao.ch%d   = %d\r\n", i, gStat.row.ao.ch[i]);
  }
  rtprintf("dio.i0   = %d\r\n", gStat.row.dio.i0);
  rtprintf("dio.i1   = %d\r\n", gStat.row.dio.i1);
  rtprintf("dio.o0   = %d\r\n", gStat.row.dio.o0);
  rtprintf("dio.o1   = %d\r\n", gStat.row.dio.o1);
  rtprintf("qei0.pos = %d\r\n", gStat.row.qei0.pos);
  rtprintf("qei0.tic = %d\r\n", gStat.row.qei0.tic);
  rtprintf("qei0.t   = %d\r\n", gStat.row.qei0.t);
  rtprintf("qei0.dir = %d\r\n", gStat.row.qei0.dir);
  rtprintf("qei0.err = %d\r\n", gStat.row.qei0.err);
  rtprintf("qei1.pos = %d\r\n", gStat.row.qei1.pos);
  rtprintf("qei1.tic = %d\r\n", gStat.row.qei1.tic);
  rtprintf("qei1.t   = %d\r\n", gStat.row.qei1.t);
  rtprintf("qei1.dir = %d\r\n", gStat.row.qei1.dir);
  rtprintf("qei1.err = %d\r\n", gStat.row.qei1.err);
  return CMD_NOERR;
}
/*---------------------------------------------------------------------------*/

int MAIN_o(int argc, char *argv[])
{
  bool b;

  if(argc < 5) return CMD_ARGS;
  rtprintf("%s %s %s %s %s\r\n", argv[0], argv[1], argv[2], argv[3], argv[4]);
  if(str2Bool(argv[1], &b) != CMD_NOERR) return CMD_ARGS;
  gStat.row.dio.o0 = b;
  if(str2Bool(argv[2], &b) != CMD_NOERR) return CMD_ARGS;
  gStat.row.dio.o1 = b;
  if(str2UW16(argv[3], &gStat.row.ao.ch0, 0, 0xFFF) != CMD_NOERR)
    return CMD_ARGS;
  if(str2UW16(argv[4], &gStat.row.ao.ch1, 0, 0xFFF) != CMD_NOERR)
    return CMD_ARGS;
  return CMD_NOERR;
}
/*---------------------------------------------------------------------------*/

int MAIN_reset(int argc, char *argv[])
{
  sMsgRTnodeBaseCfg_t *cfg;
  bool                 ret;

  /* Allocate space for configuration */
  cfg = pvPortMalloc(sizeof(sMsgRTnodeBaseCfg_t));
  if(cfg == NULL) return CMD_ERR;
  /* Set default configuration */
  setDefCfg(cfg);
  /* Store configuration */
  ret = flashStorageBlockStore(&gStat.fsb, cfg, sizeof(sMsgRTnodeBaseCfg_t));
  /* Release allocated memory */
  vPortFree(cfg);
  if(ret) return CMD_NOERR;
  else return CMD_ERR;
}
/*---------------------------------------------------------------------------*/

int MAIN_c(int argc, char *argv[])
{
  sMsgRTnodeBaseCfg_t *cfg;

  cfg = flashStorageBlockRestore(&gStat.fsb, NULL);
  rtprintf("cfg.id          = %d\r\n", cfg->id);
  rtprintf("cfg.gidLen      = %d\r\n", cfg->gidLen);
  rtprintf("cfg.gid0        = %d\r\n", cfg->gid0);
  rtprintf("cfg.gid1        = %d\r\n", cfg->gid1);
  rtprintf("cfg.gid2        = %d\r\n", cfg->gid2);
  rtprintf("cfg.gid3        = %d\r\n", cfg->gid3);
  rtprintf("cfg.gid4        = %d\r\n", cfg->gid4);
  rtprintf("cfg.gid5        = %d\r\n", cfg->gid5);
  rtprintf("cfg.gid6        = %d\r\n", cfg->gid6);
  rtprintf("cfg.gid7        = %d\r\n", cfg->gid7);
  rtprintf("cfg.gid8        = %d\r\n", cfg->gid8);
  rtprintf("cfg.gid9        = %d\r\n", cfg->gid9);
  rtprintf("cfg.ethMac0     = %d\r\n", cfg->ethMac0);
  rtprintf("cfg.ethMac1     = %d\r\n", cfg->ethMac1);
  rtprintf("cfg.ethMac2     = %d\r\n", cfg->ethMac2);
  rtprintf("cfg.ethMac3     = %d\r\n", cfg->ethMac3);
  rtprintf("cfg.ethMac4     = %d\r\n", cfg->ethMac4);
  rtprintf("cfg.ethMac5     = %d\r\n", cfg->ethMac5);
  rtprintf("cfg.ethCtrPort  = %d\r\n", cfg->ethCtrPort);
  rtprintf("cfg.ethRecPort  = %d\r\n", cfg->ethRecPort);
  rtprintf("cfg.ethSndPort  = %d\r\n", cfg->ethSndPort);
  rtprintf("cfg.ethRemPort  = %d\r\n", cfg->ethRemPort);
  rtprintf("cfg.ethDelay    = %d\r\n", cfg->ethDelay);
  rtprintf("cfg.updateFreq  = %d\r\n", cfg->updateFreq);
  rtprintf("cfg.qei0VelFreq = %d\r\n", cfg->qei0VelFreq);
  rtprintf("cfg.qei0VelExt  = %d\r\n", cfg->qei0VelExt);
  rtprintf("cfg.qei1VelFreq = %d\r\n", cfg->qei1VelFreq);
  rtprintf("cfg.qei1VelExt  = %d\r\n", cfg->qei1VelExt);
  return CMD_NOERR;
}
/*---------------------------------------------------------------------------*/

int MAIN_id(int argc, char *argv[])
{
  uint32_t i;

  rtprintf("node.id     = %d\r\n", gStat.node.id);
  rtprintf("node.gidLen = %d\r\n", gStat.node.gidLen);
  for(i = 0; i < gStat.node.gidLen; i++)
    rtprintf("node.id%d    = %d\r\n", i, gStat.node.gid[i]);
  return CMD_NOERR;
}
/*---------------------------------------------------------------------------*/

int MAIN_reboot(int argc, char *argv[])
{
  /* Reboot system */
  NVIC_SystemReset();
  /* Should never get here, just in case*/
  return CMD_NOERR;
}
/*---------------------------------------------------------------------------*/
