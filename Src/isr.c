/*****************************************************************************
 *                                                                           *
 *   isr.c                                                                   *
 *                                                                           *
 *   Interrupt service routines                                               *
 *                                                                           *
 *   Copyright (C) 2015 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                               *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

/* Hardware includes. */
#include <XMC4500.h>
#include <DAVE.h>     //Declarations from DAVE Code Generation (includes SFR declaration)
#include <xmc_vadc.h>
#include <xmc_gpio.h>
#include <xmc_ccu4.h>
#include <xmc_posif.h>
#include <xmc_uart.h>

/* FreeRTOS includes */
#include "FreeRTOS.h"

/* Others */
#include "rtStream.h"
#include "globals.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

extern BaseType_t  xTimerGet(uint64_t *pullTime);

/*****************************************************************************
 *  Global variables
 *****************************************************************************/

extern uint32_t            gMaxTime;
extern uint32_t            gMinTime;
extern uint32_t            gDiff;

/*****************************************************************************
 * Functions implementation
 *****************************************************************************/

void VADC0_G0_0_IRQHandler(void)
{
  volatile XMC_VADC_RESULT_SIZE_t *result = gStat.row.ai.ch;
  int i;

  /* Read the result register */
  for(i = 0; i < BSPRTNODEBASE_AI_SIZE; i++){
    result[i] = XMC_VADC_GROUP_GetResult(BSPRTNODEBASE_AI_DEV_0, 2*i) >> 1;
  }
  /* Read digital inputs */
  gStat.row.dio.port &= ~(BSPRTNODEBASE_DIO_I_MASK);
  gStat.row.dio.port |= BSPRTNODEBASE_DIO_DEV->IN & BSPRTNODEBASE_DIO_I_MASK;
  /* Acknowledge the interrupt */
  XMC_VADC_GROUP_QueueClearReqSrcEvent(BSPRTNODEBASE_AI_DEV_0);
}
/*---------------------------------------------------------------------------*/

void CCU40_0_IRQHandler(void)
{
  sMsrQeiRow *dev  = &gStat.qei0.row;
  BaseType_t  hptw = false;

  XMC_CCU4_SLICE_ClearEvent(BSPRTNODEBASE_QEI0_TMR_DEV_3,
                            XMC_CCU4_SLICE_IRQ_ID_COMPARE_MATCH_UP);
  /* Get position */
  dev->pos = XMC_CCU4_SLICE_GetTimerValue(BSPRTNODEBASE_QEI0_TMR_DEV_0);
  /* Get direction */
  dev->dir = XMC_POSIF_QD_GetDirection(BSPRTNODEBASE_QEI0_DEV);
  /* Get last tic offset */
  dev->dT =
      XMC_CCU4_SLICE_GetCaptureRegisterValue(BSPRTNODEBASE_QEI0_TMR_DEV_2, 1);
  /* Get tic in time window */
  dev->tic =
      XMC_CCU4_SLICE_GetCaptureRegisterValue(BSPRTNODEBASE_QEI0_TMR_DEV_1, 1);
  /* Wake up task */
  vTaskNotifyGiveFromISR(gStat.qei0.task, &hptw);
  portEND_SWITCHING_ISR(hptw);
}
/*---------------------------------------------------------------------------*/

void CCU41_0_IRQHandler(void)
{
  sMsrQeiRow *dev  = &gStat.qei1.row;
  BaseType_t  hptw = false;

  XMC_CCU4_SLICE_ClearEvent(BSPRTNODEBASE_QEI1_TMR_DEV_3,
                            XMC_CCU4_SLICE_IRQ_ID_COMPARE_MATCH_UP);
  /* Get position */
  dev->pos = XMC_CCU4_SLICE_GetTimerValue(BSPRTNODEBASE_QEI1_TMR_DEV_0);
  /* Get direction */
  dev->dir = XMC_POSIF_QD_GetDirection(BSPRTNODEBASE_QEI1_DEV);
  /* Get last tic offset */
  dev->dT =
      XMC_CCU4_SLICE_GetCaptureRegisterValue(BSPRTNODEBASE_QEI1_TMR_DEV_2, 1);
  /* Get tic in time window */
  dev->tic =
      XMC_CCU4_SLICE_GetCaptureRegisterValue(BSPRTNODEBASE_QEI1_TMR_DEV_1, 1);
  /* Wake up task */
  vTaskNotifyGiveFromISR(gStat.qei1.task, &hptw);
  portEND_SWITCHING_ISR(hptw);
}
/*---------------------------------------------------------------------------*/

void USIC0_0_IRQHandler(void)
{
  BaseType_t hptw = false;

  while(XMC_USIC_CH_RXFIFO_IsEmpty(BSPRTNODEBASE_UART_DEV) == false){
    uint8_t data;
    /* Read the next character from the UART */
    data = (uint8_t)XMC_UART_CH_GetReceivedData(BSPRTNODEBASE_UART_DEV);
    /* Write it to FIFO */
    rtStreamIntRxByte(&gStat.ser.stream, data);
  }
  /* Post process stream after reception */
  rtStreamIntRxBytePost(&gStat.ser.stream, &hptw);
  portEND_SWITCHING_ISR(hptw);
}
/*---------------------------------------------------------------------------*/

void USIC0_1_IRQHandler(void)
{
  BaseType_t hptw = false;
  uint32_t   n;

  /* Get number of available data */
  if((n = rtStreamTxStatFromISR(&gStat.ser.stream)) > 0){
    while((XMC_USIC_CH_TXFIFO_IsFull(BSPRTNODEBASE_UART_DEV) == false) && n--){
      uint8_t data;
      /* Get byte */
      rtStreamIntTxByte(&gStat.ser.stream, &data);
      /* Send data */
      XMC_UART_CH_Transmit(BSPRTNODEBASE_UART_DEV, data);
    }
    /* Post process stream after transmition */
    rtStreamIntTxBytePost(&gStat.ser.stream, &hptw);
  }
  else{
    /* Disable the standard transmit event */
    XMC_USIC_CH_DisableEvent(BSPRTNODEBASE_UART_DEV,
                             XMC_USIC_CH_EVENT_TRANSMIT_BUFFER);
  }
  portEND_SWITCHING_ISR(hptw);
}
/*---------------------------------------------------------------------------*/
