/***************************************************************************
 *                                                                         *
 *   ustdlib.c                                                             *
 *                                                                         *
 *   Prototypes for simple standard library functions.                     *
 *                                                                         *
 *   Copyright (C) 2009 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.edu.pl                                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <string.h>
#include "ustdlib.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

/*****************************************************************************
 *  Global variables
 *****************************************************************************/

static const char * const g_pcHex = "0123456789abcdef";

/*****************************************************************************
 * Functions implementation
 *****************************************************************************/

char *ustrncpy (char *pcDst, const char *pcSrc, int iNum)
{
  int iCount;

  iCount = 0;
  while(iNum && pcSrc[iCount]){
    pcDst[iCount] = pcSrc[iCount];
    iCount++;
    iNum--;
  }
  while(iNum){
    pcDst[iCount++] = (char)0;
    iNum--;
  }
  return(pcDst);
}

int uvsnprintf(char *pcBuf, unsigned long ulSize, const char *pcString,
               va_list vaArgP)
{
  unsigned long ulIdx, ulValue, ulCount, ulBase, ulNeg;
  char *pcStr, cFill;
  int iConvertCount = 0;

  if(ulSize){
    ulSize--;
  }
  iConvertCount = 0;
  while(*pcString){
    for(ulIdx = 0; (pcString[ulIdx] != '%') && (pcString[ulIdx] != '\0');
        ulIdx++) {}
    if(ulIdx > ulSize){
      ustrncpy(pcBuf, pcString, ulSize);
      pcBuf += ulSize;
      ulSize = 0;
    }
    else{
      ustrncpy(pcBuf, pcString, ulIdx);
      pcBuf += ulIdx;
      ulSize -= ulIdx;
    }
    iConvertCount += ulIdx;
    pcString += ulIdx;
    if(*pcString == '%'){
      pcString++;
      ulCount = 0;
      cFill = ' ';
again:
      switch(*pcString++){
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
      {
        if((pcString[-1] == '0') && (ulCount == 0)){
          cFill = '0';
        }
        ulCount *= 10;
        ulCount += pcString[-1] - '0';
        goto again;
      }
      case 'c':
      {
        ulValue = va_arg(vaArgP, unsigned long);
        if(ulSize != 0){
          *pcBuf++ = (char)ulValue;
          ulSize--;
        }
        iConvertCount++;
        break;
      }
      case 'd':
      {
        ulValue = va_arg(vaArgP, unsigned long);
        if((long)ulValue < 0){
          ulValue = -(long)ulValue;
          ulNeg = 1;
        }
        else{
          ulNeg = 0;
        }
        ulBase = 10;
        goto convert;
      }
      case 's':
      {
        pcStr = va_arg(vaArgP, char *);
        for(ulIdx = 0; pcStr[ulIdx] != '\0'; ulIdx++){}
        if(ulCount > ulIdx){
          iConvertCount += (ulCount - ulIdx);
        }
        if(ulIdx > ulSize){
          ustrncpy(pcBuf, pcStr, ulSize);
          pcBuf += ulSize;
          ulSize = 0;
        }
        else{
          ustrncpy(pcBuf, pcStr, ulIdx);
          pcBuf += ulIdx;
          ulSize -= ulIdx;
          if(ulCount > ulIdx){
            ulCount -= ulIdx;
            if(ulCount > ulSize){
              ulCount = ulSize;
            }
            ulSize =- ulCount;
            while(ulCount--){
              *pcBuf++ = ' ';
            }
          }
        }
        iConvertCount += ulIdx;
        break;
      }
      case 'u':
      {
        ulValue = va_arg(vaArgP, unsigned long);
        ulBase = 10;
        ulNeg = 0;
        goto convert;
      }
      case 'x':
      case 'X':
      case 'p':
      {
        ulValue = va_arg(vaArgP, unsigned long);
        ulBase = 16;
        ulNeg = 0;
convert:
        for(ulIdx = 1;
            (((ulIdx * ulBase) <= ulValue) &&
                (((ulIdx * ulBase) / ulBase) == ulIdx));
            ulIdx *= ulBase, ulCount--){}
        if(ulNeg){
          ulCount--;
        }
        if(ulNeg && (ulSize != 0) && (cFill == '0')){
          *pcBuf++ = '-';
          ulSize--;
          iConvertCount++;
          ulNeg = 0;
        }
        if((ulCount > 1) && (ulCount < 65536)){
          for(ulCount--; ulCount; ulCount--){
            if(ulSize != 0){
              *pcBuf++ = cFill;
              ulSize--;
            }
            iConvertCount++;
          }
        }
        if(ulNeg && (ulSize != 0)){
          *pcBuf++ = '-';
          ulSize--;
          iConvertCount++;
        }
        for(; ulIdx; ulIdx /= ulBase){
          if(ulSize != 0){
            *pcBuf++ = g_pcHex[(ulValue / ulIdx) % ulBase];
            ulSize--;
          }
          iConvertCount++;
        }
        break;
      }
      case '%':
      {
        if(ulSize != 0){
          *pcBuf++ = pcString[-1];
          ulSize--;
        }
        iConvertCount++;
        break;
      }
      default:
      {
        if(ulSize >= 5){
          ustrncpy(pcBuf, "ERROR", 5);
          pcBuf += 5;
          ulSize -= 5;
        }
        else{
          ustrncpy(pcBuf, "ERROR", ulSize);
          pcBuf += ulSize;
          ulSize = 0;
        }
        iConvertCount += 5;
        break;
      }
      }
    }
  }
  *pcBuf = 0;
  return(iConvertCount);
}

int usprintf(char *pcBuf, const char *pcString, ...)
{
    va_list vaArgP;
    int iRet;

    va_start(vaArgP, pcString);
    iRet = uvsnprintf(pcBuf, 0xffff, pcString, vaArgP);
    va_end(vaArgP);
    return(iRet);
}

int usnprintf(char *pcBuf, unsigned long ulSize, const char *pcString, ...)
{
    int iRet;
    va_list vaArgP;

    va_start(vaArgP, pcString);
    iRet = uvsnprintf(pcBuf, ulSize, pcString, vaArgP);
    va_end(vaArgP);
    return(iRet);
}
