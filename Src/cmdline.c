/***************************************************************************
 *                                                                         *
 *   cmdline.c                                                             *
 *                                                                         *
 *   Command lines processor                                               *
 *                                                                         *
 *   Copyright (C) 2009 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.edu.pl                                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
#include "cmdline.h"

/***************************************************************************** 
 * Constants
 *****************************************************************************/

/***************************************************************************** 
 * Global variables
 *****************************************************************************/

/***************************************************************************** 
 * Function prototypes
 *****************************************************************************/

static int CMD_lineProcess(char *pcCmdLine);

/***************************************************************************** 
 * Functions implementation
 *****************************************************************************/

int CMD_lineProcess(char *pcCmdLine)
{
  CMD_lineEntry_str const *pCmdEntry;
  static char *argv[CMD_MAX_ARGS];
  char *pcChar;
  int argc;
  int bFindArg = 1;
  int isString = 0;

  /* Initialize the argument counter, and point to the beginning of the
   command line string */
  argc = 0;
  pcChar = pcCmdLine;
  /* Advance through the command line until a zero character is found */
  while(*pcChar){
    if(*pcChar == '"'){
      isString = ~isString;
      *pcChar = '\0';
    }
    else if((*pcChar == ' ') && (!isString)){
      *pcChar = '\0';
      bFindArg = 1;
    }
    else{
      /* If bFindArg is set, then that means we are looking for the start 
       of the next argument. */
      if(bFindArg){
        /* As long as the maximum number of arguments has not been
         reached, then save the pointer to the start of this new arg
         in the argv array, and increment the count of args, argc */
        if(argc < CMD_MAX_ARGS){
          argv[argc] = pcChar;
          argc++;
          bFindArg = 0;
        }
        /* The maximum number of arguments has been reached so return
         the error */
        else{
          return (CMD_ARGS);
        }
      }
    }
    /* Advance to the next character in the command line. */
    pcChar++;
  }
  /* If one or more arguments was found, then process the command. */
  if(argc){
    if(gCmdInput == NULL){
      /* Start at the beginning of the command table, to look for a matching 
       command. */
      pCmdEntry = &gCmdTable[0];
      /* Search through the command table until a null command string is 
       found, which marks the end of the table */
      while(pCmdEntry->pcCmd){
        /* If this command entry command string matches argv[0], then call
         the function for this command, passing the command line arguments */
        if(!strcmp(argv[0], pCmdEntry->pcCmd)){
          return (pCmdEntry->pfnCmd(argc, argv));
        }
        /* Not found, so advance to the next entry. */
        pCmdEntry++;
      }
    }
    else{
      return gCmdInput(argc, argv);
    }
  }
  /* Fall through to here means that no matching command was found, so return
   an error. */
  return (CMD_ERR);
}

int CMD_shell(char *line, int n)
{
  int cmdRet;

  if(n > 0){
    cmdRet = CMD_lineProcess(line);
    switch(cmdRet){
    case CMD_ERR:
      gCmdPrintf(CMD_MSG_ERR);
      break;
    case CMD_ARGS:
      gCmdPrintf(CMD_MSG_ARGS);
      break;
    case CMD_NOERR:
      gCmdPrintf(CMD_MSG_NOERR);
      break; 
    default:
      gCmdPrintf(CMD_MSG_ERR);
      break;
    }
  }
  return CMD_NOERR;
}

/* Display help info */
int CMD_help(int argc, char *argv[])
{
  unsigned int maxLen = 0;
  unsigned int j;

  CMD_lineEntry_str const *pCmdEntry = &gCmdTable[0];

  while(pCmdEntry->pcCmd){
    if(strlen(pCmdEntry->pcCmd) > maxLen) maxLen = strlen(pCmdEntry->pcCmd);
    pCmdEntry++;
  }
  pCmdEntry = &gCmdTable[0];
  if(argc == 1){
    while(pCmdEntry->pcCmd){
      gCmdPrintf("%s", pCmdEntry->pcCmd);
      for(j = 0; j < maxLen - strlen(pCmdEntry->pcCmd) + 1; j++)
        gCmdPrintf(" ");
      gCmdPrintf("- %s\r\n", pCmdEntry->pcHelp);
      pCmdEntry++;
    }
  }
  else{
    int i = 1;
    while(i < argc){
      pCmdEntry = &gCmdTable[0];
      while(pCmdEntry->pcCmd){
        if(!strcmp(argv[i], pCmdEntry->pcCmd)){
          gCmdPrintf("%s", pCmdEntry->pcCmd);
          for(j = 0; j < maxLen - strlen(pCmdEntry->pcCmd) + 1; j++)
            gCmdPrintf(" ");
          gCmdPrintf("- %s\r\n", pCmdEntry->pcHelp);
        }
        pCmdEntry++;
      }
      i++;
    }
  }
  return CMD_NOERR;
}

int CMD_logo(int UNUSED(argc), char *UNUSED(argv[]))
{
  gCmdPrintf("Command Line Processor v1.0\r\n");
  gCmdPrintf("author: Mariusz Janiak\r\n");
  gCmdPrintf("Wroclaw 2008\r\n\r\n");
  return CMD_NOERR;
}


