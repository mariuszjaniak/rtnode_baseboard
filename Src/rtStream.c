/*****************************************************************************
 *                                                                           *
 *   rtStream.c                                                              *
 *                                                                           *
 *   Free RTOS interrupt driven stream implementation                        *
 *                                                                           *
 *   Copyright (C) 2010 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#include "rtStream.h"
#include "task.h"

/***************************************************************************** 
 * Constants
 *****************************************************************************/

#define INTERRUPT_SUPPORT 1

#define POLL_DELAY_RX ((TickType_t ) 1)

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

/***************************************************************************** 
 * Function prototypes
 *****************************************************************************/

/***************************************************************************** 
 *  Global variables
 *****************************************************************************/

/***************************************************************************** 
 * Functions implementation
 *****************************************************************************/

bool rtStreamInit(sRtStream *dev, uint8_t *rx, uint32_t rxLen, uint8_t *tx,
                   uint32_t txLen, fUartio enTx)
{
#ifdef DEBUG
  if(dev == NULL) return false;
#endif
  if((rx == NULL) && (rxLen > 0)) return false;
  if((tx == NULL) && (txLen > 0)) return false;
  if(enTx == NULL) return false;
  fifo8bInit(&dev->rxFifo, rx, rxLen);
  fifo8bInit(&dev->txFifo, tx, txLen);
  dev->enTx = enTx;
  dev->xSemRx = xSemaphoreCreateCounting(1, 0);
  if(dev->xSemRx == NULL) return false;
  dev->xSemTx = xSemaphoreCreateCounting(1, 1);
  if(dev->xSemTx == NULL) return false;
  dev->rxCount = 0;
  dev->txCount = 0;
  return true;
}

bool rtStreamIntTxByte(sRtStream *dev, uint8_t *data)
{
#ifdef DEBUG
  if(dev == NULL) return false;
#endif
  if(fifo8bGet(&dev->txFifo, data)){
    dev->txCount++;
    return true;
  }
  return false;
}

bool rtStreamIntTxBytePost(sRtStream *dev, BaseType_t  *hptw)
{
#ifdef DEBUG
  if(dev == NULL) return false;
#endif
  if(dev->txCount > 0){
    dev->txCount = 0;
#ifdef INTERRUPT_SUPPORT
    xSemaphoreGiveFromISR(dev->xSemTx, hptw);
#else
    xSemaphoreGive(dev->xSemTx);
#endif
    return true;
  }
  return false;
}

uint32_t rtStreamIntTxData(sRtStream  *dev,
                           uint8_t    *data,
                           uint32_t    len,
                           BaseType_t *hptw)
{
  uint32_t count;

#ifdef DEBUG
  if(dev == NULL) return false;
#endif
  count = fifo8bRead(&dev->txFifo, data, len);
  if(count > 0){
#ifdef INTERRUPT_SUPPORT
    xSemaphoreGiveFromISR(dev->xSemTx, hptw);
#else
    xSemaphoreGive(dev->xSemTx);
#endif
  }
  return count;
}

bool rtStreamIntRxByte(sRtStream *dev, uint8_t data)
{
#ifdef DEBUG
  if(dev == NULL) return false;
#endif
  if(fifo8bPut(&dev->rxFifo, data)){
    dev->rxCount++;
    return true;
  }
  return false;
}

bool rtStreamIntRxBytePost(sRtStream *dev, BaseType_t  *hptw)
{
#ifdef DEBUG
  if(dev == NULL) return false;
#endif
  if(dev->rxCount > 0){
    dev->rxCount = 0;
#ifdef INTERRUPT_SUPPORT
    xSemaphoreGiveFromISR(dev->xSemRx, hptw);
#else
    xSemaphoreGive(dev->xSemRx);
#endif
    return true;
  }
  return false;
}

uint32_t rtStreamIntRxData(sRtStream  *dev,
                           uint8_t    *data,
                           uint32_t    len,
                           BaseType_t *hptw)
{
  uint32_t count;

#ifdef DEBUG
  if(dev == NULL) return false;
#endif
  count = fifo8bWrite(&dev->rxFifo, data, len);
  if(count > 0){
#ifdef INTERRUPT_SUPPORT
    xSemaphoreGiveFromISR(dev->xSemRx, hptw);
#else
    xSemaphoreGive(dev->xSemRx);
#endif
  }
  return count;
}

uint32_t rtStreamRxStat(sRtStream *dev)
{
  uint32_t ret;

#ifdef DEBUG
  if(dev == NULL) return 0;
#endif
  taskENTER_CRITICAL();
  {
    ret = fifo8bStat(&dev->rxFifo);
  }
  taskEXIT_CRITICAL();
  return  ret;
}

uint32_t rtStreamTxStat(sRtStream *dev)
{
  uint32_t ret;

#ifdef DEBUG
  if(dev == NULL) return 0;
#endif
  taskENTER_CRITICAL();
  {
    ret = fifo8bStat(&dev->txFifo);
  }
  taskEXIT_CRITICAL();
  return ret;
}

uint32_t rtStreamTxStatFromISR(sRtStream *dev)
{
  uint32_t ret;

#ifdef DEBUG
  if(dev == NULL) return 0;
#endif
  ret = fifo8bStat(&dev->txFifo);
  return ret;
}

int32_t rtStreamRead(sRtStream *dev, void* buf, uint32_t len,
                     TickType_t  timeout)
{
  xTimeOutType  xTimeOut;
  uint32_t       count;
  uint8_t       *ptr = buf;

#ifdef DEBUG
  if(dev == NULL) return RTSTREAM_ERR_ARG;
#endif
  if(buf == NULL) return RTSTREAM_ERR_ARG;
  if(len == 0) return 0;
  /* Get current timeout state */
  vTaskSetTimeOutState(&xTimeOut);
  /* Wait for the first byte */
  if(xSemaphoreTake(dev->xSemRx, timeout) == pdFALSE)
    return RTSTREAM_ERR_TIMEOUT;
  /* Read as many data as possible */
  taskENTER_CRITICAL();
  {
    count = fifo8bRead(&dev->rxFifo, ptr, len);
    /* Check if all data have been read */
    if(count == len){
      /* Check if there are available data */
      if(fifo8bStat(&dev->rxFifo) > 0){
        BaseType_t  hptw;
        xSemaphoreGiveFromCRITICAL(dev->xSemRx, &hptw);
      }
      taskEXIT_CRITICAL();
      return (int32_t)count;
    }
  }
  taskEXIT_CRITICAL();
  /* Update variables */
  ptr += count;
  len -= count;
  /* Update timeout */
  xTaskCheckForTimeOut(&xTimeOut, &timeout);
  /* Get rest of the data */
  while(len > 0){
    /* Poll the semaphore */
    if(xSemaphoreTake(dev->xSemRx, POLL_DELAY_RX) == pdTRUE){
      uint32_t count1;
      taskENTER_CRITICAL();
      {
        count1 = fifo8bRead(&dev->rxFifo, ptr, len);
        /* Check if all data have been read */
        if(count1 == len){
          /* Check if there are available data */
          if(fifo8bStat(&dev->rxFifo) > 0){
            BaseType_t  hptw;
            xSemaphoreGiveFromCRITICAL(dev->xSemRx, &hptw);
          }
          taskEXIT_CRITICAL();
          return (int32_t)(count + count1);
        }
      }
      taskEXIT_CRITICAL();
      /* Update variables */
      count += count1;
      ptr   += count1;
      len   -= count1;
      /* Update timeout */
      if(xTaskCheckForTimeOut(&xTimeOut, &timeout) == pdTRUE) break;
    }
    else break;
  }
  return (int32_t)count;
}

int32_t rtStreamWrite(sRtStream *dev, void* buf, uint32_t len,
                      TickType_t  timeout)
{
  xTimeOutType  xTimeOut;
  uint32_t       count;
  uint8_t       *ptr = buf;

#ifdef DEBUG
  if(dev == NULL) return RTSTREAM_ERR_ARG;
#endif
  if(buf == NULL) return RTSTREAM_ERR_ARG;
  if(len == 0) return 0;
  /* Get current timeout state */
  vTaskSetTimeOutState(&xTimeOut);
  count = 0;
  while(len > 0){
    if(xSemaphoreTake(dev->xSemTx, timeout) == pdTRUE){
      uint32_t count1;
      taskENTER_CRITICAL();
      {
        uint32_t stat;
        /* Check fifo state */
        stat = fifo8bStat(&dev->txFifo);
        /* Fill software fifo first */
        count1 = fifo8bWrite(&dev->txFifo, ptr, len);
        /* Check if transmitter is enabled already */
        if(stat == 0) dev->enTx();
        /* Check if all data have been write */
        if(count1 == len){
          /* Check if there is a space for more data */
          if(fifo8bStat(&dev->txFifo) < dev->txFifo.size){
            BaseType_t  hptw;
            xSemaphoreGiveFromCRITICAL(dev->xSemTx, &hptw);
          }
          taskEXIT_CRITICAL();
          return (int32_t)(count + count1);
        }
      }
      taskEXIT_CRITICAL();
      /* Update variables */
      count += count1;
      ptr   += count1;
      len   -= count1;
      /* Update timeout */
      if(xTaskCheckForTimeOut(&xTimeOut, &timeout) == pdTRUE) break;
    }
    else break;
  }
  if(count == 0) return RTSTREAM_ERR_TIMEOUT;
  return (int32_t)count;
}
