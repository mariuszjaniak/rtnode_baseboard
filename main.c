/*****************************************************************************
 *                                                                           *
 *   main.c                                                                  *
 *                                                                           *
 *   FreeRTOS + RTNet + RelaxKit                                             *
 *                                                                           *
 *   Copyright (C) 2014 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                               *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

/* All global variables belong to this module. In whole project should be only
   one such definition. */
#define _GLOBALS_NO_EXTERN_

/* Hardware includes. */
#include <XMC4500.h>
#include <DAVE.h>     //Declarations from DAVE Code Generation (includes SFR declaration)
#include <xmc_scu.h>
#include <xmc_vadc.h>
#include <xmc_dac.h>
#include <xmc_gpio.h>
#include <xmc_ccu4.h>
#include <xmc_posif.h>
#include <xmc_uart.h>
#include <xmc_flash.h>

/* FreeRTOS includes */
#include "FreeRTOS.h"
#include "timers.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* RTnet */
#include "rtnet.h"
#include "rtnet_inet.h"

#include "halTimerInterface.h"

/* Messages */
#include "msgRTnodeBaseIn.h"
#include "msgRTnodeBaseOut.h"
#include "msgRTnodeBaseCtr.h"
#include "msgRTnodeBaseCfg.h"

#include "msgRTnodeBaseCtrDef.h"

/* Others */
#include "ustdlib.h"
#include "flashStorageBlock.h"
#include "rtStream.h"
#include "cmdline.h"
#include "cmdlineio.h"
#include "globals.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define MSGRTNODEBASEOOUT_BIT_CH0 0
#define MSGRTNODEBASEOOUT_BIT_CH1 1
#define MSGRTNODEBASEOOUT_BIT_O0  2
#define MSGRTNODEBASEOOUT_BIT_O1  3

#define CTRWORKER_FREE_FRAME     -2
#define CTRWORKER_OK             -1
#define CTRWORKER_ERR(val)        (MSGRTNODEBASECTRDEF_ERR_##val)

#define SEC 1000000000UL   /* number of ns in s */


/*****************************************************************************
 * Type definitions
 *****************************************************************************/

typedef struct{
  uint8_t  *frame;
  uint32_t  size;
} sReply;

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

static void prvSetupHardware(void);
static void pre_init(void);
static void post_init(void);
static void scu_init(void);
static void vadc_init(void);
static void dac_init(void);
static void gpio_init(void);
static void ccu4_init(void);
static void posif_init(void);
static void uart_init(void);
static void eth_init(void);

static void vTimerCallback(TimerHandle_t pxTimer);

static void vCCU40_0DeferredInterruptHandlerTask(void *pvParameters);
static void vCCU41_0DeferredInterruptHandlerTask(void *pvParameters);

static void vCmdLineTask(void *pvParam);
static void vEthSrvRecTask(void *pvParam);
static void vEthSrvSndTask(void *pvParam);
static void vEthSrvCtrTask(void *pvParam);

static void uart_enTx(void);
static int  getLine(sRtStream *dev, char *line, unsigned int mlen);
static void updateOutputs(void);

static bool parseFrameCtr(uint8_t  *frame,
                          uint32_t  size,
                          sReply   *reply);
static int parseFrameCtrWorker(uint8_t  *frame,
                               uint32_t  size,
                               sReply   *reply,
                               uint8_t  *id);
static int prepareFrameCtr(uint8_t  *frame,
                           uint32_t  size,
                           uint8_t   id,
                           uint8_t   type,
                           uint32_t  prm);
static int prepareFrameIn(uint8_t           *frame,
                          uint32_t           size,
                          uint8_t            id,
                          uint8_t            gid,
                          sBspRTnodeBaseRow *row);
static bool parseFrameOut(uint8_t           *frame,
                          uint32_t            size,
                          sNodeId            *node,
                          sBspRTnodeBaseRow  *row);
static bool isRecipient(sNodeId *node, uint8_t id, uint8_t gid);

/*****************************************************************************
 *  Global variables
 *****************************************************************************/

CMD_prinf_fun      gCmdPrintf;
CMD_callback_fun   gCmdInput;
CMD_lineEntry_str  gCmdTable[] = {
  {"h",      CMD_help,    "display help info"},
  {"l",      MAIN_logo,   "display device info"},
  {"v",      MAIN_ver,    "display version info"},
  {"s",      MAIN_s,      "display RTnet status"},
  {"p",      MAIN_p,      "display server state"},
  {"io",     MAIN_io,     "display I/O state"},
  {"o",      MAIN_o,      "set outputs state"},
  {"c",      MAIN_c,      "display device configuration"},
  {"id",     MAIN_id,     "display node id"},
  {"reboot", MAIN_reboot, "reboot device"},
  {"reset",  MAIN_reset,  "restore default configuration"},
  {NULL, NULL, NULL}
};

static sMsgRTnodeBaseCfg_t *gCfg;

/*****************************************************************************
 * Functions implementation
 *****************************************************************************/

int main(void)
{
  prvSetupHardware();
  /* Create tasks */
  xTaskCreate(vCmdLineTask,
              NULL,
              configSTACK_SIZE_CMDLINE,
              &gStat.ser.stream,
              configTASK_PRIO_CMDLINE,
              &gStat.ser.task);
  xTaskCreate(vEthSrvCtrTask,
              NULL,
              configSTACK_SIZE_ETHSRVCTR,
              &gStat.ethSrv,
              configTASK_PRIO_ETHSRVCTR,
              &gStat.ethSrv.ctr.task);
  xTaskCreate(vEthSrvRecTask,
              NULL,
              configSTACK_SIZE_ETHSRVREC,
              &gStat.ethSrv,
              configTASK_PRIO_ETHSRVREC,
              &gStat.ethSrv.rec.task);
  xTaskCreate(vEthSrvSndTask,
              NULL,
              configSTACK_SIZE_ETHSRVSND,
              &gStat.ethSrv,
              configTASK_PRIO_ETHSRVSND,
              &gStat.ethSrv.snd.task);
  /* Start the scheduler */
  vTaskStartScheduler();
  while(1){;}
  return 0;
}
/*---------------------------------------------------------------------------*/

void prvSetupHardware(void)
{
  /* Initialization of DAVE Apps */
  DAVE_Init();
  /* Initialize global variables */
  pre_init();
  /* Initialize Versatile Analog-to-Digital Converter*/
  scu_init();
  /* Initialize Versatile Analog-to-Digital Converter*/
  vadc_init();
  /* Initialize Digital-to-Analog Converter */
  dac_init();
  /* Initialize GPIO */
  gpio_init();
  /* Initialize CCU4 timer modules */
  ccu4_init();
  /* Initialize POSIF */
  posif_init();
  /* Initialize UART */
  uart_init();
  /* Initialize ETH */
  eth_init();
  /* Update global variable state */
  post_init();
}
/*---------------------------------------------------------------------------*/

void pre_init(void)
{
  sMsgRTnodeBaseCfg_t *vCfg = NULL;

  /* Allocate memory for board configuration */
  gCfg = pvPortMalloc(sizeof(sMsgRTnodeBaseCfg_t));
  if(gCfg == NULL)
    while(1){;}
  /* Initialize the flash parameter block */
  flashStorageBlockInit(&gStat.fsb,
                        BSPRTNODEBASE_FLASH_START,
                        BSPRTNODEBASE_FLASH_END,
                        BSPRTNODEBASE_FLASH_ESIZE,
                        BSPRTNODEBASE_FLASH_PSIZE);
  /* Initialize configuration structure */
  vCfg = flashStorageBlockRestore(&gStat.fsb, NULL);
  if(vCfg == NULL){
    setDefCfg(gCfg);
    flashStorageBlockStore(&gStat.fsb, gCfg, sizeof(sMsgRTnodeBaseCfg_t));
  }
  else{
    /* Copy parameter block from flash */
    memcpy(gCfg, vCfg, sizeof(sMsgRTnodeBaseCfg_t));
  }
  /* Initialize global variable */
  gCmdPrintf = rtprintf;           /* defined in cmdline module */
  gCmdInput  = NULL;               /* defined in cmdline module */
  /* Initialize stream interface */
  rtStreamInit(&gStat.ser.stream,
               gStat.ser.rx,
               SERIAL_FIFO_RX_SIZE,
               gStat.ser.rx,
               SERIAL_FIFO_TX_SIZE,
               uart_enTx);
  /* Set measure */
  gStat.qei0.f        = gCfg->qei0VelFreq;
  gStat.qei0.tic      = 0;
  gStat.qei0.dT       = 0;
  gStat.qei0.extSize  = gCfg->qei0VelExt;
  gStat.qei0.extCount = gCfg->qei0VelExt;
  gStat.qei0.extTest  = 0;
  gStat.qei0.extDir   = 0;
  gStat.qei0.isExt    = true;

  gStat.qei1.f        = gCfg->qei1VelFreq;
  gStat.qei1.tic      = 0;
  gStat.qei1.dT       = 0;
  gStat.qei1.extSize  = gCfg->qei1VelExt;
  gStat.qei1.extCount = gCfg->qei1VelExt;
  gStat.qei1.extTest  = 0;
  gStat.qei1.extDir   = 0;
  gStat.qei1.isExt    = true;
  /* Set IO */
  CLEARARRAY(gStat.row.ai.ch);
  gStat.row.ao.ch0   = BSPRTNODEBASE_AO_CH0_VAL;
  gStat.row.ao.ch1   = BSPRTNODEBASE_AO_CH1_VAL;
  gStat.row.dio.port = 0;
  gStat.row.qei0.pos = 0;
  gStat.row.qei0.tic = 0;
  gStat.row.qei0.t   = 0;
  gStat.row.qei0.dir = 0;
  gStat.row.qei0.err = 0;
  gStat.row.qei1.pos = 0;
  gStat.row.qei1.tic = 0;
  gStat.row.qei1.t   = 0;
  gStat.row.qei1.dir = 0;
  gStat.row.qei1.err = 0;
  /* Initialize node id */
  gStat.node.id = gCfg->id;
  gStat.node.gidLen  = gCfg->gidLen;
  memset(gStat.node.gid, GID_ALL, sizeof(gStat.node.gid));
  memcpy(gStat.node.gid,
         &(gCfg->gid0),
         gStat.node.gidLen*sizeof(gStat.node.gid[0]));
}
/*---------------------------------------------------------------------------*/

void post_init(void)
{
  /* Create and start timer */
  gStat.xTimer = xTimerCreate("Timer",
                              gCfg->updateFreq,
                              pdTRUE,
                              0,
                              vTimerCallback);
  if(gStat.xTimer != NULL) xTimerStart(gStat.xTimer, 0);
  /* Release allocated memory */
  vPortFree(gCfg);
}
/*---------------------------------------------------------------------------*/

void scu_init(void)
{
  gStat.sysClock = XMC_SCU_CLOCK_GetCpuClockFrequency();
}
/*---------------------------------------------------------------------------*/

void vadc_init(void)
{
  XMC_VADC_GLOBAL_CONFIG_t xGlobalCfg = {
    .clock_config = {
      .analog_clock_divider  = 3,
      .msb_conversion_clock  = 0,
      .arbiter_clock_divider = 1
    },
  };
  XMC_VADC_GROUP_CONFIG_t xGroupCfg = {
    .class1 = {
      .conversion_mode_standard = XMC_VADC_CONVMODE_12BIT,
      .sample_time_std_conv     = 3U,
    }
  };
  XMC_VADC_QUEUE_CONFIG_t xQueueCfg;
  int                     i;

  /* Initialize the VADC global registers */
  XMC_VADC_GLOBAL_Init(BSPRTNODEBASE_AI_DEV, &xGlobalCfg);
  /* Configure a conversion kernel */
  XMC_VADC_GROUP_Init(BSPRTNODEBASE_AI_DEV_0, &xGroupCfg);
  XMC_VADC_GROUP_QueueInit(BSPRTNODEBASE_AI_DEV_0, &xQueueCfg);
  /* Configure measurement sequence */
  for(i = 0; i < BSPRTNODEBASE_AI_SIZE; i++)
  {
    XMC_VADC_CHANNEL_CONFIG_t  xChannelCfg = {
      .input_class   = XMC_VADC_CHANNEL_CONV_GROUP_CLASS1,
      .alias_channel = XMC_VADC_CHANNEL_ALIAS_DISABLED
    };
    XMC_VADC_RESULT_CONFIG_t xResultOutCfg = {
      .post_processing_mode   = XMC_VADC_DMM_REDUCTION_MODE,
      .data_reduction_control = 0,   /* Reduction disabled */
      .wait_for_read_mode     = false,
      .part_of_fifo           = true
    };
    XMC_VADC_RESULT_CONFIG_t xResultInCfg = {
      .post_processing_mode   = XMC_VADC_DMM_REDUCTION_MODE,
      .data_reduction_control = 1,  /* Accumulate 2 result values */
      .wait_for_read_mode     = true,
      .part_of_fifo           = false
    };
    XMC_VADC_QUEUE_ENTRY_t xQueueEntry = {
      .refill_needed      = true,  /* Refill is needed */
      .external_trigger   = true,  /* External trigger is required */
      .generate_interrupt = false
    };
    /* Configure a channel */
    xChannelCfg.result_reg_number = 2*i + 1;  /* Input result FIFO */
    XMC_VADC_GROUP_ChannelInit(BSPRTNODEBASE_AI_DEV_0, i, &xChannelCfg);
    /* Configure a result registers */
    XMC_VADC_GROUP_ResultInit(BSPRTNODEBASE_AI_DEV_0, 2*i,     &xResultOutCfg);
    XMC_VADC_GROUP_ResultInit(BSPRTNODEBASE_AI_DEV_0, 2*i + 1, &xResultInCfg);
    /* Add the channel to the queue two times -- two measurements in a
     * sequence are averaged in result register */
    xQueueEntry.channel_num = i;
    XMC_VADC_GROUP_QueueInsertChannel(BSPRTNODEBASE_AI_DEV_0, xQueueEntry);
    if(i == BSPRTNODEBASE_AI_SIZE - 1) xQueueEntry.generate_interrupt = true;
    XMC_VADC_GROUP_QueueInsertChannel(BSPRTNODEBASE_AI_DEV_0, xQueueEntry);
  }
  /* Connect Request Source Event to the NVIC nodes */
  XMC_VADC_GROUP_QueueSetReqSrcEventInterruptNode(BSPRTNODEBASE_AI_DEV_0,
                                                  XMC_VADC_SR_GROUP_SR0);
  /* Enable IRQ */
  NVIC_SetPriority(VADC0_G0_0_IRQn, configVADC_G0_INTERRUPT_PRIORITY);
  NVIC_EnableIRQ(VADC0_G0_0_IRQn);
  /* Enable the analog converters */
  XMC_VADC_GROUP_SetPowerMode(BSPRTNODEBASE_AI_DEV_0,
                              XMC_VADC_GROUP_POWERMODE_NORMAL);
  /* Perform calibration of the converter */
  XMC_VADC_GLOBAL_StartupCalibration(BSPRTNODEBASE_AI_DEV);
}
/*---------------------------------------------------------------------------*/

void dac_init(void)
{
  XMC_DAC_CH_CONFIG_t const sChConfig0 = {
    .output_offset   = 0U,
    .data_type       = XMC_DAC_CH_DATA_TYPE_UNSIGNED,
    .output_scale    = XMC_DAC_CH_OUTPUT_SCALE_NONE,
    .output_negation = XMC_DAC_CH_OUTPUT_NEGATION_DISABLED,
  };
  XMC_DAC_CH_CONFIG_t const sChConfig1 = {
    .output_offset   = 0U,
    .data_type       = XMC_DAC_CH_DATA_TYPE_UNSIGNED,
    .output_scale    = XMC_DAC_CH_OUTPUT_SCALE_NONE,
    .output_negation = XMC_DAC_CH_OUTPUT_NEGATION_DISABLED,
  };
  /* Connect port pads to DAC */
  //XMC_GPIO_SetHardwareControl(XMC_GPIO_PORT14, 8, XMC_GPIO_HWCTRL_PERIPHERAL1);
  //XMC_GPIO_SetHardwareControl(XMC_GPIO_PORT14, 9, XMC_GPIO_HWCTRL_PERIPHERAL1);
  /* Initialize DAC Module*/
  XMC_DAC_CH_Init(BSPRTNODEBASE_AO_DEV, 0U, &sChConfig0);
  XMC_DAC_CH_Init(BSPRTNODEBASE_AO_DEV, 1U, &sChConfig1);
  /* Initialize DAC in Single Value mode */
  XMC_DAC_CH_StartSingleValueMode(BSPRTNODEBASE_AO_DEV, BSPRTNODEBASE_AO_CH0);
  XMC_DAC_CH_StartSingleValueMode(BSPRTNODEBASE_AO_DEV, BSPRTNODEBASE_AO_CH1);
  /* Write a value into DAC output */
  XMC_DAC_CH_Write(BSPRTNODEBASE_AO_DEV,
                   BSPRTNODEBASE_AO_CH0,
                   gStat.row.ao.ch0);
  XMC_DAC_CH_Write(BSPRTNODEBASE_AO_DEV,
                   BSPRTNODEBASE_AO_CH1,
                   gStat.row.ao.ch1);
}
/*---------------------------------------------------------------------------*/

void gpio_init(void)
{
  XMC_GPIO_CONFIG_t xConfig;

  xConfig.mode            = XMC_GPIO_MODE_INPUT_TRISTATE;
  /* Set digital inputs */
  XMC_GPIO_Init(BSPRTNODEBASE_DIO_DEV, BSPRTNODEBASE_DIO_I0_PIN, &xConfig);
  XMC_GPIO_Init(BSPRTNODEBASE_DIO_DEV, BSPRTNODEBASE_DIO_I1_PIN, &xConfig);
  /* Set QEI0 ERR inputs */
  XMC_GPIO_Init(BSPRTNODEBASE_QEI0_ERR_DEV,
                BSPRTNODEBASE_QEI0_ERRA_PIN,
                &xConfig);
  XMC_GPIO_Init(BSPRTNODEBASE_QEI0_ERR_DEV,
                BSPRTNODEBASE_QEI0_ERRB_PIN,
                &xConfig);
  XMC_GPIO_Init(BSPRTNODEBASE_QEI0_ERR_DEV,
                BSPRTNODEBASE_QEI0_ERRI_PIN,
                &xConfig);
  /* Set QEI1 ERR inputs */
  XMC_GPIO_Init(BSPRTNODEBASE_QEI1_ERR_DEV,
                BSPRTNODEBASE_QEI1_ERRA_PIN,
                &xConfig);
  XMC_GPIO_Init(BSPRTNODEBASE_QEI1_ERR_DEV,
                BSPRTNODEBASE_QEI1_ERRB_PIN,
                &xConfig);
  XMC_GPIO_Init(BSPRTNODEBASE_QEI1_ERR_DEV,
                BSPRTNODEBASE_QEI1_ERRI_PIN,
                &xConfig);
  /* Set digital outputs */
  xConfig.mode            = XMC_GPIO_MODE_OUTPUT_PUSH_PULL;
  xConfig.output_level    = XMC_GPIO_OUTPUT_LEVEL_LOW;
  xConfig.output_strength = XMC_GPIO_OUTPUT_STRENGTH_STRONG_SOFT_EDGE;
  XMC_GPIO_Init(BSPRTNODEBASE_DIO_DEV, BSPRTNODEBASE_DIO_O0_PIN, &xConfig);
  XMC_GPIO_Init(BSPRTNODEBASE_DIO_DEV, BSPRTNODEBASE_DIO_O1_PIN, &xConfig);
  XMC_GPIO_SetOutputLevel(BSPRTNODEBASE_DIO_DEV,
                          BSPRTNODEBASE_DIO_O0_PIN,
                          gStat.row.dio.o0 ? XMC_GPIO_OUTPUT_LEVEL_HIGH :
                                             XMC_GPIO_OUTPUT_LEVEL_LOW);
  XMC_GPIO_SetOutputLevel(BSPRTNODEBASE_DIO_DEV,
                          BSPRTNODEBASE_DIO_O1_PIN,
                          gStat.row.dio.o1 ? XMC_GPIO_OUTPUT_LEVEL_HIGH :
                                             XMC_GPIO_OUTPUT_LEVEL_LOW);
  /* Set LEDs */
  xConfig.mode            = XMC_GPIO_MODE_OUTPUT_PUSH_PULL;
  xConfig.output_level    = XMC_GPIO_OUTPUT_LEVEL_LOW;
  xConfig.output_strength = XMC_GPIO_OUTPUT_STRENGTH_STRONG_SOFT_EDGE;
  XMC_GPIO_Init(BSPRTNODEBASE_LED_DEV, BSPRTNODEBASE_LED_ETH_PIN, &xConfig);
  XMC_GPIO_Init(BSPRTNODEBASE_LED_DEV, BSPRTNODEBASE_LED_G_PIN,   &xConfig);
  XMC_GPIO_Init(BSPRTNODEBASE_LED_DEV, BSPRTNODEBASE_LED_R_PIN,   &xConfig);
  XMC_GPIO_SetOutputLow(BSPRTNODEBASE_LED_DEV, BSPRTNODEBASE_LED_ETH_PIN);
  XMC_GPIO_SetOutputLow(BSPRTNODEBASE_LED_DEV, BSPRTNODEBASE_LED_G_PIN);
  XMC_GPIO_SetOutputLow(BSPRTNODEBASE_LED_DEV, BSPRTNODEBASE_LED_R_PIN);
}
/*---------------------------------------------------------------------------*/

void ccu4_init(void)
{
  int i;

  /* Ensure fCCU reaches CCU40 and CCU41 */
  XMC_CCU4_SetModuleClock(BSPRTNODEBASE_QEI0_TMR_DEV, XMC_CCU4_CLOCK_SCU);
  XMC_CCU4_SetModuleClock(BSPRTNODEBASE_QEI1_TMR_DEV, XMC_CCU4_CLOCK_SCU);
  XMC_CCU4_Init(BSPRTNODEBASE_QEI0_TMR_DEV,
                XMC_CCU4_SLICE_MCMS_ACTION_TRANSFER_PR_CR);
  XMC_CCU4_Init(BSPRTNODEBASE_QEI1_TMR_DEV,
                XMC_CCU4_SLICE_MCMS_ACTION_TRANSFER_PR_CR);
  /* Get the slices out of idle mode */
  for(i = 0; i < BSPRTNODEBASE_QEI0_TMR_SIZE; i++)
    XMC_CCU4_EnableClock(BSPRTNODEBASE_QEI0_TMR_DEV, i);
  for(i = 0; i < BSPRTNODEBASE_QEI1_TMR_SIZE; i++)
    XMC_CCU4_EnableClock(BSPRTNODEBASE_QEI1_TMR_DEV, i);
  /* Start the prescaler and restore clocks to slices */
  XMC_CCU4_StartPrescaler(BSPRTNODEBASE_QEI0_TMR_DEV);
  XMC_CCU4_StartPrescaler(BSPRTNODEBASE_QEI1_TMR_DEV);
  /* Initialize the slices */
  {
    XMC_CCU4_SLICE_COMPARE_CONFIG_t xCmpCfg = {
      .timer_mode          = XMC_CCU4_SLICE_TIMER_COUNT_MODE_EA,
      .monoshot            = false,
      .shadow_xfer_clear   = false,
      .dither_timer_period = false,
      .dither_duty_cycle   = false,
      .prescaler_mode      = XMC_CCU4_SLICE_PRESCALER_MODE_NORMAL,
      .mcm_enable          = false,
      //.prescaler_initval   = 0U,
      .float_limit         = 0U,
      .dither_limit        = 0U,
      .passive_level       = XMC_CCU4_SLICE_OUTPUT_PASSIVE_LEVEL_LOW,
      .timer_concatenation = false
    };
    XMC_CCU4_SLICE_CAPTURE_CONFIG_t xCptCfg =
    {
      .fifo_enable         = false,
      .same_event          = false,
      .ignore_full_flag    = false,
      .prescaler_mode      = XMC_CCU4_SLICE_PRESCALER_MODE_NORMAL,
      .prescaler_initval   = 0,
      .float_limit         = 0,
      .timer_concatenation = false
    };

    xCmpCfg.prescaler_initval = BSPRTNODEBASE_QEI0_TMR_0_DIV;
    XMC_CCU4_SLICE_CompareInit(BSPRTNODEBASE_QEI0_TMR_DEV_0 , &xCmpCfg);
    xCptCfg.timer_clear_mode  = XMC_CCU4_SLICE_TIMER_CLEAR_MODE_ALWAYS;
    xCptCfg.prescaler_initval = BSPRTNODEBASE_QEI0_TMR_1_DIV;
    XMC_CCU4_SLICE_CaptureInit(BSPRTNODEBASE_QEI0_TMR_DEV_1 , &xCptCfg);
    xCptCfg.timer_clear_mode = XMC_CCU4_SLICE_TIMER_CLEAR_MODE_NEVER;
    xCptCfg.prescaler_initval = BSPRTNODEBASE_QEI0_TMR_2_DIV;
    XMC_CCU4_SLICE_CaptureInit(BSPRTNODEBASE_QEI0_TMR_DEV_2 , &xCptCfg);
    xCmpCfg.prescaler_initval = BSPRTNODEBASE_QEI0_TMR_3_DIV;
    XMC_CCU4_SLICE_CompareInit(BSPRTNODEBASE_QEI0_TMR_DEV_3 , &xCmpCfg);

    xCmpCfg.prescaler_initval = BSPRTNODEBASE_QEI1_TMR_0_DIV;
    XMC_CCU4_SLICE_CompareInit(BSPRTNODEBASE_QEI1_TMR_DEV_0 , &xCmpCfg);
    xCptCfg.timer_clear_mode = XMC_CCU4_SLICE_TIMER_CLEAR_MODE_ALWAYS;
    xCptCfg.prescaler_initval = BSPRTNODEBASE_QEI1_TMR_1_DIV;
    XMC_CCU4_SLICE_CaptureInit(BSPRTNODEBASE_QEI1_TMR_DEV_1 , &xCptCfg);
    xCptCfg.timer_clear_mode = XMC_CCU4_SLICE_TIMER_CLEAR_MODE_NEVER;
    xCptCfg.prescaler_initval = BSPRTNODEBASE_QEI1_TMR_2_DIV;
    XMC_CCU4_SLICE_CaptureInit(BSPRTNODEBASE_QEI1_TMR_DEV_2 , &xCptCfg);
    xCmpCfg.prescaler_initval = BSPRTNODEBASE_QEI1_TMR_3_DIV;
    XMC_CCU4_SLICE_CompareInit(BSPRTNODEBASE_QEI1_TMR_DEV_3 , &xCmpCfg);
  }
  /* Enable compare match events */
  XMC_CCU4_SLICE_EnableEvent(BSPRTNODEBASE_QEI0_TMR_DEV_3,
                             XMC_CCU4_SLICE_IRQ_ID_COMPARE_MATCH_UP);
  XMC_CCU4_SLICE_EnableEvent(BSPRTNODEBASE_QEI1_TMR_DEV_3,
                             XMC_CCU4_SLICE_IRQ_ID_COMPARE_MATCH_UP);
  /* Connect compare match event to SR0 */
  XMC_CCU4_SLICE_SetInterruptNode(BSPRTNODEBASE_QEI0_TMR_DEV_3,
                                  XMC_CCU4_SLICE_IRQ_ID_COMPARE_MATCH_UP,
                                  XMC_CCU4_SLICE_SR_ID_0);
  XMC_CCU4_SLICE_SetInterruptNode(BSPRTNODEBASE_QEI1_TMR_DEV_3,
                                  XMC_CCU4_SLICE_IRQ_ID_COMPARE_MATCH_UP,
                                  XMC_CCU4_SLICE_SR_ID_0);
  /* Set priority */
  NVIC_SetPriority(BSPRTNODEBASE_QEI0_TMR_IRQ,
                   configCCU4_CC40_INTERRUPT_PRIORITY);
  NVIC_SetPriority(BSPRTNODEBASE_QEI1_TMR_IRQ,
                   configCCU4_CC41_INTERRUPT_PRIORITY);
  /* Enable IRQ */
  NVIC_EnableIRQ(BSPRTNODEBASE_QEI0_TMR_IRQ);
  NVIC_EnableIRQ(BSPRTNODEBASE_QEI1_TMR_IRQ);
  /* Set slices period */
  XMC_CCU4_SLICE_SetTimerPeriodMatch(BSPRTNODEBASE_QEI0_TMR_DEV_0, 0xFFFF);
  XMC_CCU4_SLICE_SetTimerPeriodMatch(BSPRTNODEBASE_QEI0_TMR_DEV_1, 0xFFFF);
  XMC_CCU4_SLICE_SetTimerPeriodMatch(BSPRTNODEBASE_QEI0_TMR_DEV_2, 0xFFFF);

  XMC_CCU4_SLICE_SetTimerPeriodMatch(BSPRTNODEBASE_QEI1_TMR_DEV_0, 0xFFFF);
  XMC_CCU4_SLICE_SetTimerPeriodMatch(BSPRTNODEBASE_QEI1_TMR_DEV_1, 0xFFFF);
  XMC_CCU4_SLICE_SetTimerPeriodMatch(BSPRTNODEBASE_QEI1_TMR_DEV_2, 0xFFFF);
  /* Set velocity timestamp period  */
  {
    uint32_t fccu = BSPRTNODEBASE_CPU_FREQ >> BSPRTNODEBASE_QEI0_TMR_3_DIV;
    uint16_t val;

    val = CEILDIV_UINT(fccu, gCfg->qei0VelFreq);
    XMC_CCU4_SLICE_SetTimerPeriodMatch(BSPRTNODEBASE_QEI0_TMR_DEV_3, val);
    XMC_CCU4_SLICE_SetTimerCompareMatch(BSPRTNODEBASE_QEI0_TMR_DEV_3, val);

    val = CEILDIV_UINT(fccu, gCfg->qei1VelFreq);
    XMC_CCU4_SLICE_SetTimerPeriodMatch(BSPRTNODEBASE_QEI1_TMR_DEV_3, val);
    XMC_CCU4_SLICE_SetTimerCompareMatch(BSPRTNODEBASE_QEI1_TMR_DEV_3, val);
  }
  /* Enable shadow transfer */
  XMC_CCU4_EnableShadowTransfer(BSPRTNODEBASE_QEI0_TMR_DEV,
                                XMC_CCU4_SHADOW_TRANSFER_SLICE_0);
  XMC_CCU4_EnableShadowTransfer(BSPRTNODEBASE_QEI0_TMR_DEV,
                                XMC_CCU4_SHADOW_TRANSFER_SLICE_1);
  XMC_CCU4_EnableShadowTransfer(BSPRTNODEBASE_QEI0_TMR_DEV,
                                XMC_CCU4_SHADOW_TRANSFER_SLICE_2);
  XMC_CCU4_EnableShadowTransfer(BSPRTNODEBASE_QEI0_TMR_DEV,
                                XMC_CCU4_SHADOW_TRANSFER_SLICE_3);

  XMC_CCU4_EnableShadowTransfer(BSPRTNODEBASE_QEI1_TMR_DEV,
                                XMC_CCU4_SHADOW_TRANSFER_SLICE_0);
  XMC_CCU4_EnableShadowTransfer(BSPRTNODEBASE_QEI1_TMR_DEV,
                                XMC_CCU4_SHADOW_TRANSFER_SLICE_1);
  XMC_CCU4_EnableShadowTransfer(BSPRTNODEBASE_QEI1_TMR_DEV,
                                XMC_CCU4_SHADOW_TRANSFER_SLICE_2);
  XMC_CCU4_EnableShadowTransfer(BSPRTNODEBASE_QEI1_TMR_DEV,
                                XMC_CCU4_SHADOW_TRANSFER_SLICE_3);
  /* Configure slices input selector */
  {
    XMC_CCU4_SLICE_EVENT_CONFIG_t xInDevCfg;

    /* Position counter */
    /** Count */
    xInDevCfg.duration     = XMC_CCU4_SLICE_EVENT_FILTER_DISABLED;
    xInDevCfg.edge         = XMC_CCU4_SLICE_EVENT_EDGE_SENSITIVITY_RISING_EDGE;
    xInDevCfg.level        = XMC_CCU4_SLICE_EVENT_LEVEL_SENSITIVITY_ACTIVE_HIGH;
    xInDevCfg.mapped_input = XMC_CCU4_SLICE_INPUT_E; /* POSIFx.OUT0 */
    XMC_CCU4_SLICE_ConfigureEvent(BSPRTNODEBASE_QEI0_TMR_DEV_0,
                                  XMC_CCU4_SLICE_EVENT_0,
                                  &xInDevCfg);
    XMC_CCU4_SLICE_ConfigureEvent(BSPRTNODEBASE_QEI1_TMR_DEV_0,
                                  XMC_CCU4_SLICE_EVENT_0,
                                  &xInDevCfg);
    XMC_CCU4_SLICE_CountConfig(BSPRTNODEBASE_QEI0_TMR_DEV_0,
                               XMC_CCU4_SLICE_EVENT_0);
    XMC_CCU4_SLICE_CountConfig(BSPRTNODEBASE_QEI1_TMR_DEV_0,
                               XMC_CCU4_SLICE_EVENT_0);

    /** Up/Down */
    xInDevCfg.duration     = XMC_CCU4_SLICE_EVENT_FILTER_DISABLED;
    xInDevCfg.edge         = XMC_CCU4_SLICE_EVENT_EDGE_SENSITIVITY_NONE;
    xInDevCfg.level        = XMC_CCU4_SLICE_EVENT_LEVEL_SENSITIVITY_ACTIVE_LOW;
    xInDevCfg.mapped_input = XMC_CCU4_SLICE_INPUT_F; /* POSIFx.OUT1 */
    XMC_CCU4_SLICE_ConfigureEvent(BSPRTNODEBASE_QEI0_TMR_DEV_0,
                                  XMC_CCU4_SLICE_EVENT_1,
                                  &xInDevCfg);
    XMC_CCU4_SLICE_ConfigureEvent(BSPRTNODEBASE_QEI1_TMR_DEV_0,
                                  XMC_CCU4_SLICE_EVENT_1,
                                  &xInDevCfg);
    XMC_CCU4_SLICE_DirectionConfig(BSPRTNODEBASE_QEI0_TMR_DEV_0,
                                   XMC_CCU4_SLICE_EVENT_1);
    XMC_CCU4_SLICE_DirectionConfig(BSPRTNODEBASE_QEI1_TMR_DEV_0,
                                   XMC_CCU4_SLICE_EVENT_1);
    /* Count number of ticks in defined time slot */
    /** Count */
    xInDevCfg.duration     = XMC_CCU4_SLICE_EVENT_FILTER_DISABLED;
    xInDevCfg.edge         = XMC_CCU4_SLICE_EVENT_EDGE_SENSITIVITY_RISING_EDGE;
    xInDevCfg.level        = XMC_CCU4_SLICE_EVENT_LEVEL_SENSITIVITY_ACTIVE_HIGH;
    xInDevCfg.mapped_input = XMC_CCU4_SLICE_INPUT_L; /* POSIFx.OUT2 */
    XMC_CCU4_SLICE_ConfigureEvent(BSPRTNODEBASE_QEI0_TMR_DEV_1,
                                  XMC_CCU4_SLICE_EVENT_0,
                                  &xInDevCfg);
    XMC_CCU4_SLICE_ConfigureEvent(BSPRTNODEBASE_QEI1_TMR_DEV_1,
                                  XMC_CCU4_SLICE_EVENT_0,
                                  &xInDevCfg);
    XMC_CCU4_SLICE_CountConfig(BSPRTNODEBASE_QEI0_TMR_DEV_1,
                               XMC_CCU4_SLICE_EVENT_0);
    XMC_CCU4_SLICE_CountConfig(BSPRTNODEBASE_QEI1_TMR_DEV_1,
                               XMC_CCU4_SLICE_EVENT_0);
    /** Capture & Clear */
    xInDevCfg.duration     = XMC_CCU4_SLICE_EVENT_FILTER_DISABLED;
    xInDevCfg.edge         = XMC_CCU4_SLICE_EVENT_EDGE_SENSITIVITY_RISING_EDGE;
    xInDevCfg.level        = XMC_CCU4_SLICE_EVENT_LEVEL_SENSITIVITY_ACTIVE_HIGH;
    xInDevCfg.mapped_input = XMC_CCU4_SLICE_INPUT_P; /* CCU40.ST3 */
    XMC_CCU4_SLICE_ConfigureEvent(BSPRTNODEBASE_QEI0_TMR_DEV_1,
                                  XMC_CCU4_SLICE_EVENT_1,
                                  &xInDevCfg);
    XMC_CCU4_SLICE_ConfigureEvent(BSPRTNODEBASE_QEI1_TMR_DEV_1,
                                  XMC_CCU4_SLICE_EVENT_1,
                                  &xInDevCfg);
    XMC_CCU4_SLICE_Capture0Config(BSPRTNODEBASE_QEI0_TMR_DEV_1,
                                  XMC_CCU4_SLICE_EVENT_1);
    XMC_CCU4_SLICE_Capture0Config(BSPRTNODEBASE_QEI1_TMR_DEV_1,
                                  XMC_CCU4_SLICE_EVENT_1);
    /* Measure time between PCLK ticks */
    /** Flush & Start */
    xInDevCfg.duration     = XMC_CCU4_SLICE_EVENT_FILTER_DISABLED;
    xInDevCfg.edge         = XMC_CCU4_SLICE_EVENT_EDGE_SENSITIVITY_RISING_EDGE;
    xInDevCfg.level        = XMC_CCU4_SLICE_EVENT_LEVEL_SENSITIVITY_ACTIVE_HIGH;
    xInDevCfg.mapped_input = XMC_CCU4_SLICE_INPUT_F; /* POSIFx.OUT2 */
    XMC_CCU4_SLICE_ConfigureEvent(BSPRTNODEBASE_QEI0_TMR_DEV_2,
                                  XMC_CCU4_SLICE_EVENT_0,
                                  &xInDevCfg);
    XMC_CCU4_SLICE_ConfigureEvent(BSPRTNODEBASE_QEI1_TMR_DEV_2,
                                  XMC_CCU4_SLICE_EVENT_0,
                                  &xInDevCfg);
    XMC_CCU4_SLICE_StartConfig(BSPRTNODEBASE_QEI0_TMR_DEV_2,
                               XMC_CCU4_SLICE_EVENT_0,
                               XMC_CCU4_SLICE_START_MODE_TIMER_START_CLEAR);
    XMC_CCU4_SLICE_StartConfig(BSPRTNODEBASE_QEI1_TMR_DEV_2,
                               XMC_CCU4_SLICE_EVENT_0,
                               XMC_CCU4_SLICE_START_MODE_TIMER_START_CLEAR);
    /** Capture & Clear */
    xInDevCfg.duration     = XMC_CCU4_SLICE_EVENT_FILTER_DISABLED;
    xInDevCfg.edge         = XMC_CCU4_SLICE_EVENT_EDGE_SENSITIVITY_RISING_EDGE;
    xInDevCfg.level        = XMC_CCU4_SLICE_EVENT_LEVEL_SENSITIVITY_ACTIVE_HIGH;
    xInDevCfg.mapped_input = XMC_CCU4_SLICE_INPUT_P; /* CCU40.ST3 */
    XMC_CCU4_SLICE_ConfigureEvent(BSPRTNODEBASE_QEI0_TMR_DEV_2,
                                  XMC_CCU4_SLICE_EVENT_1,
                                  &xInDevCfg);
    XMC_CCU4_SLICE_ConfigureEvent(BSPRTNODEBASE_QEI1_TMR_DEV_2,
                                  XMC_CCU4_SLICE_EVENT_1,
                                  &xInDevCfg);
    XMC_CCU4_SLICE_Capture0Config(BSPRTNODEBASE_QEI0_TMR_DEV_2,
                                  XMC_CCU4_SLICE_EVENT_1);
    XMC_CCU4_SLICE_Capture0Config(BSPRTNODEBASE_QEI1_TMR_DEV_2,
                                  XMC_CCU4_SLICE_EVENT_1);
    /* Velocity timestamp */
    /** Start */
    xInDevCfg.duration     = XMC_CCU4_SLICE_EVENT_FILTER_DISABLED;
    xInDevCfg.edge         = XMC_CCU4_SLICE_EVENT_EDGE_SENSITIVITY_RISING_EDGE;
    xInDevCfg.level        = XMC_CCU4_SLICE_EVENT_LEVEL_SENSITIVITY_ACTIVE_HIGH;
    xInDevCfg.mapped_input = XMC_CCU4_SLICE_INPUT_F; /* POSIFx.OUT5 */
    XMC_CCU4_SLICE_ConfigureEvent(BSPRTNODEBASE_QEI0_TMR_DEV_3,
                                  XMC_CCU4_SLICE_EVENT_0,
                                  &xInDevCfg);
    XMC_CCU4_SLICE_ConfigureEvent(BSPRTNODEBASE_QEI1_TMR_DEV_3,
                                  XMC_CCU4_SLICE_EVENT_0,
                                  &xInDevCfg);
    XMC_CCU4_SLICE_StartConfig(BSPRTNODEBASE_QEI0_TMR_DEV_3,
                               XMC_CCU4_SLICE_EVENT_0,
                               XMC_CCU4_SLICE_START_MODE_TIMER_START);
    XMC_CCU4_SLICE_StartConfig(BSPRTNODEBASE_QEI1_TMR_DEV_3,
                               XMC_CCU4_SLICE_EVENT_0,
                               XMC_CCU4_SLICE_START_MODE_TIMER_START);
  }
  /* Start timers */
  XMC_CCU4_SLICE_StartTimer(BSPRTNODEBASE_QEI0_TMR_DEV_0);
  XMC_CCU4_SLICE_StartTimer(BSPRTNODEBASE_QEI1_TMR_DEV_0);
  XMC_CCU4_SLICE_StartTimer(BSPRTNODEBASE_QEI0_TMR_DEV_1);
  XMC_CCU4_SLICE_StartTimer(BSPRTNODEBASE_QEI1_TMR_DEV_1);

  /* The deferred interrupt handler task CCU40 */
  xTaskCreate(vCCU40_0DeferredInterruptHandlerTask,
              NULL,
              configSTACK_SIZE_CCU40,
              &gStat.qei0,
              configTASK_PRIO_CCU40,
              &gStat.qei0.task);
  /* The deferred interrupt handler task CCU41 */
  xTaskCreate(vCCU41_0DeferredInterruptHandlerTask,
              NULL,
              configSTACK_SIZE_CCU41,
              &gStat.qei1,
              configTASK_PRIO_CCU41,
              &gStat.qei1.task);
}
/*---------------------------------------------------------------------------*/

void posif_init(void)
{
  /* Initialize POSIF modules 0 */
  {
    XMC_POSIF_CONFIG_t xGlobalCfg = {
      .mode   = XMC_POSIF_MODE_QD,
      .input0 = XMC_POSIF_INPUT_PORT_A,
      .input1 = XMC_POSIF_INPUT_PORT_A,
      .input2 = XMC_POSIF_INPUT_PORT_A,
      .filter = XMC_POSIF_FILTER_1_CLOCK_CYCLE
    };
    XMC_POSIF_Init(BSPRTNODEBASE_QEI0_DEV, &xGlobalCfg);
  }
  /* Initialize POSIF modules 1 */
  {
    XMC_POSIF_CONFIG_t xGlobalCfg = {
      .mode   = XMC_POSIF_MODE_QD,
      .input0 = XMC_POSIF_INPUT_PORT_B,
      .input1 = XMC_POSIF_INPUT_PORT_B,
      .input2 = XMC_POSIF_INPUT_PORT_B,
      .filter = XMC_POSIF_FILTER_1_CLOCK_CYCLE
    };
    XMC_POSIF_Init(BSPRTNODEBASE_QEI1_DEV, &xGlobalCfg);
  }
  /* Initialize quadrature decoder mode */
  {
    XMC_POSIF_QD_CONFIG_t xQdCfg = {
      .mode         = XMC_POSIF_QD_MODE_QUADRATURE,
      .phase_a      = XMC_POSIF_INPUT_ACTIVE_LEVEL_HIGH,
      .phase_b      = XMC_POSIF_INPUT_ACTIVE_LEVEL_HIGH,
      .phase_leader = 0,
      .index        = XMC_POSIF_QD_INDEX_GENERATION_ALWAYS,
    };

    XMC_POSIF_QD_Init(BSPRTNODEBASE_QEI0_DEV, &xQdCfg);
    XMC_POSIF_QD_Init(BSPRTNODEBASE_QEI1_DEV, &xQdCfg);
  }
  /* Star decoder */
  XMC_POSIF_Start(BSPRTNODEBASE_QEI0_DEV);
  XMC_POSIF_Start(BSPRTNODEBASE_QEI1_DEV);
}
/*---------------------------------------------------------------------------*/

void  uart_init(void)
{
  XMC_UART_CH_CONFIG_t xCfg = {
    .baudrate     = 57600U,
    .data_bits    = 8U,
    .frame_length = 8U,
    .stop_bits    = 1U,
    .oversampling = 16U,
    .parity_mode  = XMC_USIC_CH_PARITY_MODE_NONE
  };

  /* Configure USIC0 CH0 in UART mode */
  XMC_UART_CH_Init(BSPRTNODEBASE_UART_DEV, &xCfg);
  XMC_UART_CH_SetInputSource(BSPRTNODEBASE_UART_DEV,
                             XMC_UART_CH_INPUT_RXD,
                             USIC0_C0_DX0_P1_4);
  /* Enable RX and TX fifos */
  XMC_USIC_CH_RXFIFO_Configure(BSPRTNODEBASE_UART_DEV,
                               0,
                               XMC_USIC_CH_FIFO_SIZE_16WORDS,
                               0);
  XMC_USIC_CH_TXFIFO_Configure(BSPRTNODEBASE_UART_DEV,
                               16,
                               XMC_USIC_CH_FIFO_SIZE_16WORDS,
                               1);
  /* Clear the receive FIFO */
  XMC_USIC_CH_RXFIFO_Flush(BSPRTNODEBASE_UART_DEV);
  /* Clear the transmit FIFO */
  XMC_USIC_CH_TXFIFO_Flush(BSPRTNODEBASE_UART_DEV);
  /* Start UART */
  XMC_UART_CH_Start(BSPRTNODEBASE_UART_DEV);
  /* Set GPIO for UART */
  {
    const XMC_GPIO_CONFIG_t xCfgTx =
    {
      .mode            = XMC_GPIO_MODE_OUTPUT_PUSH_PULL_ALT2,
      .output_level    = XMC_GPIO_OUTPUT_LEVEL_HIGH,
      .output_strength = XMC_GPIO_OUTPUT_STRENGTH_STRONG_SHARP_EDGE
    };
    const XMC_GPIO_CONFIG_t xCfgRx   = {
      .mode            = XMC_GPIO_MODE_INPUT_TRISTATE,
      .output_level    = XMC_GPIO_OUTPUT_LEVEL_HIGH,
      .output_strength = XMC_GPIO_OUTPUT_STRENGTH_STRONG_SHARP_EDGE
    };
    XMC_GPIO_Init(BSPRTNODEBASE_UART_IO_DEV,
                  BSPRTNODEBASE_UART_RX_PIN,
                  &xCfgRx);
    XMC_GPIO_Init(BSPRTNODEBASE_UART_IO_DEV,
                  BSPRTNODEBASE_UART_TX_PIN,
                  &xCfgTx);
  }
  /* Enable UART RX event */
  XMC_USIC_CH_RXFIFO_EnableEvent(
      BSPRTNODEBASE_UART_DEV,
      (uint32_t)XMC_USIC_CH_RXFIFO_EVENT_CONF_STANDARD //|
      //(uint32_t)XMC_USIC_CH_RXFIFO_EVENT_CONF_ALTERNATE
  );
  /* Connect RX FIFO event to SR0 */
  XMC_USIC_CH_RXFIFO_SetInterruptNodePointer(
      BSPRTNODEBASE_UART_DEV,
      XMC_USIC_CH_RXFIFO_INTERRUPT_NODE_POINTER_STANDARD,
      BSPRTNODEBASE_UART_RX_SR
  );
  /* Connect TX FIFO event to SR1 */
  XMC_USIC_CH_TXFIFO_SetInterruptNodePointer(
      BSPRTNODEBASE_UART_DEV,
      XMC_USIC_CH_TXFIFO_INTERRUPT_NODE_POINTER_STANDARD,
      BSPRTNODEBASE_UART_TX_SR
  );
  /* Set priority */
  NVIC_SetPriority(BSPRTNODEBASE_UART_RX_ISR,
                   configUART_RX_INTERRUPT_PRIORITY);
  NVIC_SetPriority(BSPRTNODEBASE_UART_TX_ISR,
                   configUART_TX_INTERRUPT_PRIORITY);
  /* Enable IRQ */
  NVIC_EnableIRQ(BSPRTNODEBASE_UART_RX_ISR);
  NVIC_EnableIRQ(BSPRTNODEBASE_UART_TX_ISR);
}
/*---------------------------------------------------------------------------*/

void eth_init(void)
{
  /* Init globals */
  gStat.ethSrv.ctr.port = gCfg->ethCtrPort;
  gStat.ethSrv.ctr.stat = 0;
  gStat.ethSrv.ctr.err  = false;

  gStat.ethSrv.rec.port = gCfg->ethRecPort;
  gStat.ethSrv.rec.stat = 0;
  gStat.ethSrv.rec.err  = false;

  gStat.ethSrv.snd.port = gCfg->ethSndPort;
  gStat.ethSrv.snd.stat = 0;
  gStat.ethSrv.snd.err  = false;

  gStat.ethSrv.remPort  = gCfg->ethRemPort;
  gStat.ethSrv.delay    = gCfg->ethDelay;

  xRTnetInit(&(gCfg->ethMac0));
}
/*---------------------------------------------------------------------------*/

void vCCU40_0DeferredInterruptHandlerTask(void *pvParameters)
{
  sMsrQei            *dev = &gStat.qei0;
  XMC_POSIF_QD_DIR_t  dir;
  uint32_t            t;
  uint16_t            tic, dT;

  /* Main loop */
  for( ;; ){
    /* Wait for the event */
    ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
    /* Calculate velocity (by "Speed Measurement Algorithms for Low-Resolution
     * Incremental Encoder Equipped Drivers: a Comparative Analysis")*/
    gStat.row.qei0.pos = dev->row.pos;
    tic                = dev->row.tic;
    dT                 = dev->row.dT;
    dir                = dev->row.dir;
    /* Calculate time window */
    t  = SEC / gStat.qei0.f;
    /* Process tics */
    if(gStat.qei0.isExt){
      /* Extend time window */
      t *= gStat.qei0.extSize;
      /* Check if any tics have been captured in this time slot*/
      if(tic > 0){
        gStat.qei0.tic += tic;
        gStat.qei0.extTest++;
        gStat.qei0.extDir = (dir == XMC_POSIF_QD_DIR_COUNTERCLOCKWISE) ? -1 : 1;
      }
      /* Manage extended time window */
      gStat.qei0.extCount--;
      if(gStat.qei0.extCount == 0){
        gStat.qei0.extCount = gStat.qei0.extSize;
        /* Store result */
        gStat.row.qei0.tic  = gStat.qei0.tic;
        gStat.row.qei0.dir  = gStat.qei0.extDir;
        /* Clear helper variables */
        gStat.qei0.tic      = 0;
        gStat.qei0.extDir   = 0;
        /* Check if switch to normal measurement window*/
        if(gStat.qei0.extTest == gStat.qei0.extSize) gStat.qei0.isExt = false;
        gStat.qei0.extTest = 0;
      }
    }
    else{
      if(tic > 0){
        uint16_t dT_prv, dT_act;
        /* Store result */
        gStat.row.qei0.tic = tic;
        gStat.row.qei0.dir = (dir == XMC_POSIF_QD_DIR_COUNTERCLOCKWISE) ? -1 : 1;
        /* Update time window (T_base + dT_prv - dT_act [ns])*/
        dT_act             = dT;
        dT_prv             = gStat.qei0.dT;
        gStat.qei0.dT      = dT;
        {
          uint64_t tmp;
          tmp  = SEC;
          tmp *= dT_prv;
          tmp /= BSPRTNODEBASE_CPU_FREQ >> BSPRTNODEBASE_QEI0_TMR_2_DIV;
          t += (uint32_t)tmp;
        }
        {
          uint64_t tmp;
          tmp  = SEC;
          tmp *= dT_act;
          tmp /= BSPRTNODEBASE_CPU_FREQ >> BSPRTNODEBASE_QEI0_TMR_2_DIV;
          t -= (uint32_t)tmp;
        }
      }
      else{
        gStat.qei0.isExt = true;
        gStat.qei0.dT    = 0;
        gStat.qei0.extCount--;
        /* Extend time window */
        t *= gStat.qei0.extSize;
      }
    }
    /* Set time */
    gStat.row.qei0.t = t;
    /* Read states of ERR inputs */
    gStat.row.qei0.erra =
        XMC_GPIO_GetInput(BSPRTNODEBASE_QEI0_ERR_DEV,
                          BSPRTNODEBASE_QEI0_ERRA_PIN);
    gStat.row.qei0.errb =
        XMC_GPIO_GetInput(BSPRTNODEBASE_QEI0_ERR_DEV,
                          BSPRTNODEBASE_QEI0_ERRB_PIN);
    gStat.row.qei0.erri =
        XMC_GPIO_GetInput(BSPRTNODEBASE_QEI0_ERR_DEV,
                          BSPRTNODEBASE_QEI0_ERRI_PIN);
  }
}
/*---------------------------------------------------------------------------*/

void vCCU41_0DeferredInterruptHandlerTask(void *pvParameters)
{
  sMsrQei            *dev = &gStat.qei1;
  XMC_POSIF_QD_DIR_t  dir;
  uint32_t            t;
  uint16_t            tic, dT;

  /* Main loop */
  for( ;; ){
    /* Wait for the event */
    ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
    /* Calculate velocity (by "Speed Measurement Algorithms for Low-Resolution
     * Incremental Encoder Equipped Drivers: a Comparative Analysis")*/
    gStat.row.qei1.pos = dev->row.pos;
    tic                = dev->row.tic;
    dT                 = dev->row.dT;
    dir                = dev->row.dir;
    /* Calculate time window */
    t  = SEC / gStat.qei1.f;
    /* Process tics */
    if(gStat.qei1.isExt){
      /* Extend time window */
      t *= gStat.qei1.extSize;
      /* Check if any tics have been captured in this time slot*/
      if(tic > 0){
        gStat.qei1.tic += tic;
        gStat.qei1.extTest++;
        gStat.qei1.extDir = (dir == XMC_POSIF_QD_DIR_COUNTERCLOCKWISE) ? -1 : 1;
      }
      /* Manage extended time window */
      gStat.qei1.extCount--;
      if(gStat.qei1.extCount == 0){
        gStat.qei1.extCount = gStat.qei1.extSize;
        /* Store result */
        gStat.row.qei1.tic  = gStat.qei1.tic;
        gStat.row.qei1.dir  = gStat.qei1.extDir;
        /* Clear helper variables */
        gStat.qei1.tic      = 0;
        gStat.qei1.extDir   = 0;
        /* Check if switch to normal measurement window*/
        if(gStat.qei1.extTest == gStat.qei1.extSize) gStat.qei1.isExt = false;
        gStat.qei1.extTest = 0;
      }
    }
    else{
      if(tic > 0){
        uint16_t dT_prv, dT_act;
        /* Store result */
        gStat.row.qei1.tic = tic;
        gStat.row.qei1.dir = (dir == XMC_POSIF_QD_DIR_COUNTERCLOCKWISE) ? -1 : 1;
        /* Update time window (T_base + dT_prv - dT_act [ns])*/
        dT_act             = dT;
        dT_prv             = gStat.qei1.dT;
        gStat.qei1.dT      = dT;
        {
          uint64_t tmp;
          tmp  = SEC;
          tmp *= dT_prv;
          tmp /= BSPRTNODEBASE_CPU_FREQ >> BSPRTNODEBASE_QEI1_TMR_2_DIV;
          t += (uint32_t)tmp;
        }
        {
          uint64_t tmp;
          tmp  = SEC;
          tmp *= dT_act;
          tmp /= BSPRTNODEBASE_CPU_FREQ >> BSPRTNODEBASE_QEI1_TMR_2_DIV;
          t -= (uint32_t)tmp;
        }
      }
      else{
        gStat.qei1.isExt = true;
        gStat.qei1.dT    = 0;
        gStat.qei1.extCount--;
        /* Extend time window */
        t *= gStat.qei1.extSize;
      }
    }
    /* Set time */
    gStat.row.qei1.t = t;
      /* Read states of ERR inputs */
      gStat.row.qei1.erra =
          XMC_GPIO_GetInput(BSPRTNODEBASE_QEI1_ERR_DEV,
                            BSPRTNODEBASE_QEI1_ERRA_PIN);
      gStat.row.qei1.errb =
          XMC_GPIO_GetInput(BSPRTNODEBASE_QEI1_ERR_DEV,
                            BSPRTNODEBASE_QEI1_ERRB_PIN);
      gStat.row.qei1.erri =
          XMC_GPIO_GetInput(BSPRTNODEBASE_QEI1_ERR_DEV,
                            BSPRTNODEBASE_QEI1_ERRI_PIN);
  }
}
/*---------------------------------------------------------------------------*/

void vCmdLineTask(void *pvParam)
{
  sRtStream *dev = pvParam;
  char       line[CMD_LINE_LEN];
  uint32_t   len;

  MAIN_logo(0, NULL);
  while(1){
    len = getLine(dev, line, CMD_LINE_LEN);
    if(len != 0) CMD_shell(line, len);
  }
}
/*---------------------------------------------------------------------------*/

void vTimerCallback(TimerHandle_t pxTimer)
{
  /* Start input update */
  XMC_VADC_GROUP_QueueTriggerConversion(VADC_G0);
  /* Update outputs states */
  updateOutputs();
  /* Test LED */
  XMC_GPIO_ToggleOutput(BSPRTNODEBASE_LED_DEV, BSPRTNODEBASE_LED_G_PIN);
}
/*---------------------------------------------------------------------------*/

void vEthSrvCtrTask(void *pvParam)
{
  sEthSrv          *srv = pvParam;
  sEth             *dev = &srv->ctr;
  xRTnetSocket_t    socket;
  bool              loop;

  /* Wait until RTnet stack will be ready. ????*/
  while(xRTnetWaitRedy(portMAX_DELAY) == pdFAIL){;}
  dev->stat++;
  /* Create the socket */
  loop = true;
  while(loop){
    socket = xRTnetSocket(RTNET_AF_INET,
                          RTNET_SOCK_DGRAM,
                          RTNET_IPPROTO_UDP);
    if(socket == NULL){
      dev->err = true;
      vTaskSuspend(NULL);
      dev->err = false;
    }
    else loop = false;
  }
  dev->stat++;
  /* Binds socket */
  loop = true;
  while(loop){
    xRTnetSockAddr_t  bindAddr;

    bindAddr.sin_port = rtnet_htons(dev->port);
    if(xRTnetBind(socket, &bindAddr, sizeof(&bindAddr)) != 0){
      dev->err = true;
      vTaskSuspend(NULL);
      dev->err = false;
    }
    else loop = false;
  }
  dev->stat++;
  /* Task loop */
  while(1){
    xRTnetSockAddr_t  address;
    sReply            reply;
    uint32_t          ret;
    uint8_t          *data;
    /* Receive using the zero copy semantics */
    ret = lRTnetRecvfrom(socket,
                         &data,
                         0,
                         RTNET_ZERO_COPY,
                         &address,
                         (uint32_t*) sizeof(address));
    if(ret <= 0){
      dev->err = true;
      continue;
    }
    /* Parse frame */
    if(!parseFrameCtr(data, ret, &reply)){
      dev->err = true;
      continue;
    }
    /* Send  using the zero copy semantics */
    ret = lRTnetSendto(socket,
                       reply.frame,
                       reply.size,
                       RTNET_ZERO_COPY,
                       &address,
                       sizeof(address));
    if(ret > 0) dev->err = false;
    else dev->err = true;
  }
}
/*---------------------------------------------------------------------------*/

void vEthSrvRecTask(void *pvParam)
{
  sEthSrv          *srv = pvParam;
  sEth             *dev = &srv->rec;
  xRTnetSocket_t    socket;
  bool              loop;

  /* Wait until RTnet stack will be ready. ????*/
  while(xRTnetWaitRedy(portMAX_DELAY) == pdFAIL){;}
  dev->stat++;
  /* Create the socket */
  loop = true;
  while(loop){
    socket = xRTnetSocket(RTNET_AF_INET,
                          RTNET_SOCK_DGRAM,
                          RTNET_IPPROTO_UDP);
    if(socket == NULL){
      dev->err = true;
      vTaskSuspend(NULL);
      dev->err = false;
    }
    else loop = false;
  }
  dev->stat++;
  /* Binds socket */
  loop = true;
  while(loop){
    xRTnetSockAddr_t  bindAddr;

    bindAddr.sin_port = rtnet_htons(dev->port);
    if(xRTnetBind(socket, &bindAddr, sizeof(&bindAddr)) != 0){
      dev->err = true;
      vTaskSuspend(NULL);
      dev->err = false;
    }
    else loop = false;
  }
  dev->stat++;
  /* Task loop */
  while(1){
    xRTnetSockAddr_t  address;
    uint32_t          ret;
    uint8_t          *data;
    /* Receive using the zero copy semantics */
    ret = lRTnetRecvfrom(socket,
                         &data,
                         0,
                         RTNET_ZERO_COPY,
                         &address,
                         (uint32_t*) sizeof(address));
    XMC_GPIO_ToggleOutput(BSPRTNODEBASE_LED_DEV, BSPRTNODEBASE_LED_R_PIN);
    if(ret <= 0){
      dev->err = true;
      continue;
    }
    /* Parse frame */
    if(parseFrameOut(data, ret, &gStat.node, &gStat.row)) dev->err = false;
    else dev->err = true;
    /* Return the buffer. */
    vRTnetReleaseUdpDataBuffer(data);
  }
}
/*---------------------------------------------------------------------------*/

void vEthSrvSndTask(void *pvParam)
{
  sEthSrv          *srv = pvParam;
  sEth             *dev = &srv->snd;
  xRTnetSockAddr_t  address;
  xRTnetSocket_t    socket;
  bool              loop;
  size_t            size = msgRTnodeBaseInSerializeSize();
  uint16_t          delay = srv->delay;

  /* Wait until RTnet stack will be ready. */
  while(xRTnetWaitRedy(portMAX_DELAY) == pdFAIL){;}
  dev->stat++;
  {
    uint8_t  macBroadcast[] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
    uint32_t ip;

    /* Get node IP address */
    ip  = ulRTnetGetIpAddr();     /* network byte order ip */
    /* Add broadcast address */
    ip |= rtnet_htonl(RTNET_NETMASK_BROADCAST);
    xRTnetRouteAdd(macBroadcast, ip);
    /* Set remote address */
    address.sin_addr = ip;
    address.sin_port = rtnet_htons(srv->remPort);
  }
  /* Create the socket */
  loop = true;
  while(loop){
    socket = xRTnetSocket(RTNET_AF_INET,
                          RTNET_SOCK_DGRAM,
                          RTNET_IPPROTO_UDP);
    if(socket == NULL){
      dev->err = true;
      vTaskSuspend(NULL);
      dev->err = false;
    }
    else loop = false;
  }
  dev->stat++;
  /* Binds socket */
  loop = true;
  while(loop){
    xRTnetSockAddr_t  bindAddr;

    bindAddr.sin_port = rtnet_htons(dev->port);
    if(xRTnetBind(socket, &bindAddr, sizeof(&bindAddr)) != 0){
      dev->err = true;
      vTaskSuspend(NULL);
      dev->err = false;
    }
    else loop = false;
  }
  dev->stat++;
  /* Task loop */
  while(1){
    uint32_t  ret;
    uint8_t  *data;
    /* Wait for next TDMA cycle */
    xRTnetWaitSync(portMAX_DELAY);
    /* Delay frame accordingly */
    delay--;
    if(delay > 0) continue;
    else delay = srv->delay;
    /* Get data buffer */
    data = pvRTnetGetUdpDataBuffer(size);
    if(data == NULL) dev->err = true;
    else{
      ret = prepareFrameIn(data,
                          size,
                          gStat.node.id,
                          GID_ALL,
                          &gStat.row);
      if(ret <= 0) dev->err = true;
      else{
        /* Send  using the zero copy semantics */
        ret = lRTnetSendto(socket,
                           data,
                           size,
                           RTNET_ZERO_COPY,
                           &address,
                           sizeof(address));
        if(ret > 0) dev->err = false;
        else dev->err = true;
      }
    }
    /* Blink ETH led */
    XMC_GPIO_ToggleOutput(BSPRTNODEBASE_LED_DEV, BSPRTNODEBASE_LED_ETH_PIN);
  }
}
/*---------------------------------------------------------------------------*/

void uart_enTx(void)
{
  /* Enable UART TX event */
  XMC_USIC_CH_TXFIFO_EnableEvent(
      BSPRTNODEBASE_UART_DEV,
      (uint32_t)XMC_USIC_CH_TXFIFO_EVENT_CONF_STANDARD
  );
  /*Trigger the transmit buffer interrupt*/
  XMC_USIC_CH_TriggerServiceRequest(BSPRTNODEBASE_UART_DEV,
                                    BSPRTNODEBASE_UART_TX_SR);
}
/*---------------------------------------------------------------------------*/

int getLine(sRtStream *dev, char *line, unsigned int mlen)
{
  unsigned int len = 0;
  char         c;

  while(1){
    if (len == mlen-1){
      line[len] = '\0';
      return -2;
    }
    rtStreamRead(dev, &c, 1, portMAX_DELAY);
    switch(c){
    case '\r':
      line[len] = '\0';
      return len;
    case '\n':
      break;
    default:
      line[len++] = c;
      break;
    }
  }
}
/*---------------------------------------------------------------------------*/

void updateOutputs()
{
  /* Set analog outputs */
  XMC_DAC_CH_Write(BSPRTNODEBASE_AO_DEV,
                   BSPRTNODEBASE_AO_CH0,
                   gStat.row.ao.ch0);
  XMC_DAC_CH_Write(BSPRTNODEBASE_AO_DEV,
                   BSPRTNODEBASE_AO_CH1,
                   gStat.row.ao.ch1);
  /* Set digital outputs */
  XMC_GPIO_SetOutputLevel(BSPRTNODEBASE_DIO_DEV,
                          BSPRTNODEBASE_DIO_O0_PIN,
                          gStat.row.dio.o0 ? XMC_GPIO_OUTPUT_LEVEL_HIGH :
                                             XMC_GPIO_OUTPUT_LEVEL_LOW);
  XMC_GPIO_SetOutputLevel(BSPRTNODEBASE_DIO_DEV,
                          BSPRTNODEBASE_DIO_O1_PIN,
                          gStat.row.dio.o1 ? XMC_GPIO_OUTPUT_LEVEL_HIGH :
                                             XMC_GPIO_OUTPUT_LEVEL_LOW);
}
/*---------------------------------------------------------------------------*/

int rtprintf(const char *format, ...)
{
  sRtStream *stream = &gStat.ser.stream;
  char       buf[PRINTF_LINE_LEN];
  va_list    argLst;
  int        len;

  /* Start the varargs processing */
  va_start(argLst, format);
  /* Call usprintf function to perform the conversion */
  len = uvsnprintf(buf, PRINTF_LINE_LEN, format, argLst);
  if(len >= PRINTF_LINE_LEN) len = PRINTF_LINE_LEN - 1;
  /* End the varargs processing */
  va_end(argLst);
  /* Transmit text through stream device */
  len = rtStreamWrite(stream, buf, len, portMAX_DELAY);
  return len;
}
/*---------------------------------------------------------------------------*/

void setDefCfg(sMsgRTnodeBaseCfg_t *cfg)
{
  cfg->id          = DEF_ID;
  cfg->gidLen      = DEF_GID_LEN;
  cfg->gid0        = DEF_GID_0;
  cfg->gid1        = DEF_GID_1;
  cfg->gid2        = DEF_GID_2;
  cfg->gid3        = DEF_GID_3;
  cfg->gid4        = DEF_GID_4;
  cfg->gid5        = DEF_GID_5;
  cfg->gid6        = DEF_GID_6;
  cfg->gid7        = DEF_GID_7;
  cfg->gid8        = DEF_GID_8;
  cfg->gid9        = DEF_GID_9;
  cfg->ethMac0     = DEF_ETH_MAC_ADDR0;
  cfg->ethMac1     = DEF_ETH_MAC_ADDR1;
  cfg->ethMac2     = DEF_ETH_MAC_ADDR2;
  cfg->ethMac3     = DEF_ETH_MAC_ADDR3;
  cfg->ethMac4     = DEF_ETH_MAC_ADDR4;
  cfg->ethMac5     = DEF_ETH_MAC_ADDR5;
  cfg->ethCtrPort  = DEF_ETH_CTR_PORT;
  cfg->ethRecPort  = DEF_ETH_REC_PORT;
  cfg->ethSndPort  = DEF_ETH_SND_PORT;
  cfg->ethRemPort  = DEF_ETH_REM_PORT;
  cfg->ethDelay    = DEF_ETH_DELAY;
  cfg->updateFreq  = DEF_UPDATE_FREQ;
  cfg->qei0VelFreq = DEF_QEI0_VEL_FREQ;
  cfg->qei0VelExt  = DEF_QEI0_VEL_EXT;
  cfg->qei1VelFreq = DEF_QEI1_VEL_FREQ;
  cfg->qei1VelExt  = DEF_QEI1_VEL_EXT;
}
/*---------------------------------------------------------------------------*/

bool parseFrameCtr(uint8_t  *frame,
                   uint32_t  size,
                   sReply   *reply)
{
  uint8_t id = 0;
  int     ret;

  ret = parseFrameCtrWorker(frame, size, reply, &id);
  switch(ret){
  case CTRWORKER_FREE_FRAME:
    /* Release the frame buffer */
    vRTnetReleaseUdpDataBuffer(frame);
    return false;
  case CTRWORKER_OK:
    /* Replay is ready so return immediately */
    return true;
  default:
    /* Send ERR (reuse old frame buffer)*/
    size = prepareFrameCtr(frame,
                           size,
                           id,
                           MSGRTNODEBASECTRDEF_TYPE_ERR,
                           ret);
    if(size <= 0){
      /* Return the buffer. */
      vRTnetReleaseUdpDataBuffer(frame);
      return false;
    }
    break;
  }
  /* Prepare replay */
  reply->frame = frame;
  reply->size  = size;
  return true;
}
/*---------------------------------------------------------------------------*/

int parseFrameCtrWorker(uint8_t  *frame,
                        uint32_t  size,
                        sReply   *reply,
                        uint8_t  *id)
{
  sMsgRTnodeBaseCtr_t msg;
  int                 ret;

  /* Deserialize control structure */
  ret = msgRTnodeBaseCtrDeSerialize(&msg, frame, size);
  if(ret <= 0) return CTRWORKER_ERR(FRAME);
  /* Set message id */
  *id = msg.id;
  /* Check message type */
  switch(msg.type){
  case MSGRTNODEBASECTRDEF_TYPE_PING:
    return CTRWORKER_ERR(OK);
  case MSGRTNODEBASECTRDEF_TYPE_REBOOT:
    /* Delay system reboot (prm holds number of seconds)*/
    vTaskDelay(msg.prm * configTICK_RATE_HZ);
    /* Reboot system */
    NVIC_SystemReset();
    /* Should never get here, just in case*/
    return CTRWORKER_FREE_FRAME;
  case MSGRTNODEBASECTRDEF_TYPE_CFGNEW:
  {
    sMsgRTnodeBaseCfg_t *cfg;

    /* Allocate space for configuration */
    cfg = pvPortMalloc(sizeof(sMsgRTnodeBaseCfg_t));
    if(cfg == NULL) return CTRWORKER_ERR(MEM);
    /* Deserialize config message */
    ret = msgRTnodeBaseCfgDeSerialize(cfg, frame + ret, size - ret);
    if(ret <= 0){
      /* Free allocated memory */
      vPortFree(cfg);
      return CTRWORKER_ERR(FRAME);
    }
    if(!flashStorageBlockStore(&gStat.fsb,
                               cfg,
                               sizeof(sMsgRTnodeBaseCfg_t))){
      /* Free allocated memory */
      vPortFree(cfg);
      return CTRWORKER_ERR(FLASH);
    }
    /* Free allocated memory */
    vPortFree(cfg);
    return CTRWORKER_ERR(OK);
  }
  case MSGRTNODEBASECTRDEF_TYPE_CFGREQ:
  {
    sMsgRTnodeBaseCfg_t *cfg;
    uint32_t             len;
    uint8_t             *ptr;

    /* Allocate new frame */
    size = msgRTnodeBaseCtrSerializeSize() + msgRTnodeBaseCfgSerializeSize();
    ptr  = pvRTnetGetUdpDataBuffer(size);
    if(ptr == NULL) return CTRWORKER_ERR(MEM);
    /* Insert ctr message into frame */
    len = prepareFrameCtr(ptr,
                          size,
                          msg.id,
                          MSGRTNODEBASECTRDEF_TYPE_CFGREP,
                          0);
    if(len <= 0){
      vRTnetReleaseUdpDataBuffer(ptr);
      return CTRWORKER_ERR(INT);
    }
    /* Get configuration from flash */
    cfg = flashStorageBlockRestore(&gStat.fsb, NULL);
    if(cfg == NULL){
      vRTnetReleaseUdpDataBuffer(ptr);
      return CTRWORKER_ERR(FLASH);
    }
    /* Insert cfg message into frame */
    len = msgRTnodeBaseCfgSerialize(cfg, ptr + len, size - len);
    if(len <= 0){
      vRTnetReleaseUdpDataBuffer(ptr);
      return CTRWORKER_ERR(INT);
    }
    /* Release old frame buffer */
    vRTnetReleaseUdpDataBuffer(frame);
    /* Update frame buffer pointer */
    frame = ptr;
    break;
  }
  default:
    return CTRWORKER_ERR(UNKNOWN);
  }
  /* Prepare replay */
  reply->frame = frame;
  reply->size  = size;
  return CTRWORKER_OK;
}
/*---------------------------------------------------------------------------*/

int prepareFrameCtr(uint8_t  *frame,
                    uint32_t  size,
                    uint8_t   id,
                    uint8_t   type,
                    uint32_t  prm)
{
  sMsgRTnodeBaseCtr_t msg = {
    .id = id,
    .type = type,
    .prm  = prm
  };
  return msgRTnodeBaseCtrSerialize(&msg, frame, size);
}
/*---------------------------------------------------------------------------*/

int prepareFrameIn(uint8_t           *frame,
                   uint32_t           size,
                   uint8_t            id,
                   uint8_t            gid,
                   sBspRTnodeBaseRow *row)
{
  sMsgRTnodeBaseIn_t msg = {
    .id   = id,
    .gid  = gid,
    .ch00 = row->ai.ch00,
    .ch01 = row->ai.ch01,
    .ch02 = row->ai.ch02,
    .ch03 = row->ai.ch03,
    .i0   = row->dio.i0,
    .i1   = row->dio.i1,
    .pos0 = row->qei0.pos,
    .tic0 = row->qei0.tic,
    .t0   = row->qei0.t,
    .dir0 = row->qei0.dir,
    .err0 = row->qei0.err,
    .pos1 = row->qei1.pos,
    .tic1 = row->qei1.tic,
    .t1   = row->qei1.t,
    .dir1 = row->qei1.dir,
    .err1 = row->qei1.err,
  };
  return msgRTnodeBaseInSerialize(&msg, frame, size);
}
/*---------------------------------------------------------------------------*/

bool parseFrameOut(uint8_t           *frame,
                   uint32_t            size,
                   sNodeId            *node,
                   sBspRTnodeBaseRow  *row)
{
  uint32_t proc, left;
  int      ret;

  proc = 0;
  left = size;
  while(proc < size){
    sMsgRTnodeBaseOut_t msg;
    ret = msgRTnodeBaseOutDeSerialize(&msg, frame, left);
    if(ret <= 0) return false;
    frame += ret;
    proc  += ret;
    left  -= ret;
    /* Check if recipient */
    if(isRecipient(node, msg.id, msg.gid)){
      if(TESTBIT(msg.mask, MSGRTNODEBASEOOUT_BIT_CH0))
        row->ao.ch0 = msg.ch00 & 0x0FFF;
      if(TESTBIT(msg.mask, MSGRTNODEBASEOOUT_BIT_CH1))
        row->ao.ch1 = msg.ch01 & 0x0FFF;
      if(TESTBIT(msg.mask, MSGRTNODEBASEOOUT_BIT_O0))
        row->dio.o0 = msg.o0;
      if(TESTBIT(msg.mask, MSGRTNODEBASEOOUT_BIT_O1))
        row->dio.o1 = msg.o1;
      updateOutputs();
    }
  }
  return true;
}
/*---------------------------------------------------------------------------*/

bool isRecipient(sNodeId *node, uint8_t id, uint8_t gid)
{
  /* Check if group id matches*/
  if(gid != GID_ALL){
    int i;
    for(i = 0; i < node->gidLen; i++){
      if(node->gid[i] == gid) break;
    }
    if(i == node->gidLen) return false;
  }
  if((id != ID_ALL) && (id != node->id)) return false;
  return true;
}
/*---------------------------------------------------------------------------*/
