/*****************************************************************************
 *                                                                           *
 *   rtStream.h                                                              *
 *                                                                           *
 *   Free RTOS interrupt driven stream implementation                        *
 *                                                                           *
 *   Copyright (C) 2010 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                               *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#ifndef _RTSTREAM_H_
#define _RTSTREAM_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "stdbool.h"
#include "fifo8b.h"
#include "FreeRTOS.h"
#include "semphr.h"

/***************************************************************************** 
 * Constants
 *****************************************************************************/

#define RTSTREAM_ERR_ARG     -1
#define RTSTREAM_ERR_TIMEOUT -2

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

typedef void(*fUartio)(void);

typedef struct{
  sFifo8b 	       rxFifo;
  sFifo8b 	       txFifo;
  fUartio 	       enTx;
  xSemaphoreHandle xSemRx;
  xSemaphoreHandle xSemTx;
  uint32_t          rxCount;
  uint32_t          txCount;
} sRtStream;

/*****************************************************************************
 * Globals variables 
 *****************************************************************************/

/***************************************************************************** 
 * Function prototypes
 *****************************************************************************/

/***************************************************************************** 
 *
 * Initialize UART input/output structure
 *
 * \param  dev         	 points to stream structure
 * \param  tx          	 points to transmitter data buffer,  
 * \param  txLen       	 transmitter data length,  
 * \param  rx          	 points to receiver data buffer,  
 * \param  rxLen       	 receiver data length,  
 * \param  enTx 	       function that start data transmission,
  * \return Returns the TRUE value if succeed initialization, FALSE in
 *         other case.
 *
 *****************************************************************************/

bool rtStreamInit(sRtStream *dev, uint8_t *rx, uint32_t rxLen, uint8_t *tx,
		   uint32_t txLen, fUartio enTx);

/***************************************************************************** 
 *
 * Get byte to transmit (must be called by transmitter interrupt service 
 * routine)
 *
 * \param  dev  points to stream structure
 * \param  data points to variable where byte to transmit will be stored 
 * \return Returns the TRUE if there is byte to transmit, FALSE in other case
 *
 *****************************************************************************/

bool rtStreamIntTxByte(sRtStream *dev, uint8_t *data);

/*****************************************************************************
 *
 * Post process stream after transmition of number of bytes (must be called by
 * transmitter interrupt service routine)
 *
 * \param  dev  points to stream structure
 * \param  hptw higher priority task woken 
 * \return Returns the TRUE if at least one byte has been transmitted, FALSE in
 *         other case.
 *
 *****************************************************************************/

bool rtStreamIntTxBytePost(sRtStream *dev, BaseType_t *hptw);

/*****************************************************************************
 *
 * Get data block to transmit (must be called by transmitter interrupt service
 * routine)
 *
 * \param  dev  points to stream structure
 * \param  data points to variable where data block to transmit will be stored
 * \param  len  requested data block length
 * \param  hptw higher priority task woken
 * \return Returns the TRUE if there is byte to transmit, FALSE in other case
 *
 *****************************************************************************/

uint32_t rtStreamIntTxData(sRtStream  *dev,
                           uint8_t    *data,
                           uint32_t    len,
                           BaseType_t *hptw);

/***************************************************************************** 
 *
 * Store received byte (must be called by receiver interrupt service routine)
 *
 * \param  dev  points to stream structure
 * \param  data received byte 
 * \return Returns the TRUE if there is a space for received byte, FALSE in
 *         other case
 *
 *****************************************************************************/

bool rtStreamIntRxByte(sRtStream *dev, uint8_t data);

/*****************************************************************************
 *
 * Post process stream after reception of number of bytes (must be called by
 * receiver interrupt service routine)
 *
 * \param  dev  points to stream structure
 * \param  hptw higher priority task woken 
 * \return Returns the TRUE if at least one byte has been received, FALSE in
 *         other case 
 *
 *****************************************************************************/

bool rtStreamIntRxBytePost(sRtStream *dev, BaseType_t  *hptw);

/*****************************************************************************
 *
 * Store received data block (must be called by transmitter interrupt service
 * routine)
 *
 * \param  dev  points to stream structure
 * \param  data points to variable where data block to transmit will be stored
 * \param  len  requested data block length
 * \param  hptw higher priority task woken
 * \return Returns the TRUE if there is byte to transmit, FALSE in other case
 *
 *****************************************************************************/

uint32_t rtStreamIntRxData(sRtStream  *dev,
                           uint8_t    *data,
                           uint32_t    len,
                           BaseType_t *hptw);

/***************************************************************************** 
 *
 * Check number of available data in receiver buffer   
 *
 * \param  dev points to stream structure
 * \return Returns number of available data in receiver buffer.
 *
 *****************************************************************************/
  
uint32_t rtStreamRxStat(sRtStream *dev);

/***************************************************************************** 
 *
 * Check number of available data in transmitter buffer   
 *
 * \param  dev points to stream structure
 * \return Returns number of available data in transmitter buffer.
 *
 *****************************************************************************/

uint32_t rtStreamTxStat(sRtStream *dev);

/***************************************************************************** 
 *
 * Check number of available data in transmitter buffer (call from ISR)
 *
 * \param  dev points to stream structure
 * \return Returns number of available data in transmitter buffer.
 *
 *****************************************************************************/

uint32_t rtStreamTxStatFromISR(sRtStream *dev);

/*****************************************************************************
 *
 * Read data from stream
 *
 * \param  dev     points to stream structure
 * \param  buf     points to a buffer where a received data will be stored
 * \param  len     requested data length to read
 * \param  timeout read timeout
 * \return On success, the number of bytes read is returned, otherwise
 *           - RTSTREAM_ERR_ARG - wrong arguments, usually one of 'dev' or buf
 *                                is void,
 *           - RTSTREAM_ERR_TIMEOUT - no data has been read during given period.
 *
 *****************************************************************************/

int32_t rtStreamRead(sRtStream *dev, void* buf, uint32_t len,
                    TickType_t  timeout);

/*****************************************************************************
 *
 * Write data to stream
 *
 * \param  dev     points to stream structure
 * \param  buf     points to a buffer containing data to send
 * \param  len     requested data length to write
 * \param  timeout write timeout
 * \return On success, the number of bytes written is returned, otherwise
 *           - RTSTREAM_ERR_ARG - wrong arguments, usually one of 'dev' or buf
 *                                is void,
 *           - RTSTREAM_ERR_TIMEOUT - no data has been written during given
 *                                    period.
 *
 *****************************************************************************/

int32_t rtStreamWrite(sRtStream *dev, void* buf, uint32_t len,
                     TickType_t  timeout);

#ifdef __cplusplus
}
#endif

#endif /* _RTSTREAM_H_ */
