/*****************************************************************************
 *                                                                           *
 *   RTnetConfig.h                                                           *
 *                                                                           *
 *   RTnet configuration file                                                *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#ifndef _FREERTOSRTNETCONFIG_H_
#define _FREERTOSRTNETCONFIG_H_

#include "FreeRTOSConfig.h"

/* rtnetconfigRTBUF_NUMBER defines the total number of real-time data buffers
 * that are available to the RTnet stack.*/

#define rtnetconfigRTBUF_NUMBER (10)

/* The maximum transmission unit (MTU) supported by Ethernet controller */

#define rtnetconfigETHERNET_MTU_SIZE (1500UL)

/* rtnetconfigTDMA_SLOTS_NUMBER defines the total number of TDMA slots that
 * are available to the RTnet stack.*/

#define rtnetconfigTDMA_SLOTS_NUMBER (4)

/* The length of the TDMA discipline transmit queue. */

#define rtnetconfigTDMA_FIFO_LENGTH  (rtnetconfigTDMA_SLOTS_NUMBER + 2)

/* A FreeRTOS queue is used to send events from application tasks to the RTnet
 * stack. The rtnetconfigEVENT_QUEUE_LENGTH sets the maximum number of events
 * that can be queued for processing at any one time. */

#define rtnetconfigEVENT_QUEUE_LENGTH (10)

/* The transmit queue length  */

#define rtnetconfigTX_QUEUE_LENGTH (rtnetconfigTDMA_FIFO_LENGTH)

/* The size, in words (not bytes), of the stack allocated to the RTnet task. */

#define rtnetconfigRTNET_TASK_STACK_SIZE (configMINIMAL_STACK_SIZE * 2)

/* The RTnet stack executes it its own task. The rtnetconfigRTNET_TASK_PRIORITY
 * sets the priority of the task that executes the RTnet stack.  The priority
 * is a standard FreeRTOS task priority so can take any value from 0
 * (the lowest priority) to (configMAX_PRIORITIES - 1) (the highest priority).
 * The configMAX_PRIORITIES is a standard FreeRTOS configuration parameter
 * defined in FreeRTOSConfig.h. Consideration needs to be given as to the
 * priority assigned to the task executing the RTnet stack relative to the
 * priority assigned to tasks that use this stack. */

#define rtnetconfigRTNET_TASK_PRIORITY (configMAX_PRIORITIES - 3)

/* Define target machine byte order (endianness), please uncomment suitable. */

#define rtnetconfigBYTE_ORDER_LITTLE_ENDIAN 1234
//#define rtnetconfigBYTE_ORDER_BIG_ENDIAN    4321

/* The TDMA discipline maximum cycle length given in [ns] (default 1s). */

#define rtnetconfigTDMA_CYCLE_MAX (1000000000ULL)

/* The maximum number of command line arguments for stage 1. */

#define rtnetconfigRTCFG_CMD_ARG_NUM 20

/* The Stage 2 Burst Rate field specifies the number of stage 2 configuration
 * frames the client is able to receive without sending an Acknowledge
 * Configuration frame. */

#define rtnetconfigRTCFG_BURST_RATE  (2)

/* The size of the ARP cache */

#define rtnetconfigARP_TAB_SIZE (10)

#endif /* _FREERTOSRTNETCONFIG_H_ */
