/*****************************************************************************
 *                                                                           *
 *   globals.h                                                               *
 *                                                                           *
 *   Global constants, type definitions, macros and variables                *
 *                                                                           *
 *   Copyright (C) 2014 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                               *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <xmc_posif.h>
#include "bspRTnodeBase.h"
#include "flashStorageBlock.h"
#include "rtnet_common.h"
#include "rtStream.h"
#include "movingAverage.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "msgRTnodeBaseCfg.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

#ifdef GEXTERN
#  undef GEXTERN
#endif

#ifndef _GLOBALS_NO_EXTERN_
#  define GEXTERN extern
#else
#  define GEXTERN
#endif

/* Controller version */
#define VER_MAJOR                1
#define VER_MINOR                0

/* Serial interface */
#define SERIAL_FIFO_RX_SIZE     160
#define SERIAL_FIFO_TX_SIZE     160
#define CMD_LINE_LEN            80
#define PRINTF_LINE_LEN         80


/* Node id */

#define ID_ALL                  0xFF
#define GID_ALL                 0xFF

#define DEF_ID                  0

#define DEF_GID_SIZE            10

#define DEF_GID_LEN             1

#define DEF_GID_0               0
#define DEF_GID_1               GID_ALL
#define DEF_GID_2               GID_ALL
#define DEF_GID_3               GID_ALL
#define DEF_GID_4               GID_ALL
#define DEF_GID_5               GID_ALL
#define DEF_GID_6               GID_ALL
#define DEF_GID_7               GID_ALL
#define DEF_GID_8               GID_ALL
#define DEF_GID_9               GID_ALL

/* ETH interface*/
#define DEF_ETH_MAC_ADDR0       0x00
#define DEF_ETH_MAC_ADDR1       0x01
#define DEF_ETH_MAC_ADDR2       0x02
#define DEF_ETH_MAC_ADDR3       0x03
#define DEF_ETH_MAC_ADDR4       0x04
#define DEF_ETH_MAC_ADDR5       0x09

#define DEF_ETH_CTR_PORT        1523
#define DEF_ETH_REC_PORT        1524
#define DEF_ETH_SND_PORT        2001

#define DEF_ETH_REM_PORT        1525

#define DEF_ETH_DELAY           200UL

/* Row inputs */
#define DEF_UPDATE_FREQ         500UL  /* Tick */

#define DEF_QEI0_VEL_FREQ       1000UL /* 1kHz */
#define DEF_QEI1_VEL_FREQ       1000UL /* 1kHz */

#define DEF_QEI0_VEL_EXT        5      /* Extended window size */
#define DEF_QEI1_VEL_EXT        5      /* Extended window size */

/*****************************************************************************
 * Macros
 *****************************************************************************/

#define TESTBIT(val, bit)      ((val) & (1UL << (bit)))
#define GETBIT(val, bit)       (((val) & (1UL << (bit))) ? TRUE : FALSE)
#define SETBIT(val, bit)       ((val) | (1UL << (bit)))
#define CLRBIT(val, bit)       ((val) & ~(1UL << (bit)))
#define CPYBIT(val, bit, src)  ((src) ? (val | (1UL << (bit))) : \
                                        (val & ~(1UL << (bit))))
#define SETBITW(val, bit)      (val |=  (1UL << (bit)))
#define CLRBITW(val, bit)      (val &= ~(1UL << (bit)))
#define CPYBITW(val, bit, src) ((src) ? (val |=  (1UL << (bit))) : \
                                        (val &= ~(1UL << (bit))))
#define MASK32(bit)            (1UL << (bit))
#define WORDMSB(val)           ((uint8_t)((val) >> 8))
#define WORDLSB(val)           ((uint8_t)((val) & 0xFF))
#define MSBLSBWORD(msb, lsb)   ((uint16_t)(((msb) << 8) | (lsb)))

#define ABS(val)               (((val) < 0) ? -(val) : (val))

#define CEILDIV_UINT(NUM, DEN) ((NUM)/(DEN) + ((NUM) % (DEN) != 0))

/* The fastest way to set an array */
/* Exemple:                        */
/* int foo[16];                    */
/* SETARRAY(foo, 1, 2, 3);         */
#define SETARRAY(ARRAY, ...)                                           \
  memcpy(ARRAY, ((__typeof__(ARRAY)){ __VA_ARGS__ }), sizeof(ARRAY))

/* The fastest way to clear an array */
#define CLEARARRAY(ARRAY) memset(ARRAY, 0, sizeof(ARRAY))
#define CLEARARRAYSIZE(ARRAY,SIZE) memset(ARRAY, 0, SIZE)

/* Static assertion */
#define ASSERT_CONCAT_(a, b) a##b
#define ASSERT_CONCAT(a, b) ASSERT_CONCAT_(a, b)
/** These can't be used after statements in c89. */
#ifdef __COUNTER__
  #define STATIC_ASSERT(e,m) \
    { enum { ASSERT_CONCAT(static_assert_, __COUNTER__) = 1/(!!(e)) }; }
#else
  /* This can't be used twice on the same line so ensure if using in headers
   * that the headers are not included twice (by wrapping in #ifndef...#endif)
   * Note it doesn't cause an issue when used on same line of separate modules
   * compiled with gcc -combine -fwhole-program.  */
  #define STATIC_ASSERT(e,m) \
    { enum { ASSERT_CONCAT(assert_line_, __LINE__) = 1/(!!(e)) }; }
#endif

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

typedef struct{
  uint8_t           rx[SERIAL_FIFO_RX_SIZE];
  uint8_t           tx[SERIAL_FIFO_TX_SIZE];
  sRtStream         stream;
  TaskHandle_t      task;
  TaskHandle_t      task1;
} sSer;

typedef struct{
  xTaskHandle       task;
  uint32_t          stat;
  uint16_t          port;
  bool              err;
} sEth;

typedef struct{
  sEth              ctr;
  sEth              rec;
  sEth              snd;
  uint16_t          remPort;
  uint32_t          delay;
  bool              link;
} sEthSrv;

typedef struct{
  XMC_POSIF_QD_DIR_t dir;
  uint16_t           pos;
  uint16_t           tic;
  uint16_t           dT;
} sMsrQeiRow;


typedef struct{
  TaskHandle_t      task;
  sMsrQeiRow        row;
  uint32_t          f;
  uint16_t          tic;
  uint16_t          dT;
  uint8_t           extSize;
  uint8_t           extCount;
  uint8_t           extTest;
  int8_t            extDir;
  bool              isExt;
} sMsrQei;

typedef struct{
  uint8_t           id;
  uint8_t           gidLen;
  uint8_t           gid[DEF_GID_SIZE];
} sNodeId;

typedef struct{
  uint32_t           sysClock;
  sEthSrv            ethSrv;
  sSer               ser;
  sMsrQei            qei0;
  sMsrQei            qei1;
  sNodeId            node;
  sBspRTnodeBaseRow  row;
  sflashStorageBlock fsb;
  TimerHandle_t      xTimer;
} sStat;

/*****************************************************************************
 * Globals variables
 *****************************************************************************/

GEXTERN sStat gStat;

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

int  rtprintf(const char *format, ...);
void setDefCfg(sMsgRTnodeBaseCfg_t *cfg);

#ifdef __cplusplus
}
#endif

#endif /* _GLOBALS_H_ */
