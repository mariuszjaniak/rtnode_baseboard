/*****************************************************************************
 *                                                                           *
 *   flashStorageBlock.h                                                     *
 *                                                                           *
 *   Flash storage for data blocks                                           *
 *                                                                           *
 *   Copyright (C) 2015 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                               *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *****************************************************************************/

#ifndef _FLASHSTORAGEBLOCK_
#define _FLASHSTORAGEBLOCK_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "stdbool.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

/*****************************************************************************
  * Type definitions
 *****************************************************************************/

typedef struct {
  void     *start;
  void     *end;
  void     *current;
  uint32_t  esize;               // erase block size
  uint32_t  psize;               // program block size
} sflashStorageBlock;

/*****************************************************************************
 * Globals variables 
 *****************************************************************************/

/***************************************************************************** 
 * Function prototypes
 *****************************************************************************/

void flashStorageBlockInit(sflashStorageBlock *dev,
                           void               *start,
                           void               *end,
                           uint32_t            esize,
                           uint32_t            psize);

bool flashStorageBlockStore(sflashStorageBlock *dev,
                            void               *block,
                            uint32_t            bsize);

void* flashStorageBlockRestore(sflashStorageBlock *dev, uint32_t *bsize);

#ifdef __cplusplus
}
#endif

#endif /* End _FLASHSTORAGEBLOCK_ */
