/***************************************************************************
 *                                                                         *
 *   ustdlib.h                                                             *
 *                                                                         *
 *   Prototypes for simple standard library functions.                     *
 *                                                                         *
 *   Copyright (C) 2009 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.edu.pl                                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __USTDLIB_H__
#define __USTDLIB_H__

#include <stdarg.h>

#ifdef __cplusplus
extern "C"
{
#endif

/*****************************************************************************
 * Constants
 *****************************************************************************/

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

extern int uvsnprintf(char *pcBuf, unsigned long ulSize, const char *pcString,
                      va_list vaArgP);
extern int usprintf(char *pcBuf, const char *pcString, ...);
extern int usnprintf(char *pcBuf, unsigned long ulSize, const char *pcString,
                     ...);
extern char *ustrncpy (char *pcDst, const char *pcSrc, int iNum);


#ifdef __cplusplus
}
#endif

#endif // __USTDLIB_H__
