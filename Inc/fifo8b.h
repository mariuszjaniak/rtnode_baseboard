/*****************************************************************************
 *                                                                           *
 *   fifo8b.h                                                                *
 *                                                                           *
 *   Byte size software FIFO                                                 *
 *                                                                           *
 *   Copyright (C) 2009 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *****************************************************************************/

#ifndef _FIFO8B_
#define _FIFO8B_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "stdbool.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

/*****************************************************************************
  * Type definitions
 *****************************************************************************/

typedef struct {
  uint32_t in;            /* begin of the buffer*/
  uint32_t out;           /* end of the buffer*/
  uint32_t size;          /* fifo buffer size */
  bool     full;          /* full flag */
  uint8_t  *q;             /* buffer */
} sFifo8b;

/*****************************************************************************
 * Globals variables 
 *****************************************************************************/

/***************************************************************************** 
 * Function prototypes
 *****************************************************************************/

/* Init fifo */
void fifo8bInit(sFifo8b *fifo, uint8_t *buf, uint32_t size);

/* Get fifo state */
uint32_t fifo8bStat(sFifo8b *fifo);

/* Get byte from fifo */
bool fifo8bGet(sFifo8b *fifo, uint8_t *data);

/* Put byte into fifo */
bool fifo8bPut(sFifo8b *fifo, uint8_t data);

/* Read data from fifo*/
uint32_t fifo8bRead(sFifo8b *fifo, uint8_t *data, uint32_t len);

/* Write data to fifo*/
uint32_t fifo8bWrite(sFifo8b *fifo, uint8_t *data, uint32_t len);

/* Remove last added byte from fifo */
bool fifo8bDel(sFifo8b *fifo);

#ifdef __cplusplus
}
#endif

#endif /* End _FIFO8B_ */
