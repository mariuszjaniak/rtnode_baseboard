/***************************************************************************
 *                                                                         *
 *   cmdline.h                                                             *
 *                                                                         *
 *   Command lines processor                                               *
 *                                                                         *
 *   Copyright (C) 2009 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.edu.pl                                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/

#ifndef _CMDLINE_H_
#define _CMDLINE_H_

#ifdef __cplusplus
extern "C"
{
#endif

/***************************************************************************** 
 * Constants
 *****************************************************************************/

#define CMD_MAX_ARGS          20

#define CMD_LINE_LEN          80


#define CMD_ERR               -101
#define CMD_ARGS              -102
#define CMD_NOERR             0

#define CMD_MSG_NOERR         "!OK\r\n"
#define CMD_MSG_ARGS          "!ARG\r\n"
#define CMD_MSG_ERR           "!ERR\r\n"

#ifdef UNUSED
#elif defined(__GNUC__)
# define UNUSED(x) UNUSED_ ## x __attribute__((unused))
#else
# define UNUSED(x) x
#endif

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

/* Command line function callback type */
typedef int (*CMD_callback_fun)(int argc, char *argv[]); 
typedef int (*CMD_prinf_fun)(const char *pcString, ...); 

/* Structure for an entry in the command list table. */
typedef struct
{
  /* A pointer to a string containing the name of the command */
  const char *pcCmd;
  /* A function pointer to the implementation of the command */
  CMD_callback_fun pfnCmd;
  /* A pointer to a string of brief help text for the command. */
  const char *pcHelp;
} CMD_lineEntry_str;

/*****************************************************************************
 * Globals variables 
 *****************************************************************************/

extern CMD_lineEntry_str  gCmdTable[];  /* The command table must be
					   implemented by the application. */

extern CMD_prinf_fun      gCmdPrintf;   /* The printf function must be
					   implemented by the application. */

extern CMD_callback_fun   gCmdInput;    /* The command input function, if NULL
					 * shell operates in normal mode, if
					 * set shell operates in data input
					 * mode for last called command */


/***************************************************************************** 
 * Function prototypes
 *****************************************************************************/

int CMD_shell(char *line, int n);
int CMD_help(int argc, char *argv[]);
int CMD_logo(int argc, char *argv[]);

#ifdef __cplusplus
}
#endif

#endif /* _CMDLINE_H_ */
