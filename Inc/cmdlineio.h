/***************************************************************************
 *                                                                         *
 *   cmdlineio.h                                                           *
 *                                                                         *
 *   Command line input/output instructions                                *
 *                                                                         *
 *   Copyright (C) 2009 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.edu.pl                                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/

#ifndef _CMDLINEIO_H_
#define _CMDLINEIO_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "cmdline.h"

/***************************************************************************** 
 * Constants
 *****************************************************************************/

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

/*****************************************************************************
 * Globals variables 
 *****************************************************************************/

/***************************************************************************** 
 * Function prototypes
 *****************************************************************************/

int MAIN_ver(int argc, char *argv[]);
int MAIN_logo(int argc, char *argv[]);
int MAIN_s(int argc, char *argv[]);
int MAIN_p(int argc, char *argv[]);
int MAIN_io(int argc, char *argv[]);
int MAIN_o(int argc, char *argv[]);
int MAIN_c(int argc, char *argv[]);
int MAIN_id(int argc, char *argv[]);
int MAIN_reboot(int argc, char *argv[]);
int MAIN_reset(int argc, char *argv[]);

#ifdef __cplusplus
}
#endif

#endif /* _CMDLINEIO_H_ */
