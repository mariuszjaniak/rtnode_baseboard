/***************************************************************************
 *                                                                         *
 *   movingAverage.h                                                       *
 *                                                                         *
 *   Moving average filter                                                 *
 *                                                                         *
 *   Copyright (C) 2010 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.wroc.pl                                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef _MOVINGAVERAGE_
#define _MOVINGAVERAGE_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

/***************************************************************************** 
 * Constants
 *****************************************************************************/

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

typedef struct{
  int32_t *z;                /* data buffer */
  int64_t  y;                /* previous output value (not scaled)*/
  uint32_t v;                /* define filter length n = 2^v (n = 1<<v) */
  uint32_t s;                /* filter state */
}sMA32b;

/***************************************************************************** 
 * Function prototypes
 *****************************************************************************/

/***************************************************************************** 
 *
 * Initialize moving average filter structure  
 *
 * \param par points to moving average structure 
 * \param z   points to data buffer  
 * \param v   define data buffer length n = 2^v (n = 1<<v)
 *
 *****************************************************************************/

void   movingAverage32bInit(sMA32b *par, int32_t *z, uint32_t v);

/***************************************************************************** 
 *
 * Calculate filter output   
 *
 * \param par points to moving average structure 
 * \param in  input value   
 * \return    output value 
 *
 *****************************************************************************/

int32_t movingAverage32b(sMA32b *par, int32_t in);

#ifdef __cplusplus
}
#endif

#endif /* End _MOVINGAVERAGE_ */
