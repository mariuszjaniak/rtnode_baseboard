/***************************************************************************
 *                                                                         *
 *   bspRTnodeBase.h                                                       *
 *                                                                         *
 *   RTnode base board support package                                     *
 *                                                                         *
 *   Copyright (C) 2015 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.edu.pl                                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/

#ifndef _BSPRTNODEBASE_H_
#define _BSPRTNODEBASE_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <xmc_vadc.h>
#include <xmc_dac.h>
#include <xmc_gpio.h>
#include <xmc_flash.h>

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define BSPRTNODEBASE_CPU_FREQ       120000000UL

#define BSPRTNODEBASE_AI_DEV         VADC
#define BSPRTNODEBASE_AI_DEV_0       VADC_G0
#define BSPRTNODEBASE_AI_SIZE        4UL

#define BSPRTNODEBASE_AO_DEV         XMC_DAC0
#define BSPRTNODEBASE_AO_SIZE        2UL
#define BSPRTNODEBASE_AO_CH0         0U
#define BSPRTNODEBASE_AO_CH1         1U
#define BSPRTNODEBASE_AO_CH0_VAL     2048
#define BSPRTNODEBASE_AO_CH1_VAL     2048
#define BSPRTNODEBASE_DIO_DEV        XMC_GPIO_PORT5

#define BSPRTNODEBASE_DIO_I0_PIN     0U
#define BSPRTNODEBASE_DIO_I0_MASK    (1UL << BSPRTNODEBASE_DIO_I0_PIN)
#define BSPRTNODEBASE_DIO_I1_PIN     2U
#define BSPRTNODEBASE_DIO_I1_MASK    (1UL << BSPRTNODEBASE_DIO_I1_PIN)
#define BSPRTNODEBASE_DIO_I_MASK     (BSPRTNODEBASE_DIO_I0_MASK | \
                                      BSPRTNODEBASE_DIO_I1_MASK)

#define BSPRTNODEBASE_DIO_O0_PIN     1U
#define BSPRTNODEBASE_DIO_O0_MASK    (1UL << BSPRTNODEBASE_DIO_O0_PIN)
#define BSPRTNODEBASE_DIO_O1_PIN     3U
#define BSPRTNODEBASE_DIO_O1_MASK    (1UL << BSPRTNODEBASE_DIO_O1_PIN)
#define BSPRTNODEBASE_DIO_O_MASK     (BSPRTNODEBASE_DIO_O0_MASK | \
                                      BSPRTNODEBASE_DIO_O1_MASK)

#define BSPRTNODEBASE_LED_DEV        XMC_GPIO_PORT2
#define BSPRTNODEBASE_LED_ETH_PIN    10U
#define BSPRTNODEBASE_LED_G_PIN      11U
#define BSPRTNODEBASE_LED_R_PIN      12U

#define BSPRTNODEBASE_QEI0_DEV       POSIF0
#define BSPRTNODEBASE_QEI1_DEV       POSIF1
#define BSPRTNODEBASE_QEI_SIZE       2UL

#define BSPRTNODEBASE_QEI0_TMR_DEV   CCU40
#define BSPRTNODEBASE_QEI0_TMR_DEV_0 CCU40_CC40
#define BSPRTNODEBASE_QEI0_TMR_DEV_1 CCU40_CC41
#define BSPRTNODEBASE_QEI0_TMR_DEV_2 CCU40_CC42
#define BSPRTNODEBASE_QEI0_TMR_DEV_3 CCU40_CC43
#define BSPRTNODEBASE_QEI0_TMR_SIZE  4
#define BSPRTNODEBASE_QEI0_TMR_IRQ   CCU40_0_IRQn
#define BSPRTNODEBASE_QEI0_TMR_0_DIV 0UL
#define BSPRTNODEBASE_QEI0_TMR_1_DIV 0UL
#define BSPRTNODEBASE_QEI0_TMR_2_DIV 5UL             /* div by 32 (in >> 5)*/
#define BSPRTNODEBASE_QEI0_TMR_3_DIV 5UL             /* div by 32 (in >> 5)*/
#define BSPRTNODEBASE_QEI0_ERR_DEV   XMC_GPIO_PORT1
#define BSPRTNODEBASE_QEI0_ERRA_PIN  6
#define BSPRTNODEBASE_QEI0_ERRB_PIN  7
#define BSPRTNODEBASE_QEI0_ERRI_PIN  8

#define BSPRTNODEBASE_QEI1_TMR_DEV   CCU41
#define BSPRTNODEBASE_QEI1_TMR_DEV_0 CCU41_CC40
#define BSPRTNODEBASE_QEI1_TMR_DEV_1 CCU41_CC41
#define BSPRTNODEBASE_QEI1_TMR_DEV_2 CCU41_CC42
#define BSPRTNODEBASE_QEI1_TMR_DEV_3 CCU41_CC43
#define BSPRTNODEBASE_QEI1_TMR_SIZE  4
#define BSPRTNODEBASE_QEI1_TMR_IRQ   CCU41_0_IRQn
#define BSPRTNODEBASE_QEI1_TMR_0_DIV 0UL
#define BSPRTNODEBASE_QEI1_TMR_1_DIV 0UL
#define BSPRTNODEBASE_QEI1_TMR_2_DIV 5UL             /* div by 32 (in >> 5)*/
#define BSPRTNODEBASE_QEI1_TMR_3_DIV 5UL             /* div by 32 (in >> 5)*/
#define BSPRTNODEBASE_QEI1_ERR_DEV   XMC_GPIO_PORT3
#define BSPRTNODEBASE_QEI1_ERRA_PIN  11
#define BSPRTNODEBASE_QEI1_ERRB_PIN  12
#define BSPRTNODEBASE_QEI1_ERRI_PIN  13

#define BSPRTNODEBASE_UART_DEV        XMC_UART0_CH0
#define BSPRTNODEBASE_UART_RX_ISR     USIC0_0_IRQn
#define BSPRTNODEBASE_UART_TX_ISR     USIC0_1_IRQn
#define BSPRTNODEBASE_UART_RX_SR      0
#define BSPRTNODEBASE_UART_TX_SR      1
#define BSPRTNODEBASE_UART_IO_DEV     XMC_GPIO_PORT1
#define BSPRTNODEBASE_UART_RX_PIN     4
#define BSPRTNODEBASE_UART_TX_PIN     5

#define BSPRTNODEBASE_FLASH_SEC_SIZE  0x40000UL       /* 256kB */
#define BSPRTNODEBASE_FLASH_ESIZE     BSPRTNODEBASE_FLASH_SEC_SIZE
#define BSPRTNODEBASE_FLASH_PSIZE     XMC_FLASH_BYTES_PER_PAGE
#define BSPRTNODEBASE_FLASH_START     ((uint8_t *)XMC_FLASH_SECTOR_11)
#define BSPRTNODEBASE_FLASH_END       (BSPRTNODEBASE_FLASH_START + \
                                       BSPRTNODEBASE_FLASH_SEC_SIZE)

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

typedef union{
  struct{
    uint16_t       ch00;
    uint16_t       ch01;
    uint16_t       ch02;
    uint16_t       ch03;
  };
  uint16_t         ch[BSPRTNODEBASE_AI_SIZE];
} sBspRTnodeBaseAI;

typedef union{
  struct{
    uint16_t       ch0;
    uint16_t       ch1;
  };
  uint16_t         ch[BSPRTNODEBASE_AO_SIZE];
} sBspRTnodeBaseAO;

typedef union{
  struct{
    uint32_t       i0 : 1;
    uint32_t       o0 : 1;
    uint32_t       i1 : 1;
    uint32_t       o1 : 1;
    uint32_t          : 28;
  };
  uint32_t         port;
} sBspRTnodeBaseDIO;

typedef struct{
  uint16_t         pos;
  uint16_t         tic;
  uint32_t         t;
  union{
    struct {
      uint32_t     erra : 1;
      uint32_t     errb : 1;
      uint32_t     erri : 1;
    };
    uint32_t       err;
  };
  int8_t           dir;
} sBspRTnodeBaseQEI;

typedef struct{
  sBspRTnodeBaseAI  ai;
  sBspRTnodeBaseAO  ao;
  sBspRTnodeBaseDIO dio;
  sBspRTnodeBaseQEI qei0;
  sBspRTnodeBaseQEI qei1;
} sBspRTnodeBaseRow;


/*****************************************************************************
 * Globals variables
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/


#ifdef __cplusplus
}
#endif

#endif /* _BSPRTNODEBASE_H_ */
